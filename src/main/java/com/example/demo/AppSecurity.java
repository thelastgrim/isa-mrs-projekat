package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableAsync
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class AppSecurity extends WebSecurityConfigurerAdapter{

	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
	    	http.httpBasic().disable();
	    	http.formLogin().disable();
	    	http.rememberMe().disable();
	    	http.headers().frameOptions().disable();
	    	http.csrf().disable();
	    }
}
