package com.example.demo.controllers;

import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.AdminRepo;
import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.RequestOperationRepo;
import com.example.demo.database.RequestRepo;
import com.example.demo.database.ReservationRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.database.SurgeryRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.functionalites.ReservationSorter;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.Admin;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.ReqOperation;
import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.example.demo.model.Surgery;
import com.example.demo.services.AdminService;
import com.example.demo.services.ClinicService;
import com.example.demo.services.SystemService;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Controller
@PreAuthorize("hasAuthority('Admin')")
public class AdminController {
	@Autowired
	ClinicRepo repo;
	
	@Autowired
	private ClinicService clinicService;
	
	@Autowired
	AdminRepo adrepo;
	
	@Autowired
	ReservationRepo resRepo;
	
	@Autowired
	RoomRepo rrepo;
	
	@Autowired
	UserRepo urepo;
	
	@Autowired
	PatientRepo patRepo;
	
	@Autowired
	ExamTypeRepo exTypeRepo;
	
	@Autowired
	ExamRepo examRepo;
	
	@Autowired
	SurgeryRepo sRepo;
	
	@Autowired
	RequestRepo reqRepo;
	
	@Autowired
	RequestAppoRepo reAppoRepo;
	
	@Autowired
	RequestOperationRepo reqOperRepo;
	

	@Autowired
	SystemService systemService;
	
	@Autowired
	AppointmentRepo appoRepo;
	
	@Autowired
	DoctorRepo drrepo;
	@Autowired
	AdminService adminService;
	
	private static Gson g = new Gson();

	@RequestMapping("/searchRoom")
	@ResponseBody
	public String addRoom(@RequestBody Map<String, String> payload, HttpServletRequest request) {
		//NAPOMENA: za sad zbog testiranja trazimo slobodan termin u trajanju od min 59min
		HashMap<String, ArrayList<LocalDate>> dates = new HashMap<>();
		//Integer user_id = Integer.parseInt(request.getSession().getAttribute("USER_ID").toString());

		//Clinic c = adrepo.findById(user_id).get().getClinic();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String num = payload.get("number").toString();
		Date searchDate = new Date();
		try {
			searchDate = formatter.parse(payload.get("date").toString());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		List<Room> rooms = rrepo.findByNameContainingIgnoreCase(num);
		List<Room> rooms2 = rrepo.findByNumberContaining(num);
		
		for(Room r: rooms) {
			System.out.println( "---*-*-**-sobe: "+r.toString());
		}
		
		for(Room r: rooms2) {
			System.out.println( "---*-*-**-sobeNumber: "+r.toString());
		}
		
		List<Room> rooms2Copy = new ArrayList<>(rooms2);
		rooms2Copy.removeAll(rooms);
		rooms.addAll(rooms2Copy);
		
		System.out.println("--------------------");
		for(Room r: rooms) {
			System.out.println( "  ###sobe: "+r.toString());
			System.out.println(r.getClinic());
		}
		
		
		
		for(Room r: rooms) {
			
			ArrayList<LocalDate> date = new ArrayList<>();
			/*System.out.println("````````````````````````````");
			
			System.out.println(r.getRess());
			System.out.println("````````````````````````````");
*/
			//Set<Reservation> re = r.getRessByDate(searchDate);
			Set<Reservation> re = r.getRess();
			final Map<String, TemporalAdjuster> ADJUSTERS = new HashMap<>();
			ADJUSTERS.put("day", TemporalAdjusters.ofDateAdjuster(d -> d));
			Map<Object, List<Reservation>> result = re.stream().collect(Collectors.groupingBy(item -> (item.getAppointment().getDateTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().with(ADJUSTERS.get("day"))));
			System.out.println("********************");
			result.forEach((k,v) -> {
			ReservationSorter.sortByDateUp(v) ;
			boolean free = false;
			for(int i=0; i <v.size();i++) {
				if(v.size() == 1) {
					free = true;
					break;
				}
				//proveravamo da li ima termina od pocetka r.vremena(7h) do prvog rezevisanog za taj dan
				if( ( 07 - v.get(0).getAppointment().getDateTime().getHours()) < -1) {
					free = true;
					break;
				}else if( (07 - v.get(0).getAppointment().getDateTime().getHours()) <=1) {
					if( ( 59- v.get(0).getAppointment().getDateTime().getMinutes() ) <= -59) {
						free = true;
						break;
					}
				}
				
				//proveravamo da li ima termina od kraja r.vremena(15h) do poslednjeg rezevisanog za taj dan
				if( ( 15 - v.get(v.size()-1).getAppointment().getDateTime().getHours()) >1) {
					free = true;
					break;
				}else if( (15 - v.get(v.size()-1).getAppointment().getDateTime().getHours()) <=1) {
					if( ( 59- v.get(v.size()-1).getAppointment().getDateTime().getMinutes() ) >=59) {
						free = true;
						break;
					}
				}
				
				
				if(i <  (v.size()-1) ) {
					Reservation rr = v.get(i);
					Reservation rr2 = v.get(i+1);
					
					if( ( (rr2.getAppointment().getDateTime().getTime() - rr.getAppointment().getDateTime().getTime())/(1000*60 ) ) >=59 ) {
						free = true;
						break;
					}
					
				}
				
				
				
			}
			
			if(!free) {
				date.add((LocalDate)k);
			}
			System.out.println("Slobodno: " +free);
			System.out.println(k + " value:" +v);});
			String s = r.getNumber() + " " + r.getName();
			//trazimo prvi slobodan datum
			LocalDate firstDate = searchDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			boolean flag = true;
			while(flag) {
				if(date.contains(firstDate)) {
					firstDate = firstDate.plusDays(1);
				}else {
					flag = false;
				}
			}
			s = s+ " "+firstDate.toString(); 
			dates.put(s, date);
			System.out.println("*********************");
			
			
		
		}
		
		System.out.println(dates);
		Writer writer = new StringWriter();
		JsonGenerator jsonGenerator;
		String outputResponse = null;
		try {
			jsonGenerator = new JsonFactory().createJsonGenerator(writer);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(jsonGenerator, dates);
			jsonGenerator.close();
			outputResponse = writer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputResponse;	
	}
	
	@RequestMapping("doctors")
	@ResponseBody
	public String fetchSingleClinicData(@RequestBody Map<String, String> payload,HttpServletRequest request) {
		
		String typeFilter = payload.get("type"); 
		String dateFilter = payload.get("date");
		Integer clinic_id = Integer.parseInt(payload.get("clinic"));	
		Clinic c = repo.findById(clinic_id).get();		
		
		System.err.println("-----doctors " + typeFilter + " " + dateFilter + " "+ c);
	
		String roomNumAndName = payload.get("room");
		String[] parts = roomNumAndName.split(" ");
		int number = Integer.parseInt(parts[0]);
		
		String[] nameParts = Arrays.copyOfRange(parts, 1, parts.length);//najpre ide broj sale pa razmak pa naziv koji moze imati takodje razmak u sebi, pa zato uzimamo sve do kraja
		String name = String.join(" ", nameParts);	
		
		return  clinicService.getDoctorsAndResTime(c, dateFilter, typeFilter, number, name);
	}
	
	@RequestMapping("doctorsSurgeries")
	@ResponseBody
	public String fetchSingleClinicData2(@RequestBody Map<String, String> payload,HttpServletRequest request) {
		
		String typeFilter = payload.get("type"); 
		String dateFilter = payload.get("date");
		Integer clinic_id = Integer.parseInt(payload.get("clinic"));
		Integer surgeryId = Integer.parseInt(payload.get("surgeryId"));
		Clinic c = repo.findById(clinic_id).get();		
		
		System.err.println("-----doctors " + typeFilter + " " + dateFilter + " "+ c);
	
		String roomNumAndName = payload.get("room");
		String[] parts = roomNumAndName.split(" ");
		int number = Integer.parseInt(parts[0]);
		
		String[] nameParts = Arrays.copyOfRange(parts, 1, parts.length);//najpre ide broj sale pa razmak pa naziv koji moze imati takodje razmak u sebi, pa zato uzimamo sve do kraja
		String name = String.join(" ", nameParts);	
		
		return  clinicService.getDoctorsAndResTimeSurgeries(c, dateFilter, typeFilter, number, name,surgeryId);
	}
	
	
	@RequestMapping("/reserveRoom")
	@ResponseBody
	public  String reserveRoom(@RequestBody Map<String, String> payload) {
		ReqAppointment req;
		try {
			 req = reAppoRepo.findById(Integer.parseInt(payload.get("req"))).get();

		} catch (Exception e) {
			// TODO: handle exception
			return "Meanwhile, the request was processed by another admin!";
		}
		Integer patientId = Integer.parseInt(payload.get("patient")); 
		Integer appoID = Integer.parseInt(payload.get("appointment"));
		String roomNumAndName = payload.get("room");
		Patient p = patRepo.findById(patientId).get();
		Exam apo = (Exam)appoRepo.findById(appoID).get();
		String[] parts = roomNumAndName.split(" ");
		int number = Integer.parseInt(parts[0]);
		
		String[] nameParts = Arrays.copyOfRange(parts, 1, parts.length);//najpre ide broj sale pa razmak pa naziv koji moze imati takodje razmak u sebi, pa zato uzimamo sve do kraja
		String name = String.join(" ", nameParts);
		System.err.println(".......NUM and NAME ROOM:		 " + number + "->	 "+ name);
		Room r = rrepo.findByNumberAndName(number, name);
		String dateStr = payload.get("date");
		String time = payload.get("time");
		String dateAndTime = dateStr + " "+time;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			date = formatter.parse(dateAndTime); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.err.println("=========================================");
		System.err.println();
		boolean a = false; 
		a =systemService.reservationUnique(date, r);
		System.err.println(" .  .  .  .  .  .  .  "+a);
		System.err.println();
		
		
		System.err.println(" .  .  .  .  .  .  .  .  .  .  .  .  .  .");
		
		if (!systemService.reservationUnique(date, r)) {
			System.out.println("VREME VISE NIJE SLOBODNO");
			return "Room for selected time is not available anymore.";
		}
		
		System.err.println("=========================================");
		System.err.println();
		System.err.println("--------- "+p +"\n       " + apo + "\n        " +r);
		
		//Reservation res = new Reservation(r, p, apo);
				Reservation res = new Reservation();
				res.setRoom(r);
				res.setAppointment(apo);
			
		apo.setReservation(res);
		Integer drId = Integer.parseInt(payload.get("doctorId"));
		Doctor dr = drrepo.findById(drId).get();
		
		System.err.println("+++++ date and time res room: "+ dateAndTime);
		
		apo.setDateTime(date); //ukoliko je admin promenio datum i vreme cuvamo te izmene
		apo.setDoctor(dr);//ukoliko je admin promenio doktora cuvamo te izmene
		
		
		resRepo.saveAndFlush(res);
	
		reAppoRepo.delete(req);
		//SendEmail se = new SendEmail();
		//se.sendReservation(p, dr, apo, r);
		
		SendEmail se = new SendEmail();

		new Thread(new Runnable() {
		    public void run() {
		    	se.sendAppointmentConfirmation(req, p);
		    }
		}).start();

		
		reAppoRepo.flush();
		
		return "Room booked";
	}
	
	@RequestMapping("/reserveRoomForSurgeries")
	@ResponseBody
	public  String reserveRoomSurgeries(@RequestBody Map<String, String> payload) {
		
		String roomNumAndName = payload.get("room");
		
		String[] parts = roomNumAndName.split(" ");
		int number = Integer.parseInt(parts[0]);
		String[] nameParts = Arrays.copyOfRange(parts, 1, parts.length);//najpre ide broj sale pa razmak pa naziv koji moze imati takodje razmak u sebi, pa zato uzimamo sve do kraja
		String name = String.join(" ", nameParts);
		Room r = rrepo.findByNumberAndName(number, name);
		String dateStr = payload.get("date");
		String time = payload.get("time");
		String dateAndTime = dateStr + " "+time;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			date = formatter.parse(dateAndTime); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean a = false; 
		a =systemService.reservationUnique(date, r);
		
		if (!systemService.reservationUnique(date, r)) {
			System.out.println("VREME VISE NIJE SLOBODNO");
			return "Room for selected time is not available anymore.";
		}
		
		List<Surgery> sur = sRepo.findAll();
		Surgery surg = null;
		System.out.println(payload.get("surgeryId") + "ajdebrateIstampaj");
		for(Surgery s:sur ) {
			if(s.getId() == Integer.parseInt(payload.get("surgeryId"))) {
				surg = s;
			}
		}
		
		Reservation res = new Reservation();
		res.setRoom(r);
		res.setOperation(surg);
		
		ReqOperation req = new ReqOperation();
		req.setDateTime(date);
		req.setOperation(surg);
		
		
		Integer drId = Integer.parseInt(payload.get("doctorId"));
		Doctor dr = drrepo.findById(drId).get();
		
		System.err.println("+++++ date and time res room: "+ dateAndTime);
		
		
		
		reqOperRepo.saveAndFlush(req);
		resRepo.saveAndFlush(res);
	
		//reAppoRepo.delete(req);
		//SendEmail se = new SendEmail();
		//se.sendReservation(p, dr, apo, r);
		
		SendEmail se = new SendEmail();

//		new Thread(new Runnable() {
//		    public void run() {
//		    	se.sendAppointmentConfirmation(req, p);
//		    }
//		}).start();

		
		reAppoRepo.flush();
		
		return "Room booked";
	}
	
	
	
	@RequestMapping(value = "processLeaveReq",
			method = RequestMethod.POST)
	
	public @ResponseBody  String processLeaveReq(@RequestBody Map<String, String> payload) {
		System.err.println("processLeaveReq");
		adminService.saveLReqResponse(payload);
		return "1";
		
	}
	
	@RequestMapping("sFreeAppo")
	public @ResponseBody  String freeAppo(@RequestBody Map<String, String> payload) {
		
		adminService.saveFreeAppo(payload);
		return "1";
		
	}
	
	@RequestMapping("searchExType")
	public @ResponseBody  String searchExType(@RequestBody Map<String, String> payload) {
		Admin a= (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
		String types = adminService.searchEType(a.getClinic().getIdc(),payload);
		System.err.println("TYPES: "+types);
		
		return 	types;
	}
	
	@RequestMapping("searchDr")
	public @ResponseBody  String searchDr(@RequestBody Map<String, String> payload) {
		Admin a= (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
		String types = adminService.searchDoc(a.getClinic(),payload);

		return 	types;
	}
	
	@RequestMapping("deleteRoom")
	@ResponseBody
	public boolean deleteRoom(@RequestBody Map<String, String> payload) {
		System.out.println(payload.get("id"));
		Room r = rrepo.findByNumber(Integer.parseInt(payload.get("id")));
		
		List<Appointment> appos = appoRepo.findAll();
		
		for (Appointment appointment : appos) {
			try {
				
				if (appointment.getReservation().getRoom().getNumber() == r.getNumber()) {
					return false;
				}
			} catch (NullPointerException e) {
				// TODO: handle exception
				// termin bez rezervacije
				continue;
			}
			
		}
		
		rrepo.delete(r);
		rrepo.flush();
		return true;
	}

	
}
