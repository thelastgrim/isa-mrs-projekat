package com.example.demo.controllers;

import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DiagRepo;
import com.example.demo.database.RequestRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.model.Address;
import com.example.demo.model.CCAdmin;
import com.example.demo.model.Clinic;
import com.example.demo.model.Diagnosis;
import com.example.demo.model.ReqRegistration;
import com.example.demo.model.STATUS;
import com.example.demo.model.User;
import com.example.demo.services.SystemService;

@Controller
@PreAuthorize("hasAuthority('CCAdmin')")
public class CCAdminController {
	
	@Autowired
	UserRepo repo;
	
	@Autowired
	ClinicRepo clrepo;
	
	@Autowired
	RequestRepo rrepo;
	
	@Autowired
	DiagRepo diagrepo;
	
	@Autowired
	private SystemService systemService;
	
	@RequestMapping("ccAdmin")
	public String requests(Model model) {
		
		List<ReqRegistration> requests = rrepo.findAll();
		List<Diagnosis> diagnoses = diagrepo.findAll();
		
		model.addAttribute("diagnoses",diagnoses);
		
		if (!requests.isEmpty()) {
			System.out.println(requests.get(0).toString());
			
			model.addAttribute("requests", requests);
			
			
		}
		return "admin";
				
	}
	
	
	@RequestMapping("addAdmin")
	public @ResponseBody String addAdmin(@RequestBody Map<String, String> payload) {
		
		if(repo.findByeMail(payload.get("email")) != null) {
			
			return "0";
		}
		Address a = new Address();
		a.setCountry(payload.get("country"));
		a.setCity(payload.get("city"));
		a.setStreetName(payload.get("street"));
	
		User u = new User();
		u.setAddress(a);
		u.setFirstName(payload.get("firstName"));
		u.setLastName(payload.get("lastName"));
		u.setPhoneNUmber(payload.get("phoneNumber"));
		u.seteMail(payload.get("email"));
		u.setPassword(payload.get("pass"));
		u.setStatus(STATUS.VALID);
		
		CCAdmin cca = new CCAdmin(u);
		
		
		repo.saveAndFlush(cca);
		
		return "1";
	}
	
	@RequestMapping("adminReg")
	public String adminReg() {
		return "adminReg";
	}
	
	@RequestMapping(value = "addClinic",
			method = RequestMethod.POST)
	@ResponseBody
	public String addClinic(@RequestBody Map<String, String> payload) {

		Address a = new Address();
		a.setCountry(payload.get("county"));
		a.setCity(payload.get("city"));
		a.setStreetName(payload.get("street"));
		
		Clinic c = new Clinic();
		c.setDescription(payload.get("description"));
		c.setName(payload.get("name"));
		c.setAddress(a);
		
		clrepo.saveAndFlush(c);
		
		return "success";
	}
	
	@RequestMapping("addDiagnosis")
	@ResponseBody
	public String addDiagnosis(@RequestBody Map<String,String> payload) {
		
		Diagnosis diag = new Diagnosis();
		diag.setCode(payload.get("code"));
		diag.setLatin(payload.get("latin"));
		diag.setDescription(payload.get("description"));
		
		
		//System.out.println(payload + "alooo ovde sam");
		
		diagrepo.saveAndFlush(diag);
		
		return "ok";
		
		
	}
	

}
