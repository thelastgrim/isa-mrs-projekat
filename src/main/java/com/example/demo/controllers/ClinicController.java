package com.example.demo.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.ClinicRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.model.Clinic;
import com.example.demo.services.ClinicService;


@Controller
@PreAuthorize("hasAuthority('Patient')")
public class ClinicController {
	
	@Autowired
	ClinicRepo repo;
	
	@Autowired
	ExamRepo erepo;
	
	@Autowired
	ExamTypeRepo etrepo;
	
	@Autowired
	ExamRepo exrepo;
	
	@Autowired
	private ClinicService clinicService;

	
	@RequestMapping("/clinic{idc}+{type}+{date}")
	public String fetchClinicPage(@PathVariable("idc") String idc, @PathVariable("type") String type, @PathVariable("date") String date, Model model) {
		
		model = clinicService.getClinicPageData(model,Integer.parseInt(idc), type, date);

		return "clinic_home";
	}
	
	@RequestMapping("filterClinicNarrow")
	@ResponseBody
	public String fetchSingleClinicData(@RequestBody Map<String, String> payload) {
		
		String typeFilter = payload.get("type").split(" \\[")[0]; 
		String dateFilter = payload.get("date");
		Integer clinic_id = Integer.parseInt(payload.get("clinic"));
		String sorterFilter = payload.get("sorter");
		Clinic c = repo.findById(clinic_id).get();
		
		return clinicService.getSingleClinicData(c, dateFilter, typeFilter, sorterFilter);
	}
	
	@RequestMapping("filterClinics")
	@ResponseBody
	public String fetchFilteredClinics(@RequestBody Map<String, String> payload) {
		
		return clinicService.getFilteredClinics(payload);
	}
	
	
	//@PostMapping("changeClinicName")
	@RequestMapping(value = "/changeClinicName",
					method = RequestMethod.POST,
					produces = MediaType.TEXT_PLAIN_VALUE,
					consumes = MediaType.APPLICATION_JSON_VALUE)
	public String changeClinicName(@RequestBody Map<String, String> payload) {
		System.out.println("promena linike");
	
		Clinic c = repo.findByName(payload.get("oldname").trim());
		System.err.println("PRE " + c.toString());
		c.setName(payload.get("name"));
		repo.saveAndFlush(c);
		System.err.println("POSLE " + c.toString());
		
		return "homepage";
	}
	
	@RequestMapping(value = "/changeClinicAddress",
			method = RequestMethod.POST,
			produces = MediaType.TEXT_PLAIN_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String changeClinicAddress(@RequestBody Map<String, String> payload) {
		
		Clinic c = repo.findByName(payload.get("name").trim());
		
		//c.setAddress(payload.get("address"));
		repo.saveAndFlush(c);
		System.err.println("	***NAKON IZMENE  " + c.toString());
		
		return "homepage";
	}
	
	@RequestMapping(value = "/changeClinicDescription",
			method = RequestMethod.POST,
			produces = MediaType.TEXT_PLAIN_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String changeClinicDescription(@RequestBody Map<String, String> payload) {

		
		Clinic c = repo.findByName(payload.get("name").trim());
		c.setDescription(payload.get("description"));
		repo.saveAndFlush(c);
		System.err.println("	***NAKON IZMENE OPISA  " + c.toString());
		
		return "homepage";
	}
	
	@RequestMapping(value = "search",
			method = RequestMethod.POST,
			produces = MediaType.TEXT_PLAIN_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String search(@RequestBody Map<String, String> payload) {

		String str = "";
		Clinic c = repo.findByName(payload.get("name").trim());
		try {
			str =  c.getName() + "," + c.getAddress() + "," + c.getDescription();
		}catch (NullPointerException e) {
			str = "Klinika nije pronadjena.";
		}

		return str;
	}
 
}

