package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.AddressRepo;
import com.example.demo.database.AdminRepo;
import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DiagRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RatingsDoctorsRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.RequestLeaveRepo;
import com.example.demo.database.RequestOperationRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.database.SurgeryRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.functionalites.EncryptDecrypt;
import com.example.demo.model.Address;
import com.example.demo.model.Admin;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Diagnosis;
import com.example.demo.model.Doctor;
import com.example.demo.model.ExamType;
import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;
import com.example.demo.model.RatingsDoctor;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.ReqOperation;
import com.example.demo.model.RequestType;
import com.example.demo.model.Room;
import com.example.demo.model.STATUS;
import com.example.demo.model.Surgery;
import com.example.demo.model.User;
import com.example.demo.services.SystemService;


@org.springframework.stereotype.Controller

public class Controller { 
	
	
	
	@Autowired
	ClinicRepo repo;
	
	
	
	@Autowired
	PatientRepo prepo;
	
	@Autowired
	UserRepo urepo;
	
	@Autowired
	DiagRepo drepo;
	
	@Autowired
	DoctorRepo drRepo;
	
	@Autowired
	AppointmentRepo appoRepo;
	
	@Autowired
	AdminRepo adrepo;
	
	@Autowired
	AddressRepo addrepo;
	
	@Autowired
	RoomRepo rrepo;
	
	@Autowired
	ExamTypeRepo examRepo;
	
	@Autowired
	private SystemService systemService;
	
	@Autowired
	RequestAppoRepo reAppoRepo;
	
	@Autowired
	RequestLeaveRepo leaveRepo;
	
	@Autowired
	RatingsDoctorsRepo ratDrRepo;

	@Autowired
	RequestOperationRepo reqOperationRepo;
	
	@Autowired
	SurgeryRepo surgRepo;
	
	@RequestMapping("/")
	public String homepage() {
		return "homepage2.html";
	}

	
	@RequestMapping("diagnoses"
			)
	public String diagnoses(Model model) {
		
		List<Diagnosis> diagnoses = drepo.findAll();
		
		model.addAttribute("diagnoses",diagnoses);
		
		return "ccAdmin";
	}
	

	
	@RequestMapping("/accountActivation/{path}")
	@ResponseBody
	public String activateAccount(@PathVariable("path")String path) {
	
		try {
			EncryptDecrypt ed = new EncryptDecrypt();
			String data = ed.decrypt(path.replace("mrsicaclinic","/"));
			String id = data.split(".com")[1];

			
			Patient p = prepo.findById(Integer.parseInt(id)).get();
			
			if(p.getStatus()==STATUS.VALID) {
				return "<h1> Account already activated.</h1>";
			}

			
			p.setStatus(STATUS.VALID);

			p.setMedrec(new MedicalRecord());	
			prepo.saveAndFlush(p);
			
		}
		catch (NoSuchElementException e) {
			return "<h1> Account already activated.</h1>";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			return "<h1> There's been problem with account activation.</h1>";
		}
		
		return "<h1> Account successfuly activated. You can now log in.</h1>";
	}
	
	

	
	@RequestMapping("logout")
	public String logout(HttpServletRequest request) {
		
		String user_id = request.getSession().getAttribute("USER_ID").toString();
		
		request.getSession().invalidate();
		
		
		return "homepage2";	
	}
	
	@RequestMapping("adminHome")
	public String adminHome() {
		
		return "admin_home";	
	}
	
	@RequestMapping("doctorHome")
	public String doctorHome() {
		
		return "doctor_home";	
	}
	
	@RequestMapping("addRoom")
	@ResponseBody
	public String addRoom(@RequestBody Map<String, String> payload, HttpServletRequest request) {
		
		//Integer user_id = Integer.parseInt(request.getSession().getAttribute("USER_ID").toString());
		Admin a = (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
		//Clinic c = adrepo.findById(user_id).get().getClinic();
		Clinic c = a.getClinic();
		Room r = new Room();
		r.setNumber(Integer.parseInt(payload.get("number")));
		r.setName(payload.get("rName"));
		r.setClinic(c);
		
		rrepo.saveAndFlush(r);
		System.out.println("room added");
		
		return "ok";	
	}
	
	@RequestMapping("adminPage")
	public String clinicAdmin(HttpServletRequest request, Model model) {
		
		Admin a = (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
	
		Clinic c = a.getClinic();
		
		List<Room> rooms = rrepo.findAllByClinic_Idc(c.getIdc());
		
		System.out.println(rooms.get(0).getNumber());

		model.addAttribute("clinic", c);
		model.addAttribute("rooms", rooms);

		List<ReqAppointment> reqs = reAppoRepo.findAll();
		List<ReqAppointment> reqs2 = reAppoRepo.findByTypeAndAppointmentClinicAndAppointmentReservationIsNull(RequestType.EXAM,c);
		
		List<Surgery> surgeries = surgRepo.findAll();
		List<Surgery> surgeries2 = new ArrayList<Surgery>();
		
		for(Surgery s: surgeries) {
			
			if(c.getIdc()== s.getClinic().getIdc()) {
				surgeries2.add(s);
			}
			
		}
		
		model.addAttribute("surgeries2",surgeries2);
		
		
		
		model.addAttribute("requests", reqs2);
		model.addAttribute("leaveReqs", leaveRepo.findAll());
		model.addAttribute("types",examRepo.findAllByClinic_idc(c.getIdc()));
		model.addAttribute("rooms",rrepo.findAllByClinic_Idc(c.getIdc()));
		model.addAttribute("doctors",drRepo.findAllByClinic_Idc(c.getIdc()));
		
		return "admin_home";
		
	}
	
	@RequestMapping("addAType")
	@ResponseBody
	public String addAType(@RequestBody Map<String, String> payload, HttpServletRequest request) {
		
		ExamType at = new ExamType();
		
		
		at.setName(payload.get("name"));
		try {
			at.setDuration(Integer.parseInt(payload.get("duration")));
		} catch (Exception e) {
			return "Wrong duration.Must be integer!"; //u slucaju da neko unese 12m
		}
		try {
			at.setPrice(Integer.parseInt(payload.get("price")));
		} catch (Exception e) {
			return "Wrong price. Must be integer!";
		}
		
		
	//	Integer user_id = Integer.parseInt(request.getSession().getAttribute("USER_ID").toString());
		
		//Clinic c = adrepo.findById(user_id).get().getClinic();
		Admin a = (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
	
		Clinic c = a.getClinic();
		
		at.setClinic(c);
		
		examRepo.saveAndFlush(at);
		return "Appointment type added.";
	}
	
	@RequestMapping("addDoc")
	public @ResponseBody String newDoc(@RequestBody Map<String, String> payload, HttpServletRequest request) {
		if(urepo.findByeMail(payload.get("email"))!=null) {
			
			return "0";
		}
		Address a = new Address();
		a.setCountry(payload.get("country"));
		a.setCity(payload.get("city"));
		a.setStreetName(payload.get("street"));

		User u = new User();
		u.setAddress(a);
		u.setFirstName(payload.get("firstName"));
		u.setLastName(payload.get("lastName"));
		u.setPhoneNUmber(payload.get("phoneNumber"));
		u.seteMail(payload.get("email"));
		u.setPassword(payload.get("pass"));
		u.setStatus(STATUS.VALID);
		
		Doctor d = new Doctor(u);
		Set<ExamType> spe = d.getSpecializations();
		System.err.println("BEFORE: "+ spe);
		Set<ExamType> specializations = new HashSet<ExamType>();
		ExamType ex = examRepo.findByName(payload.get("spec"));
		specializations.add(ex);
		d.setSpecializations(specializations);
		d.setStartTime(payload.get("startTime"));
		d.setEndTime(payload.get("endTime"));
		System.err.println("AFTER: "+ d.getSpecializations());
		
	//	Integer user_id = Integer.parseInt(request.getSession().getAttribute("USER_ID").toString());
		User user = (User) Hibernate.unproxy(systemService.getLoggedUser());

		
		Clinic c = adrepo.findById(user.getId()).get().getClinic();
		
		d.setClinic(c);
	
		d.setRating(5.0);
		
		try {
			drRepo.saveAndFlush(d);
		} catch (ConstraintViolationException e) {
			return "2";
		}
		
		
		RatingsDoctor rt = new RatingsDoctor();
		rt.setDoctor(d);
		rt.setRating(5);
		ratDrRepo.saveAndFlush(rt);
		return "1";

	
	}
	
	@RequestMapping("updateClinicInfo")
	public String UpdateClinicInfo(@RequestBody Map<String, String> payload, Model model) {
		
		
		Admin admin = (Admin) Hibernate.unproxy(systemService.getLoggedUser());
		
		Clinic c = repo.findById(adrepo.findById(admin.getId()).get().getClinic().getIdc()).get();
		System.out.println();
		Address a = new Address();
		a.setCity(payload.get("c_city"));
		a.setCountry(payload.get("c_country"));
		a.setStreetName(payload.get("c_street"));
		
		c.setAddress(a);
		
		c.setName(payload.get("clinicName"));
		c.setDescription(payload.get("c_description"));
		
		model.addAttribute("clinic", c);
		
		repo.saveAndFlush(c);
	
		return "admin_home";
		
	}
	

	
	@RequestMapping("login")
	@ResponseBody
	public String login(@RequestBody Map<String, String> payload, HttpServletRequest request) {
		return systemService.LogIn(payload.get("email"), payload.get("pass"));	
	}
			
		@RequestMapping("register")
		public String register() {
			return "register";
		}
		
		@RequestMapping("registerClinic")
		public String registerClinic() {
			return "registerClinic";
		}
		
		@RequestMapping("nursePage")
		public String nursePage(Model model, HttpSession session) {
			
			List<Patient> patients = prepo.findAll();
			
			
			model.addAttribute("patients",patients);
			
			return "nurse_home";
		}
		
		
	
	@RequestMapping("changeClinic")
	public String changeClinicName(Model model) {
		
		List<Clinic> clinics = repo.findAll();
		
		model.addAttribute("clinics", clinics);
		
		return "changeClinic";
		
	}

	@RequestMapping( "/searchClinic")	
	public String searchClinic() {
		
		
		return "searchClinic";
	}

	
	@RequestMapping("defFreeAppointment")
	public String defFreeAppointment() {
		return "defFreeAppointment";
	}
	
	
	
	
	@RequestMapping("addAppoType")
	public String newAppoType() {
		return "newAppoType";
	}
	
	@RequestMapping("heldAppo")
	public String heldAppo() {
		return "yearlyGraph";
	}
	
	
	@RequestMapping("monthly")
	public @ResponseBody ArrayList<Integer>  graph() {
	//	ArrayList<Integer> li = new ArrayList<>(Arrays.asList(10,50,30,45,20,80,65,20,15,90,30,75));
		ArrayList<Integer> li = new ArrayList<>();

		Date d = new Date();
		List<Appointment>  all = appoRepo.findAll();
		int jan =0;
		int feb = 0;
		int mar =0;
		int apr =0;
		int maj = 0;
		int jun=0;
		int jul=0;
		int avg=0;
		int sept=0;
		int okt=0;
		int nov=0;
		int dec=0;
		
		for(Appointment ap: all) {
			if(ap.getDateTime().getYear() == d.getYear()) {
			//	System.out.println("---YEARLY "+ap);
				if(ap.isHeld()) {
					switch(ap.getDateTime().getMonth()) {
						case 0:
							jan = jan+1;
							break;
						case 1:
							feb=feb+1;
							break;
						case 2:
							mar=mar+1;;
							break;
						case 3:
							apr=apr+1;;
							break;
						case 4:
							maj=maj+1;;
							break;
						case 5:
							jun=jun+1;;
							break;
						case 6:
							jul=jul+1;;
							break;
						case 7:
							avg=avg+1;;
							break;
						case 8:
							sept=sept+1;;
							break;
						case 9:
							okt=okt+1;;
							break;
						case 10:
							nov=nov+1;;
							break;
						case 11:
							dec=dec+1;;
							break;
					}
				}
			}		
		}
		
		li.add(jan);
		li.add(feb);
		li.add(mar);
		li.add(apr);
		li.add(maj);
		li.add(jun);
		li.add(jul);
		li.add(avg);
		li.add(sept);
		li.add(okt);
		li.add(nov);
		li.add(dec);
	/*	System.out.println();
		System.out.println("Jan " +jan);
		System.out.println("Feb " +feb);
		System.out.println("Mart " + mar);
		System.out.println("Apr "+apr);
		System.out.println("Maj " +maj);
		System.out.println("Jun " +jun);
		System.out.println("Jul " +jul);*/
		//int[] myNum = {10,50,30,45,20,80,65,20,15,90,30,75};
		
		return li;
	}
	
	
	
	@RequestMapping("hourly")
	public@ResponseBody ArrayList<Integer>  hourlyGraph() {
		ArrayList<Integer> res = new ArrayList<Integer>();
		int am12_1 =0;
		int am1_2 =0;
		int am2_3 =0;
		int am3_4 =0;
		int am4_5 =0;
		int am5_6 =0;
		int am6_7 =0;
		int am7_8 =0;
		int am8_9 =0;
		int am9_10 =0;
		int am10_11 =0;
		int am11_12 =0;
		int pm12_1 =0;
		int pm1_2 =0;
		int pm2_3 =0;
		int pm3_4 =0;
		int pm4_5 =0;
		int pm5_6 =0;
		int pm6_7 =0;
		int pm7_8 =0;
		int pm8_9 =0;
		int pm9_10 =0;
		int pm10_11 =0;
		int pm11_12 =0;
		
		
		Date d = new Date();
		Date d00 = new Date();
		d00.setHours(00);
		d00.setMinutes(00);
		d00.setSeconds(00);
		
		Date d01 = new Date();
		d01.setHours(01);
		d01.setMinutes(00);
		d01.setSeconds(00);
		
		Date d02 = new Date();
		d02.setHours(02);
		d02.setMinutes(00);
		d02.setSeconds(00);
		
		Date d03 = new Date();
		d03.setHours(03);
		d03.setMinutes(00);
		d03.setSeconds(00);
		
		Date d04 = new Date();
		d04.setHours(04);
		d04.setMinutes(00);
		d04.setSeconds(00);
		
		Date d05 = new Date();
		d05.setHours(05);
		d05.setMinutes(00);
		d05.setSeconds(00);
		
		Date d06 = new Date();
		d06.setHours(06);
		d06.setMinutes(00);
		d06.setSeconds(00);
		
		Date d07 = new Date();
		d07.setHours(07);
		d07.setMinutes(00);
		d07.setSeconds(00);
		
		Date d08 = new Date();
		d08.setHours(8);
		d08.setMinutes(00);
		d08.setSeconds(00);
		
		Date d09 = new Date();
		d09.setHours(9);
		d09.setMinutes(00);
		d09.setSeconds(00);
		
		Date d10 = new Date();
		d10.setHours(10);
		d10.setMinutes(00);
		d10.setSeconds(00);
		
		Date d11 = new Date();
		d11.setHours(11);
		d11.setMinutes(00);
		d11.setSeconds(00);
		
		Date d12 = new Date();
		d12.setHours(12);
		d12.setMinutes(00);
		d12.setSeconds(00);
		
		Date d13 = new Date();
		d13.setHours(13);
		d13.setMinutes(00);
		d13.setSeconds(00);
		
		Date d14 = new Date();
		d14.setHours(14);
		d14.setMinutes(00);
		d14.setSeconds(00);
		
		Date d15 = new Date();
		d15.setHours(15);
		d15.setMinutes(00);
		d15.setSeconds(00);
		
		Date d16 = new Date();
		d16.setHours(16);
		d16.setMinutes(00);
		d16.setSeconds(00);
		
		Date d17 = new Date();
		d17.setHours(17);
		d17.setMinutes(00);
		d17.setSeconds(00);
		
		Date d18 = new Date();
		d18.setHours(18);
		d18.setMinutes(00);
		d18.setSeconds(00);
		
		Date d19 = new Date();
		d19.setHours(19);
		d19.setMinutes(00);
		d19.setSeconds(00);
		
		Date d20 = new Date();
		d20.setHours(20);
		d20.setMinutes(00);
		d20.setSeconds(00);
		
		Date d21 = new Date();
		d21.setHours(21);
		d21.setMinutes(00);
		d21.setSeconds(00);
		
		Date d22 = new Date();
		d22.setHours(22);
		d22.setMinutes(00);
		d22.setSeconds(00);
		
		Date d23 = new Date();
		d23.setHours(23);
		d23.setMinutes(00);
		d23.setSeconds(00);
		
		Date d24 = new Date();
		d24.setHours(24);
		d24.setMinutes(00);
		d24.setSeconds(00);
		
	
		System.err.println("DATE " +d);
		List<Appointment> li = appoRepo.findAll();
		for(Appointment a:li) {
			//System.out.println(a);
			if(a.isHeld()) {
				
			
			if(a.getDateTime().getYear() == d.getYear() && a.getDateTime().getMonth() == d.getMonth() && a.getDateTime().getDate() == d.getDate()) {
				System.err.println("HOURLY: "+a);
				
				if(a.getDateTime().after(d00) && a.getDateTime().before(d01)) {
					am12_1 = am12_1+1;
				}else if( a.getDateTime().after(d01) && a.getDateTime().before(d02) ) {
					am1_2 = am1_2 +1;
				}else if( a.getDateTime().after(d02) && a.getDateTime().before(d03) ) {
					am2_3 = am2_3 +1;
				}else if( a.getDateTime().after(d03) && a.getDateTime().before(d04) ) {
					am3_4 = am3_4 +1;
				}else if( a.getDateTime().after(d04) && a.getDateTime().before(d05) ) {
					am4_5 = am4_5 +1;
				}else if( a.getDateTime().after(d05) && a.getDateTime().before(d06) ) {
					am5_6 = am5_6 +1;
				}else if( a.getDateTime().after(d06) && a.getDateTime().before(d07) ) {
					am6_7 = am6_7 +1;
					System.out.println("******** "+a.getDateTime() + "  " + d06+ " "+ d07);
				}else if( a.getDateTime().after(d07) && a.getDateTime().before(d08) ) {
					am7_8 = am7_8 +1;
				}else if( a.getDateTime().after(d08) && a.getDateTime().before(d09) ) {
					am8_9 = am8_9 +1;
				}else if( a.getDateTime().after(d09) && a.getDateTime().before(d10) ) {
					am9_10 = am9_10 +1;
				}else if( a.getDateTime().after(d10) && a.getDateTime().before(d11) ) {
					am10_11 = am10_11 +1;
				}else if( a.getDateTime().after(d11) && a.getDateTime().before(d12) ) {
					am11_12 = am11_12 +1;
				}else if( a.getDateTime().after(d12) && a.getDateTime().before(d13) ) {
					pm12_1 = pm12_1 +1;
				}else if( a.getDateTime().after(d13) && a.getDateTime().before(d14) ) {
					pm1_2 = pm1_2 +1;
				}else if( a.getDateTime().after(d14) && a.getDateTime().before(d15) ) {
					pm2_3 = pm2_3 +1;
				}else if( a.getDateTime().after(d15) && a.getDateTime().before(d16) ) {
					pm3_4 = pm3_4 +1;
				}else if( a.getDateTime().after(d16) && a.getDateTime().before(d17) ) {
					pm4_5 = pm4_5 +1;
				}else if( a.getDateTime().after(d17) && a.getDateTime().before(d18) ) {
					pm5_6 = pm5_6 +1;
				}else if( a.getDateTime().after(d18) && a.getDateTime().before(d19) ) {
					pm6_7 = pm6_7 +1;
				}else if( a.getDateTime().after(d19) && a.getDateTime().before(d20) ) {
					pm7_8 = pm7_8 +1;
				}else if( a.getDateTime().after(d20) && a.getDateTime().before(d21) ) {
					pm8_9 = pm8_9 +1;
				}else if( a.getDateTime().after(d21) && a.getDateTime().before(d22) ) {
					pm9_10 = pm9_10 +1;
				}else if( a.getDateTime().after(d22) && a.getDateTime().before(d23) ) {
					pm10_11 = pm10_11 +1;
				}else if( a.getDateTime().after(d23) && a.getDateTime().before(d24) ) {
					pm11_12 = pm11_12 +1;
				}
			}
		}
			
		}
		
		res.add(am12_1);
		res.add(am1_2);
		res.add(am2_3);
		res.add(am3_4);
		res.add(am4_5);
		res.add(am5_6);
		res.add(am6_7);
		res.add(am7_8);
		res.add(am8_9);
		res.add(am9_10);
		res.add(am10_11);
		res.add(am11_12);
		res.add(pm12_1);
		res.add(pm1_2);
		res.add(pm2_3);
		res.add(pm3_4);
		res.add(pm4_5);
		res.add(pm5_6);
		res.add(pm6_7);
		res.add(pm7_8);
		res.add(pm8_9);
		res.add(pm9_10);
		res.add(pm10_11);
		res.add(pm11_12);
		return res;
	}
	
	@RequestMapping("weekly")
	public @ResponseBody ArrayList<Integer>  weeklyGraph() {
		ArrayList<Integer> li = new ArrayList<>();

		Date d = new Date();
		List<Appointment>  all = appoRepo.findAll();
		int pon =0;
		int uto = 0;
		int sre =0;
		int cet =0;
		int pet = 0;
		int sub=0;
		int ned=0;
		
		
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);

		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		Date monday = c.getTime();

		Date nextMonday= new Date(monday.getTime()+7*24*60*60*1000);
		
		for(Appointment ap: all) {
			if(ap.getDateTime().getYear() == d.getYear()) {
			//	System.out.println("---YEARLY "+ap);
				if(ap.getDateTime().after(monday) && ap.getDateTime().before(nextMonday)) {
					if(ap.isHeld()) {
						switch(ap.getDateTime().getDay()) {
						case 0:
							ned = ned+1;
							break;
						case 1:
							pon = pon+1;
							break;
						case 2:
							uto = uto+1;
							break;
						case 3:
							sre = sre+1;
							break;
						case 4:
							cet = cet+1;
							break;
						case 5:
							pet = pet+1;
							break;
						case 6:
							sub = sub+1;
							break;
						
						}
						
					}
				}
				
			}
		}
		
		li.add(pon);
		li.add(uto);
		li.add(sre);
		li.add(cet);
		li.add(pet);
		li.add(sub);
		li.add(ned);

		System.out.println(li.toString());
		
		return li;
	}
	
	
	@RequestMapping( "sRoom{id}")	
	public String sRoom(@PathVariable("id") Integer _id, Model model) {
		ReqAppointment r = reAppoRepo.findById(_id).get();
		model.addAttribute("req", r);
		
		
		return "searchRoom";
	}
	
	@RequestMapping( "sRoom2{id}")	
	public String sRoom2(@PathVariable("id") Integer _id, Model model) {
		Surgery s = surgRepo.findById(_id).get();
		model.addAttribute("req", s);
		
		
		return "searchRoomForSurgeries";
	}
	
	/* 	Date da= new Date();
		da.setDate(8);
		System.out.println(da);
		
		System.out.println();
	 * Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);

		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		Date monday = c.getTime();

		Date nextMonday= new Date(monday.getTime()+7*24*60*60*1000);

		boolean isThisWeek = da.after(monday) && da.before(nextMonday);
		System.out.println(isThisWeek);*/
	
}
