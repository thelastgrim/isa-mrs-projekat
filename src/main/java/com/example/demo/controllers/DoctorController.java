package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.MedRecRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.PrescriptionRepo;
import com.example.demo.database.ReportInfoRepo;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;
import com.example.demo.model.Prescription;
import com.example.demo.model.ReportInfo;
import com.example.demo.services.DoctorService;
import com.example.demo.services.SystemService;


@Controller
@PreAuthorize("hasAuthority('Doctor')")
public class DoctorController {
	@Autowired
	DoctorRepo drepo;
	
	@Autowired
	PatientRepo patrepo;

	@Autowired
	SystemService systemService;
	
	@Autowired
	private DoctorService doctorService;
	
	@Autowired
	MedRecRepo medrepo;
	
	@Autowired
	ExamRepo erepo;
	
	@Autowired
	ExamTypeRepo etyperepo;
	
	@Autowired
	PrescriptionRepo prescriptionRepo;
	
	@Autowired
	ReportInfoRepo reportInforepo;
	
	@RequestMapping("doctorPage")
	public String clinicAdmin(Model model, HttpSession session) {
		
		Doctor d = (Doctor) Hibernate.unproxy(systemService.getLoggedUser()); 
		System.out.println(d.getClinic().getTypes());
		
		model.addAttribute("types", d.getClinic().getTypes());
		model.addAttribute("doctor",d);
		model.addAttribute("patients", doctorService.getClinicPatients(d.getClinic()));
	
		
	
		return "doctor_home";
		
	}
	
	@RequestMapping("newExam")
	public String newExamPage() {
		return "NewExam";
	}
	
	
	@RequestMapping("sortPatients")
	@ResponseBody
	public String sortPatients(@RequestBody Map<String, String> payload) {
		
		
		return doctorService.getSortedPatients(payload);
		
	}
	

	@RequestMapping(value = "reqLeave",
			method = RequestMethod.POST)
	public @ResponseBody String reqLeave(@RequestBody Map<String, String> payload) {

		doctorService.saveReqLeave(payload);
		return "1";
		}
	
	@RequestMapping(value = "/bookAppoByDr",
			method = RequestMethod.POST)
	public @ResponseBody String bookAppoByDr(@RequestBody Map<String, String> payload) {
		return doctorService.bookAppoByDr(payload);
	
	}
	@RequestMapping("/patientProfile/{pid}")
	public String sortPatients(@PathVariable("pid") String pid, Model model) {
		
		Patient p = patrepo.getOne(Integer.parseInt(pid));
		Doctor d = (Doctor) Hibernate.unproxy(systemService.getLoggedUser());
		List<Exam> exams = erepo.findAll();
		
		List<Integer> examIds = new ArrayList<Integer>();
		for(Exam e : exams) {
			if(d.getId() == e.getDoctor().getId()) {
				examIds.add(e.getId());	//izdvojeni pregledi sa odgovarajucim DrID
			}
		}
		//System.out.println(examIds);
		model.addAttribute("patient",p);
		model.addAttribute("examIds",examIds);
		
		return "patient_profile";
		
	}
	
	@RequestMapping(value = "/patientProfile/updateMedRec",
			method = RequestMethod.POST)
	public @ResponseBody String updateMedRec(@RequestBody Map<String,String> payload) {
		
		Patient p = patrepo.getOne(Integer.parseInt(payload.get("patientId")));
		MedicalRecord med = p.getMedrec();
		med.setBloodType(payload.get("bloodType"));
		med.setDiopter(payload.get("dopter"));
		med.setHeight(Integer.parseInt(payload.get("height")));
		med.setWeight(Integer.parseInt(payload.get("weight")));
		

		
		System.out.println(med.getDiopter());
		medrepo.saveAndFlush(med);
		
		
		return "patient_profile";
		
	}
	@RequestMapping(value = "/patientProfile/getRecords/{pid}",
			method = RequestMethod.GET)
	public @ResponseBody Map<String,String> getRecords(@PathVariable("pid") String pid) {
		System.out.println(pid);
		
		Patient p = patrepo.getOne(Integer.parseInt(pid));
		
		Map<String,String> medrecMap = new HashMap<String,String>();
		
		medrecMap.put("bloodType",p.getMedrec().getBloodType());
		medrecMap.put("dopter",p.getMedrec().getDiopter());
		medrecMap.put("height",String.valueOf(p.getMedrec().getHeight()));
		medrecMap.put("weight",String.valueOf(p.getMedrec().getWeight()));
		
		
		
		return medrecMap;
		
	}
	@RequestMapping(value = "/patientProfile/getExamTypeName/{pid}",method = RequestMethod.GET)
	public @ResponseBody Map<String,String> getExamTypeName(@PathVariable("pid") String pid){
		
		
		//Doctor dr = drepo.getOne(Integer.parseInt(pid));
			
		//Map<String,String> exams = new HashMap<String,String>();
		Integer e = erepo.findById(Integer.parseInt(pid)).get().getType().getId(); //uzeo sam id od Exam type
		String name = etyperepo.findById(e).get().getName(); // name 
		
		List<Prescription> prescriptions = prescriptionRepo.findAll();
		//System.out.println(prescriptions);
		Prescription e2 = null;
		
		for(Prescription p : prescriptions) {
			
			if(p.getExam().getId() == Integer.parseInt(pid)) {
				e2 = p;
				break;
			}
		}
		String t = e2 == null ? "" : e2.getTherapy();
		Map<String,String> mapType = new HashMap<String,String>();
		mapType.put("name", name);
		mapType.put("therapy", t);
		return mapType;
		
	}
	
	@RequestMapping(value = "/patientProfile/getDiagnosis",method = RequestMethod.POST)
	public @ResponseBody String getDiagnosis(@RequestBody Map<String,String> payload){
		
		Doctor d = (Doctor) Hibernate.unproxy(systemService.getLoggedUser());
		ReportInfo repInfo = new ReportInfo();
		
		repInfo.setInfo(payload.get("info"));
		repInfo.setExamType(payload.get("examType"));
		repInfo.setTherapy(payload.get("therapy"));
		//repInfo.getDoctor().setId(d.getId());
		
		reportInforepo.saveAndFlush(repInfo);
		
		return "ok";
	}
	
	
	
	
	
}
