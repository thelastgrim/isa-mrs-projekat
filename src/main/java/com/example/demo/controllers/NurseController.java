package com.example.demo.controllers;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.NurseRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.model.Nurse;
import com.example.demo.model.Patient;
import com.example.demo.services.NurseService;
import com.example.demo.services.SystemService;

@Controller
@PreAuthorize("hasAuthority('Nurse')")
public class NurseController {
	
	@Autowired
	NurseRepo nrepo;
	
	@Autowired
	PatientRepo patrepo;
	
	@Autowired
	SystemService systemSerive;
	
	@Autowired
	private NurseService nurseService;
	
	/*@RequestMapping("nursePage")
	public String clicnicAd(Model model, HttpSession session) {
		
		Nurse nurse = (Nurse) Hibernate.unproxy(systemSerive.getLoggedUser());
		
		model.addAttribute("types",nurse.getClinic().getTypes());
		model.addAttribute("nurse",nurse);
		model.addAttribute("patients",nurseService.getClinicPatients(nurse.getClinic()));
		
		return "nurse_home";
	}
	*/
	@RequestMapping("nurseSortPatients")
	public String nurseSortPatients(@RequestBody Map<String,String> payload) {
		
		return nurseService.getSortedPatients(payload);
	}
	
	@RequestMapping(value = "nurseReqLeave",method = RequestMethod.POST)
	public @ResponseBody String nurseReqLeave(@RequestBody Map<String,String> payload) {
		
		nurseService.saveReqLeave(payload);
		return "1";
	}
	
	/*@RequestMapping("/patientProfile/{pid}")
	public String sortPatients(@PathVariable("pid") String pid, Model model) {
		
		Patient p = patrepo.getOne(Integer.parseInt(pid));
		
		model.addAttribute("patient",p);
		
		return "patient_profile";
		
	}
	*/
	
	
}
