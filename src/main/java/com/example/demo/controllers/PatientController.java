package com.example.demo.controllers;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.PatientRepo;
import com.example.demo.model.Patient;
import com.example.demo.services.PatientService;
import com.example.demo.services.SystemService;

@Controller
@PreAuthorize("hasAuthority('Patient')")
public class PatientController {
	
	@Autowired
	PatientRepo prepo;

	@Autowired
	private PatientService patientService;
	@Autowired
	private SystemService systemService;	

	@RequestMapping("patient")
	public String fetchPatient(Model model, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser()); 
		model = patientService.getPatientPageData(model, p);
		return "patient_home";
	}
	
	@RequestMapping("appointmentDetails")
	@ResponseBody
	public String fetchAppointmentDetails(@RequestBody Map<String, String> payload) {
		return patientService.getAppointmentDetails(payload);
	}
	
	@RequestMapping("filterDiag")
	@ResponseBody
	public String fetchsortedDiagoses(@RequestBody Map<String, String> payload, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		return patientService.getSortedDiagnoses(payload,p);
	}
	
	@RequestMapping("rate")
	@ResponseBody
	public String rate(@RequestBody Map<String, String> payload, HttpSession session) {

		Integer id = Integer.parseInt(payload.get("id"));
		Integer value = Integer.parseInt(payload.get("value"));
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		if (payload.get("type").equals("c")) {
			patientService.rateClinic(id, value, p);
			return "done";
		}else if(payload.get("type").equals("d")) {
			patientService.rateDoctor(id, value, p);
			return "done";
		}
		return null;
	}
	
	@RequestMapping("sortCRatings")
	@ResponseBody
	public String fetchSortedClinicsRatings(@RequestBody Map<String, String> payload, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		return patientService.getSortedClinicsRatings(payload.get("sorter"), p);
	}
	
	@RequestMapping("sortDRatings")
	@ResponseBody
	public String fetchSortedDoctorsRatings(@RequestBody Map<String, String> payload, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		return patientService.getSortedDoctorsRatings(payload.get("sorter"), p);
	}
	
	@RequestMapping("filterUpcoming")
	@ResponseBody
	public String fetchSortedAppointments(@RequestBody Map<String, String> payload, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		return patientService.getSortedAppointments(payload.get("sorter"), payload.get("type"), p);
	}

	@RequestMapping("fetchProfile")
	@ResponseBody
	public String fetchProfile(HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		return patientService.getPatientProfile(p);
	}
	
	@RequestMapping("updateProfile")
	@ResponseBody
	public String updateProfile(@RequestBody Map<String, String> payload, HttpSession session) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());
		patientService.updatePatientProfile(payload, p);
		return "Profile updated!";
	}

}
