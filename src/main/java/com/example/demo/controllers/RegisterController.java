package com.example.demo.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.RequestRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.model.Address;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqRegistration;
import com.example.demo.model.RequestType;
import com.example.demo.model.STATUS;
import com.example.demo.model.User;

@Controller
public class RegisterController {
	
	@Autowired
	RequestRepo repo;
	
	@Autowired
	UserRepo urepo;
	
	@RequestMapping(value = "/registerUser",
			method = RequestMethod.POST)
	public @ResponseBody String register(@RequestBody Map<String, String> payload) {
		
		if(urepo.findByeMail(payload.get("email")) != null) {
			
			return "0";
		}
		
		
		
		ReqRegistration rr = new ReqRegistration();
		
		Address a = new Address();
		a.setCountry(payload.get("country"));
		a.setCity(payload.get("city"));
		a.setStreetName(payload.get("street"));
		
		Patient u = new Patient();

		u.setAddress(a);
		u.setFirstName(payload.get("firstName"));
		u.setLastName(payload.get("lastName"));
		u.setPhoneNUmber(payload.get("phoneNumber"));
		u.seteMail(payload.get("email"));
		u.setPassword(payload.get("pass"));
		u.setSsn(payload.get("ssn"));
		u.setStatus(STATUS.PENDING);
		
		Date date = new Date(); 
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		rr.setArrived(formatter.format(date).toString());
	
		rr.setSender(u);
		rr.setType(RequestType.REGISTER);
		rr.setSeen(false);
		
		try {
			repo.saveAndFlush(rr);
		} catch (ConstraintViolationException e) {
			return "2";
		}
		
		
		
		return "1";
	}
}
