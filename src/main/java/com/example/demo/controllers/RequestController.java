package com.example.demo.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.RequestRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.ReqRegistration;
import com.example.demo.model.User;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

@Controller
public class RequestController {
	
	@Autowired
	RequestRepo rrepo;
	
	@Autowired
	UserRepo urepo;
	
	@RequestMapping("/request/{id}")
	@ResponseBody
	public String Test(@PathVariable("id")String id) {
		
		ReqRegistration r = rrepo.findById(Integer.parseInt(id)).get();
		r.setSeen(true);
		rrepo.saveAndFlush(r);
		
		return rrepo.findById(Integer.parseInt(id)).get().toString();
	}
	
	//za update requesta
	@RequestMapping("updateRequest")
	@ResponseBody
	public void updateRequest(@RequestBody Map<String, String> payload) {
		ReqRegistration r = rrepo.findById(Integer.parseInt(payload.get("id"))).get();
		r.setSeen(true);
		rrepo.saveAndFlush(r);
	}
	
	
	@RequestMapping("processRequest/{action}/{msg}")
	@ResponseBody
	public String processRequest(@PathVariable("action") String action, @PathVariable("msg") String msg, @RequestBody Map<String, List<Integer>> payload) {
		
		
		SendEmail se = new SendEmail();
		
		if (action.equals("accept")) {
			for (Integer id : payload.get("id")) {
				Optional<ReqRegistration> r = rrepo.findById(id);
				
				User u = r.get().getSender();
				
				//obrise zahtev
				rrepo.delete(r.get());
				
				//posalje mejl
				new Thread(new Runnable() {
				    public void run() {
				    	se.sendRegistrationConfirmation(u);	
				    }
				}).start();

			}
				
		}else if (action.equals("decline")) {
			for (Integer id : payload.get("id")) {
				Optional<ReqRegistration> r = rrepo.findById(id);
				
				User u = r.get().getSender();
					
				new Thread(new Runnable() {
				    public void run() {
				    	se.sendRegistrationRejection(u, msg);
				    }
				}).start();

				rrepo.delete(r.get());
				urepo.delete(u);
				
			}
		}
		
		
		return "Success!";
	}

}
