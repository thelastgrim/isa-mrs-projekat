package com.example.demo.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.ReservationRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.Appointment;
import com.example.demo.model.Exam;
import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.RequestType;
import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.example.demo.services.SystemService;

@Controller
public class SystemController {
	
	@Autowired
	PatientRepo prepo;
	
	@Autowired
	ExamRepo erepo;
	
	@Autowired
	DoctorRepo drepo;
	
	@Autowired
	ExamTypeRepo etrepo;
	
	@Autowired
	ClinicRepo crepo;
	
	@Autowired
	RequestAppoRepo rarepo;
	
	@Autowired
	ReservationRepo rrepo;
	
	@Autowired
	RoomRepo rorepo;
	
	@Autowired
	AppointmentRepo arepo;
	
	@Autowired
	private SystemService systemService;
	
	

	@RequestMapping("qs")
	@ResponseBody
	public String fetchQuickSchedule(@RequestBody Map<String, String> payload) {
		
		//AUTOMATSKO ZAKAZIVANJE BEZ REQUESTA
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());	
		if(systemService.quickSchedule(p, Integer.parseInt(payload.get("ex_id")))) {
			System.out.println("mere");
			return "1";
		}else {
			System.out.println("nemere");
			return "0";
		}
		
	}
	
	@RequestMapping("processPatientAnswer/{answer}")
	public String fetchProcessPatientAnswer(@PathVariable("answer") String _answer, Model model) {
		String msg;
		try {
			msg = systemService.proccessPatientAnswer(_answer);
		} catch (Exception e) {
			msg = "Unauthorized operation.";
		}
		
		model.addAttribute("msg", msg);
		return "default_appointment";
	}
	
	@RequestMapping("cancelAppointment")
	@ResponseBody
	public String fetchCancelAppointment(@RequestBody Map<String, String> payload) {
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());	
		String msg = systemService.cancelAppointment(payload, p);
		
		return msg;
			
	}
	
	@RequestMapping("scheduleAppointment")
	@ResponseBody
	public String fetchScheduleAppointment(@RequestBody Map<String, String> payload) {
		
		Patient p = (Patient) Hibernate.unproxy(systemService.getLoggedUser());	
		
		String msg = systemService.scheduleAppointment(payload, p);
		
		return msg;
	}
}
