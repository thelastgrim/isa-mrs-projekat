package com.example.demo.controllers;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Patient;
import com.example.demo.model.User;
import com.example.demo.services.SystemService;
import com.example.demo.services.UserService;



@Controller
public class UserController {
	
	@Autowired
	SystemService systemService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping("fetchUserProfile")
	@ResponseBody
	public String fetchProfile() {
		User u = (User) Hibernate.unproxy(systemService.getLoggedUser());
		return userService.getUserProfile(u);
	}
	
	@RequestMapping("updateUserProfile")
	@ResponseBody
	public String updateProfile(@RequestBody Map<String, String> payload, HttpSession session) {
		User u = (User) Hibernate.unproxy(systemService.getLoggedUser());
		userService.updateUserProfile(payload, u);
		return "Profile updated!";
	}

}
