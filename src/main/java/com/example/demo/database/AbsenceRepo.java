package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Absence;

public interface AbsenceRepo extends JpaRepository<Absence,Integer> {

}
