package com.example.demo.database;


import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.model.Address;

public interface AddressRepo  extends JpaRepository<Address, Integer>{
	
}
