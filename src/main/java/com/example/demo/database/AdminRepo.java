package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Admin;

public interface AdminRepo extends JpaRepository<Admin, Integer>{
	public List<Admin> findAllByClinic_Idc(Integer idc);

}
