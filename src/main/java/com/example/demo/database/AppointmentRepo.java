package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.MedicalRecord;



public interface AppointmentRepo extends JpaRepository<Appointment, Integer>{
	public List<Appointment> findByHeldFalseAndReservationIsNotNullAndMedRecord(MedicalRecord medRec);

	public List<Appointment> findByHeldFalseAndMedRecord(MedicalRecord medRec);
	
	public List<Appointment> findByHeldTrueAndMedRecord(MedicalRecord medRec);
	
	public List<Appointment> findByHeldFalseAndClinic(Clinic clinic);
}
