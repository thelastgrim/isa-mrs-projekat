package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.model.Clinic;

@RepositoryRestResource(collectionResourceRel = "clinics",path="clinics")
public interface ClinicRepo extends JpaRepository<Clinic, Integer>{
	
	public Clinic findByName(String name);
	public Clinic findByAddress(String address);
	public Clinic findByDescription(String description);
	
	public List<Clinic> findByOrderByNameDesc();
	public List<Clinic> findByOrderByNameAsc();
	
	
}
