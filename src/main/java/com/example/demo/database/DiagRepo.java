package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


import com.example.demo.model.Diagnosis;

@RepositoryRestResource(collectionResourceRel = "addDiagnosis",path="addDiagnosis")
public interface DiagRepo extends JpaRepository<Diagnosis, Integer>{
	
	public DiagRepo findById(int id);
	
}
