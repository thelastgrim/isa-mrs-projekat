package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Doctor;


public interface DoctorRepo extends JpaRepository<Doctor, Integer> {
	public List<Doctor> findAllByClinic_Idc(Integer idc);
	public List<Doctor> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String firstName, String lastName);
}
