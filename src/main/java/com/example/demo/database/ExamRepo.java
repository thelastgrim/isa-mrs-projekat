package com.example.demo.database;

import java.util.Date;
import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.MedicalRecord;


public interface ExamRepo extends JpaRepository<Exam, Integer>{
	
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("select e from Exam e where e.id = :id")
	Exam findByIdForWrite(@Param("id") Integer id);
	
	public List<Exam> findByDateTime(Date dateTime);
	
	public List<Exam> findByMedRecord(MedicalRecord medrec);
	
	
	//BRZI PREGLEDI = ima rezervaciju (soba)
	public List<Exam> findByHeldFalseAndMedRecordIsNullAndReservationIsNotNullAndClinic(Clinic clinic);
	
	//TERMINI = nema rezervaciju 
	
	public List<Exam> findByHeldFalseAndMedRecordIsNullAndReservationIsNullAndClinic(Clinic clinic);
	
	
	//public List<Exam> findByHeldFalseAndReservationIsNotNullAndDoctor(Doctor doctor);
	
	public List<Exam> findByHeldFalseAndDoctor(Doctor doctor);

	
	



}


