package com.example.demo.database;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Clinic;
import com.example.demo.model.ExamType;



public interface ExamTypeRepo extends JpaRepository<ExamType, Integer> {
	
	public Set<ExamType> findAllByClinic_idc(Integer idc);
	public List<ExamType> findAllByClinic_idcAndNameContainingIgnoreCase(Integer idc, String name);
	public List<ExamType> findAllByNameContainingIgnoreCase(String name);

	public ExamType findByName(String name);
}
