package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.MedicalRecord;

public interface MedRecRepo extends JpaRepository<MedicalRecord, Integer>{
	
	public MedicalRecord findByBloodType(String bloodType);
	public MedicalRecord findByDiopter(String diopter);
	public MedicalRecord findByHeight(String height);
	public MedicalRecord findByWeight(String weight);
	
}
