package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.Nurse;

public interface NurseRepo extends JpaRepository<Nurse, Integer> {
	public List<Nurse> findAllByClinic_Idc(Integer idc);
	public List<Nurse> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String firstName, String lastName);
}
