package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;


public interface PatientRepo extends JpaRepository<Patient, Integer>{
	
	public Patient findById(String id);

	Patient findByMedrec(MedicalRecord medrec);
	
}
