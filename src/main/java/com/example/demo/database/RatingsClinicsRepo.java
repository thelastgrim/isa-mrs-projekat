package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Clinic;
import com.example.demo.model.Patient;
import com.example.demo.model.Ratings;
import com.example.demo.model.RatingsClinic;

public interface RatingsClinicsRepo  extends JpaRepository<RatingsClinic, Integer>{
	
	/*
	@Query(value = "SELECT AVG(rating) WHERE CLINIC_IDC = ?1",nativeQuery = true)
	public float getClinicRating(int clinic_idc);

	*/
	
	public List<RatingsClinic> findByPatient(Patient patient);
	
	public List<RatingsClinic> findByPatientOrderByRatingAsc(Patient patient);
	public List<RatingsClinic> findByPatientOrderByRatingDesc(Patient patient);
	
	public RatingsClinic findByPatientAndClinic(Patient patient, Clinic clinic);
	
	public List<RatingsClinic> findByClinic(Clinic c);
	
}
