package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Doctor;
import com.example.demo.model.Patient;
import com.example.demo.model.RatingsDoctor;

public interface RatingsDoctorsRepo extends JpaRepository<RatingsDoctor, Integer>{

	public List<RatingsDoctor> findByPatient(Patient patient);
	
	public List<RatingsDoctor> findByPatientOrderByRatingAsc(Patient patient);
	public List<RatingsDoctor> findByPatientOrderByRatingDesc(Patient patient);
	
	public RatingsDoctor findByPatientAndDoctor(Patient patient, Doctor doctor);
	
	public List<RatingsDoctor> findByDoctor(Doctor doctor);
}
