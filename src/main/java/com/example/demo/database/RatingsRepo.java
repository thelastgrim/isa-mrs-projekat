package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Patient;
import com.example.demo.model.Ratings;

public interface RatingsRepo  extends JpaRepository<Ratings, Integer>{

	public List<Ratings> findByPatient(Patient patient);
	
}
