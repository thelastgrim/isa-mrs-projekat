package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Doctor;
import com.example.demo.model.ReportInfo;

public interface ReportInfoRepo extends JpaRepository<ReportInfo, Integer> {
	
	//public String findAllReportsByDoctorid(Doctor doctor);

}
