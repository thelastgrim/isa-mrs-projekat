package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Clinic;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.RequestType;


public interface RequestAppoRepo extends JpaRepository<ReqAppointment, Integer> {

	
	public List<ReqAppointment> findByTypeAndAppointmentClinicAndAppointmentReservationIsNull(RequestType type, Clinic clinic);
}
