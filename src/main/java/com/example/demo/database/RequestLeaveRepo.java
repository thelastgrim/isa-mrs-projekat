package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ReqLeave;

public interface RequestLeaveRepo  extends JpaRepository<ReqLeave, Integer> {

}
