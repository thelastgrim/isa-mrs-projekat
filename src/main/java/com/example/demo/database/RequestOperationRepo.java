package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Clinic;
import com.example.demo.model.ReqOperation;
import com.example.demo.model.RequestType;

public interface RequestOperationRepo extends JpaRepository<ReqOperation, Integer>  {

	
	
}
