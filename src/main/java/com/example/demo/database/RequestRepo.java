package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.model.Clinic;
import com.example.demo.model.ReqRegistration;


public interface RequestRepo extends JpaRepository<ReqRegistration, Integer>{

	
}
