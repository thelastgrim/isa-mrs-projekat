package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Clinic;
import com.example.demo.model.Patient;
import com.example.demo.model.Reservation;

public interface ReservationRepo extends JpaRepository<Reservation, Integer> {
	
	public List<Reservation> findByPatient(Patient patient);
//	public List<Reservation> findByRoomAndAppointmentDatetime(Date date);
	

}
