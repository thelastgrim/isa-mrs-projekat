package com.example.demo.database;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.model.Room;


@RepositoryRestResource(collectionResourceRel = "addR",path="addR")
public interface RoomRepo extends JpaRepository<Room,Integer> {
	
	public List<Room> findAllByClinic_Idc(Integer idc);
	public Room findByNumberAndName(int number, String name);
	//public List findByNumberAndNameAndRessAppointmentDateTime(Date date);
	public List<Room> findByNameContainingIgnoreCase(String name);
	public Room findByNumber(int number);

	@Query(value = "select * from Rooms where cast(number as char) like CONCAT('%',:number,'%')", nativeQuery = true)
	public List<Room> findByNumberContaining(@Param("number") String number);
	

}
