package com.example.demo.database;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Doctor;

public interface SurgeryDoctorRepo extends JpaRepository<Doctor, Integer>{

}
