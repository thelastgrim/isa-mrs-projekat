package com.example.demo.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Surgery;

public interface SurgeryRepo extends JpaRepository<Surgery, Integer>{

	public List<Surgery> findByMedRecord(MedicalRecord medrec);
}
