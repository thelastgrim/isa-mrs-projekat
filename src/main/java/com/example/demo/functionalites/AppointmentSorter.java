package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Exam;
import com.example.demo.model.Surgery;



public class AppointmentSorter {
	
	public static List<Appointment> sort(List<Appointment> appointments, String type) {
		
		switch (type) {
		case "dateTimeDown":
			return sortByDateDown(appointments);	
			
		case "dateTimeUp":
			return sortByDateUp(appointments);
		
		case "clinicDown":
			return sortByClinicDown(appointments);
			
		case "clinicUp":
			return sortByClinicUp(appointments);
			
		case "typeDown":
			return sortByTypeDown(appointments);
			
		case "typeUp":
			return sortByTypeUp(appointments);
			
		case "nameDown":
			return sortByNameDown(appointments);
			
		case "nameUp":
			return sortByNameUp(appointments);
			
		case "doctorUp":
			return sortByDoctorUp(appointments);
			
		case "doctorDown":
			return sortByDoctorDown(appointments);
			
		case "diagNameDown":
			return sortByDiagNameDown(appointments);
		
		case "diagNameUp":
			return sortByDiagNameUp(appointments);
		default:
			break;
		}
		
		
		return null;
	}
	
	public static List<Appointment> sortByDiagNameUp(List<Appointment> appointments){
			
			Collections.sort(appointments, new Comparator<Appointment>() {
			    @Override
			    public int compare(Appointment a1, Appointment a2) {
			    	
		        return (((Exam) a1).getDiagnosis().getLatin()).compareToIgnoreCase(((Exam) a2).getDiagnosis().getLatin());
		    }
		});
		
		return appointments;
	}
	
	public static List<Appointment> sortByDiagNameDown(List<Appointment> appointments){
		
		Collections.reverse(sortByDiagNameUp(appointments));
	
		return appointments;
	}
	
	
	public static List<Appointment> sortByDoctorUp(List<Appointment> appointments){
		
		Collections.sort(appointments, new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		    	String n1 = null;
		    	String n2 = null;
		    	if (a1 instanceof Exam) {
		    		n1 = ((Exam) a1).getDoctor().getFirstName() + ((Exam) a1).getDoctor().getLastName();
		    	}else {
		    		n1 = ((Surgery) a1).getDoctors().toString().replace("[", "").replace("]", "");
		    		System.out.println(n1);
		    	}
		    	
		    	if(a2 instanceof Exam) {
		    		n2 = ((Exam) a2).getDoctor().getFirstName() + ((Exam) a2).getDoctor().getLastName();
		    	}
		    	else {
		    		n2 = ((Surgery) a1).getDoctors().toString().replace("[", "").replace("]", "");
		    	}

		        return n1.compareToIgnoreCase(n2);
		    }
		});
		
		return appointments;
	}
	
	public static List<Appointment> sortByDoctorDown(List<Appointment> appointments){
		
		Collections.reverse(sortByDoctorUp(appointments));
		
		return appointments;
	}


	
	public static List<Appointment> sortByDateUp(List<Appointment> appointments){
		
		Collections.sort(appointments, new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		    	
		    	long epoch1 = a1.getDateTime().getTime();
		    	long epoch2 = a2.getDateTime().getTime();
		    	
		        return (int) (epoch1-epoch2);
		    }
		});
		
		return appointments;
	}
	
	public static List<Appointment> sortByDateDown(List<Appointment> appointments){
		
		Collections.reverse(sortByDateUp(appointments));
		
		return appointments;
	}
	
	public static List<Appointment> sortByClinicUp(List<Appointment> appointments){
		
		Collections.sort(appointments, new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		        return a2.getClinic().getName().compareTo(a2.getClinic().getName());
		    }
		});
		
		return appointments;
	}
	
	public static List<Appointment> sortByClinicDown(List<Appointment> appointments){
		
		Collections.reverse(sortByClinicUp(appointments));
		
		return appointments;
	}
	
	public static List<Appointment> sortByTypeUp(List<Appointment> appointments){
		
		Collections.sort(appointments, new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		        return a2.getClass().getName().compareToIgnoreCase(a2.getClass().getName()) ;
		    }
		});
		
		return appointments;
	}
	
	public static List<Appointment> sortByTypeDown(List<Appointment> appointments){
		
		Collections.reverse(sortByTypeUp(appointments));
		
		return appointments;
	}
	
	public static List<Appointment> sortByNameUp(List<Appointment> appointments){
		
		Collections.sort(appointments, new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		    	if ((a1 instanceof Exam) && (a2 instanceof Exam)) {
		    		
		    		return (((Exam) a1).getType().getName()).compareToIgnoreCase(((Exam) a2).getType().getName());
		    	}
		    	else if (((a1 instanceof Exam) && (a2 instanceof Surgery))) {
					
		    		return (((Exam) a1).getType().getName()).compareToIgnoreCase(((Surgery) a2).getName());
				}
		    	else if (((a1 instanceof Surgery) && (a2 instanceof Exam))) {
		    		
		    		return (((Surgery) a1).getName()).compareToIgnoreCase(((Exam) a2).getType().getName());
		    	}else {
		    		
		    		return (((Surgery) a1).getName()).compareToIgnoreCase(((Surgery) a2).getName());
		    	}
		    }
		});
		
	
		return appointments;
	}
	
	public static List<Appointment> sortByNameDown(List<Appointment> appointments){
		
		Collections.reverse(sortByNameUp(appointments));
		
		return appointments;
	}
}
