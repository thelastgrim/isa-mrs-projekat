package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.database.RatingsClinicsRepo;
import com.example.demo.model.Clinic;

public class ClinicSorter {

	public static List<Clinic> sort(List<Clinic> clinics, String type) {
		
		switch (type) {
		case "nameDown":
			return sortByNameDown(clinics);
			
		case "nameUp":
			return sortByNameUp(clinics);
			
		case "cityDown":
			return sortByCityDown(clinics);
			
		case "cityUp":
			return sortByCityUp(clinics);
			
		case "ratingDown":
			return sortByRatingDown(clinics);
					
		case "ratingUp":
			return sortByRatingUp(clinics);
		default:
			break;
		}
		
		
		return null;
	}
	
	public static List<Clinic> sortByNameUp(List<Clinic> clinics){
		
		Collections.sort(clinics, new Comparator<Clinic>() {
		    @Override
		    public int compare(Clinic c1, Clinic c2) {
		        return c1.getName().compareToIgnoreCase(c2.getName());
		    }
		});
		
		return clinics;
	}
	
	public static List<Clinic> sortByNameDown(List<Clinic> clinics){
		
		Collections.reverse(sortByNameUp(clinics));
		
		return clinics;
	}
	
	public static List<Clinic> sortByCityUp(List<Clinic> clinics){
		
		Collections.sort(clinics, new Comparator<Clinic>() {
		    @Override
		    public int compare(Clinic c1, Clinic c2) {
		        return c1.getAddress().getCity().compareToIgnoreCase(c2.getAddress().getCity());
		    }
		});
		
		
		return clinics;
	}
	
	public static List<Clinic> sortByCityDown(List<Clinic> clinics){

		Collections.reverse(sortByCityUp(clinics));
				
		return clinics;
	}
	
	public static List<Clinic> sortByRatingUp(List<Clinic> clinics){
		
		Collections.sort(clinics, new Comparator<Clinic>() {
		    @Override
		    public int compare(Clinic c1, Clinic c2) {
		    	
		        return Double.compare(c1.getRating(), c2.getRating());
		    }
		});
		
		return clinics;
	}
	
	public static List<Clinic> sortByRatingDown(List<Clinic> clinics){
		
		Collections.reverse(sortByRatingUp(clinics));
		
		return clinics;
	}
	
	
}
