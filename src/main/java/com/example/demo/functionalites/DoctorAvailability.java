package com.example.demo.functionalites;

import java.util.ArrayList;

import com.example.demo.model.Doctor;

public class DoctorAvailability {

		private Doctor doctor;
		
		//04:00, 4:20....
		private ArrayList<String> froms;

		public Doctor getDoctor() {
			return doctor;
		}

		public void setDoctor(Doctor doctor) {
			this.doctor = doctor;
		}

		public ArrayList<String> getFroms() {
			return froms;
		}

		public void setFroms(ArrayList<String> froms) {
			this.froms = froms;
		}

		public DoctorAvailability(Doctor doctor, ArrayList<String> froms) {
			super();
			this.doctor = doctor;
			this.froms = froms;
		}

		public DoctorAvailability() {
			super();
		}
}
