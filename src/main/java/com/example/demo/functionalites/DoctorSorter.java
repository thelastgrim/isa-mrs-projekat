package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Doctor;

public class DoctorSorter {
	
public static List<Doctor> sort(List<Doctor> doctors, String type) {
		
		switch (type) {
		case "nameDown":
			return sortByNameDown(doctors);
			
		case "nameUp":
			return sortByNameUp(doctors);
			
		case "ratingDown":
			return sortByRatingDown(doctors);
			
		case "ratingUp":
			return sortByRatingUp(doctors);
			
		default:
			break;
		}
		
		
		return null;
	}
	
	public static List<Doctor> sortByNameUp(List<Doctor> doctors){
		
		Collections.sort(doctors, new Comparator<Doctor>() {
		    @Override
		    public int compare(Doctor d1, Doctor d2) {
		        return d1.getName().compareToIgnoreCase(d2.getName());
		    }
		});
		
		return doctors;
	}
	
	public static List<Doctor> sortByNameDown(List<Doctor> doctors){
		
		Collections.reverse(sortByNameUp(doctors));
		
		return doctors;
	}
	
	public static List<Doctor> sortByRatingUp(List<Doctor> doctors){
		
		Collections.sort(doctors, new Comparator<Doctor>() {
		    @Override
		    public int compare(Doctor d1, Doctor d2) {
		    	
		        return Double.compare(d1.getRating(), d2.getRating());
		    }
		});
		
		return doctors;
	}
	
	public static List<Doctor> sortByRatingDown(List<Doctor> doctors){
		
		Collections.reverse(sortByRatingUp(doctors));
		
		return doctors;
	}
	
public static List<DoctorAvailability> sortAva(List<DoctorAvailability> doctors, String type) {
		
		switch (type) {
		case "nameDown":
			return sortByNameDownAva(doctors);
			
		case "nameUp":
			return sortByNameUpAva(doctors);
			
		case "ratingDown":
			return sortByRatingDownAva(doctors);
			
		case "ratingUp":
			return sortByRatingUpAva(doctors);
			
		default:
			break;
		}
		
		
		return null;
	}
	
	public static List<DoctorAvailability> sortByNameUpAva(List<DoctorAvailability> doctors){
		
		Collections.sort(doctors, new Comparator<DoctorAvailability>() {
		    @Override
		    public int compare(DoctorAvailability d1, DoctorAvailability d2) {
		        return d1.getDoctor().getName().compareToIgnoreCase(d2.getDoctor().getName());
		    }
		});
		
		return doctors;
	}
	
	public static List<DoctorAvailability> sortByNameDownAva(List<DoctorAvailability> doctors){
		
		Collections.reverse(sortByNameUpAva(doctors));
		
		return doctors;
	}
	
	public static List<DoctorAvailability> sortByRatingUpAva(List<DoctorAvailability> doctors){
		
		Collections.sort(doctors, new Comparator<DoctorAvailability>() {
		    @Override
		    public int compare(DoctorAvailability d1, DoctorAvailability d2) {
		    	
		        return Double.compare(d1.getDoctor().getRating(), d2.getDoctor().getRating());
		    }
		});
		
		return doctors;
	}
	
	public static List<DoctorAvailability> sortByRatingDownAva(List<DoctorAvailability> doctors){
		
		Collections.reverse(sortByRatingUpAva(doctors));
		
		return doctors;
	}

}
