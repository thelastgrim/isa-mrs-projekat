package com.example.demo.functionalites;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Exam;

public class ExamSorter {
	
	public static List<Exam> sortByDateTime(List<Exam> exams){
		
		Collections.sort(exams, new Comparator<Exam>() {
		    @Override
		    public int compare(Exam e1, Exam e2) {
		    	long epoch1 = e1.getDateTime().getTime();
		    	long epoch2 = e2.getDateTime().getTime();
		    	
		        return (int) (epoch1-epoch2);
		    }
		});
		
		return exams;
	}
	
	private static LocalTime StringToTime(String time) {
		/*
		 * time format : hh:mm 
		 */
			String[] data = time.split(":");
			return LocalTime.of(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
		}
	
	
	public static ArrayList<String> sortBusyTime(ArrayList<String> busy_time){
		
		Collections.sort(busy_time, new Comparator<String>() {
		    @Override
		    public int compare(String t1, String t2) {
		    	
		    	LocalTime lt1 = StringToTime(t1.split("-")[0]);
		    	LocalTime lt2 = StringToTime(t2.split("-")[0]);
		    
		    	
		        return lt1.compareTo(lt2);
		    }
		});
		
		return busy_time;
	}

}
