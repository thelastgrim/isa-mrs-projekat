package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Clinic;
import com.example.demo.model.Patient;

public class PatientSorter {

public static List<Patient> sort(List<Patient> patients, String type) {
		
		switch (type) {
		case "nameDown":
			return sortByNameDown(patients);
			
		case "nameUp":
			return sortByNameUp(patients);
			
		case "lastNameDown":
			return sortByLastNameDown(patients);
			
		case "lastNameUp":
			return sortByLastNameUp(patients);
			
		case "ssnDown":
			return sortBySsnDown(patients);
					
		case "ssnUp":
			return sortBySsnUp(patients);
		default:
			break;
		}
		
		
		return null;
	}
	
	public static List<Patient> sortByNameUp(List<Patient> patients){
		
		Collections.sort(patients, new Comparator<Patient>() {
		    @Override
		    public int compare(Patient p1, Patient p2) {
		        return p1.getFirstName().compareToIgnoreCase(p2.getFirstName());
		    }
		});
		
		return patients;
	}
	
	public static List<Patient> sortByNameDown(List<Patient> patients){
		
		Collections.reverse(sortByNameUp(patients));
		
		return patients;
	}
	
	public static List<Patient> sortByLastNameUp(List<Patient> patients){
		
		Collections.sort(patients, new Comparator<Patient>() {
		    @Override
		    public int compare(Patient p1, Patient p2) {
		        return p1.getLastName().compareToIgnoreCase(p2.getLastName());
		    }
		});
		
		
		return patients;
	}
	
	public static List<Patient> sortByLastNameDown(List<Patient> patients){
	
		Collections.reverse(sortByLastNameUp(patients));
				
		return patients;
	}
	
	
public static List<Patient> sortBySsnUp(List<Patient> patients){
		
		Collections.sort(patients, new Comparator<Patient>() {
		    @Override
		    public int compare(Patient p1, Patient p2) {
		        return p1.getSsn().compareToIgnoreCase(p2.getSsn());
		    }
		});
		
		
		return patients;
	}
	
	public static List<Patient> sortBySsnDown(List<Patient> patients){
	
		Collections.reverse(sortBySsnUp(patients));
				
		return patients;
	}
	
	
	
}
