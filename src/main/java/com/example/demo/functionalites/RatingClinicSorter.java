package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Clinic;
import com.example.demo.model.RatingsClinic;

public class RatingClinicSorter {

	
	public static List<RatingsClinic> sort(List<RatingsClinic> ratings, String type) {
		
		switch (type) {
		case "clinicNameDown":
			return sortByNameDown(ratings);
			
		case "clinicNameUp":
			return sortByNameUp(ratings);
			
		
		default:
			break;
		
		}
		
		return ratings;
	}

	
	private static List<RatingsClinic> sortByNameDown(List<RatingsClinic> ratings) {
		
		Collections.sort(ratings, new Comparator<RatingsClinic>() {
		    @Override
		    public int compare(RatingsClinic rc1, RatingsClinic rc2) {
		        return rc1.getClinic().getName().compareToIgnoreCase(rc2.getClinic().getName());
		    }
		});
		
		return ratings;
		
	}
	
	private static List<RatingsClinic> sortByNameUp(List<RatingsClinic> ratings) {
		
		Collections.reverse(sortByNameDown(ratings));
		
		return ratings;
	}

}
