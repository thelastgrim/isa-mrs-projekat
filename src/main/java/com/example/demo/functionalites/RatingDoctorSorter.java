package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Exam;
import com.example.demo.model.RatingsClinic;
import com.example.demo.model.RatingsDoctor;

public class RatingDoctorSorter {

	public static List<RatingsDoctor> sort(List<RatingsDoctor> ratings, String type) {
			
		switch (type) {
			case "doctorNameDown":
				return sortByNameDown(ratings);
				
			case "doctorNameUp":
				return sortByNameUp(ratings);
				
			
			default:
				break;
			
			}
			
			return ratings;
		}
	
		
		private static List<RatingsDoctor> sortByNameDown(List<RatingsDoctor> ratings) {
			
			Collections.sort(ratings, new Comparator<RatingsDoctor>() {
			    @Override
			    public int compare(RatingsDoctor rd1, RatingsDoctor rd2) {
			        
			    	String n1 = rd1.getDoctor().getFirstName() + rd2.getDoctor().getLastName();
			    	String n2 = rd1.getDoctor().getFirstName() + rd2.getDoctor().getLastName();
			        return n1.compareToIgnoreCase(n2);
			    }
			});
			
			return ratings;
			
		}
		
		private static List<RatingsDoctor> sortByNameUp(List<RatingsDoctor> ratings) {
			
			Collections.reverse(sortByNameDown(ratings));
			
			return ratings;
		}
}
