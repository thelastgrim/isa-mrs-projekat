package com.example.demo.functionalites;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.demo.model.Clinic;
import com.example.demo.model.Reservation;

public class ReservationSorter {

public static List<Reservation> sortByDateUp(List<Reservation> reservations){
		
		Collections.sort(reservations, new Comparator<Reservation>() {
		    @Override
		    public int compare(Reservation r1, Reservation r2) {
		    	//mozda umesto ovoga moze sort funkcija u repo-u, tipa findAllOrderByDateTimeAsc, proveri za svaki slucaj
		        return r1.getAppointment().getDateTime().compareTo(r2.getAppointment().getDateTime());
		    }
		});
		
		return reservations;
	}
}
