package com.example.demo.functionalites;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.scheduling.annotation.Async;

import com.example.demo.model.Admin;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.RequestType;
import com.example.demo.model.Room;
import com.example.demo.model.Surgery;
import com.example.demo.model.User;

public class SendEmail {
	
	private Properties props;
	final String username = "mrs.clinic.center@gmail.com";
    final String password = "mrsisa.clinic.center";
    private String sender = "PrivateClinicCenter@gmail.com";
	//private String host = "localhost";
	final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	private Base64.Encoder encoder = Base64.getEncoder();  
	
	public SendEmail() {
		 props = System.getProperties();
		 props.setProperty("mail.smtp.host", "smtp.gmail.com");
	     props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	     props.setProperty("mail.smtp.socketFactory.fallback", "false");
	     props.setProperty("mail.smtp.port", "465");
	     props.setProperty("mail.smtp.socketFactory.port", "465");
	     props.put("mail.smtp.auth", "true");
	     props.put("mail.debug", "true");
	     props.put("mail.store.protocol", "pop3");
	     props.put("mail.transport.protocol", "smtp");
	} 
	
	@Async
	public void sendRegistrationConfirmation(User u) {

	    String toEncrypt = u.geteMail()+u.getId();
	   
	    EncryptDecrypt ed;
	    String encrypted = null;
		try {
			ed = new EncryptDecrypt();
			encrypted = ed.encrypt(toEncrypt).replace("/", "mrsicaclinic");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    try{
	    	Session session = Session.getDefaultInstance(props, 
                    new Authenticator(){
                       protected PasswordAuthentication getPasswordAuthentication() {
                          return new PasswordAuthentication(username, password);
                       }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         
	         message.setFrom(new InternetAddress(sender));  
	         
	         message.setRecipients(Message.RecipientType.TO, 
                     InternetAddress.parse(u.geteMail(),false));
	         
	         message.setSubject("Validate your account for the clinic center website");  
	         
	         message.setText("Visit this link (http://localhost:8080/accountActivation/"+encrypted+") to activate your account."); 	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }
	    catch (javax.mail.SendFailedException e) {
	    	System.out.println("ne valja mail");
		} 
	    catch (MessagingException mex) {mex.printStackTrace();}  
	   }
	
	@Async
	public void sendRegistrationRejection(User u, String reason) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                   new javax.mail.Authenticator(){
                      protected PasswordAuthentication getPasswordAuthentication() {
                         return new PasswordAuthentication(username, password);
                      }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         
	         message.setFrom(new InternetAddress(sender));  
	         
	         message.setRecipients(Message.RecipientType.TO, 
                    InternetAddress.parse(u.geteMail(),false));
	         
	         message.setSubject("Your registration request has been denied.");  
	         
	         message.setText("Reason:\n"+reason); 
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (MessagingException mex) {mex.printStackTrace();}  
	   }  
	
	@Async
	public void sendAppointmentConfirmation(ReqAppointment request, Patient patient) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                 new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                    }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress address = new InternetAddress(patient.geteMail());
	
	         message.setRecipient(Message.RecipientType.TO, address);
	         
	         message.setSubject("Appointment confirmation");
	         
	         String text = "<h2>Appointment details:<h2>";
	         
	         String accept_to_encrypt = "accept-"+request.getAppointment().getId()+"for"+patient.getId();
	         String decline_to_encrypt = "decline-"+request.getAppointment().getId()+"for"+patient.getId();
	    
	         String str_ace = encoder.encodeToString(accept_to_encrypt.getBytes()); 
	         String str_dec = encoder.encodeToString(decline_to_encrypt.getBytes());
	        
	         text = text + "<br> Date and time: "+request.getAppointment().getDateTime();
	         text = text + "<br> Type: "+((Exam) request.getAppointment()).getType().getName();
	         text = text + "<br> Price: "+((Exam) request.getAppointment()).getType().getPrice()+"$";
	         text = text + "<br> Doctor: "+((Exam) request.getAppointment()).getDoctor().getName();
	         
	         text = text + "<br><form method= \"POST\" action=\"http://localhost:8080/processPatientAnswer/" +str_ace +"\">\r\n" + 
	         		"        <input type=\"submit\" value=\"Accept\">\r\n" + 
	         		"    </form>";
	         
	         text = text + "<br><form method= \"POST\" action=\"http://localhost:8080/processPatientAnswer/" +str_dec +"\">\r\n" + 
		         		"        <input type=\"submit\" value=\"Decline\">\r\n" + 
		         		"    </form>";
	 
	         message.setContent(text, "text/html");
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (Exception mex) {mex.printStackTrace();}  
		
	}
	
	@Async
	public void sendReqAppointment(ReqAppointment request, Patient patient, List<Admin> admins) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                  new Authenticator(){
                     protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                     }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress[] address = new InternetAddress[admins.size()];
	         
	         for(int i =0;i< admins.size();i++) {
	        	 address[i] = new InternetAddress(admins.get(i).geteMail());
	         }
	         message.setRecipients(Message.RecipientType.TO, address);
	         
	         message.setSubject("Appointment request");
	         
	         String text = "<h2>New request for: " ;
	         if(request.getType().equals(RequestType.EXAM)) {
	        	 Exam ex = (Exam)request.getAppointment();
	        	 text = text + "<i>" +ex.getType().getName()+"</i>";
	         }else {
	        	 Surgery sur = (Surgery)request.getAppointment();
	        	 text = text  + "<i>" + "surgery - "+ sur.getName() +"</i>" ;
	         }
	       //  text = text + " on date: " + request.getAppointment().getDateTime() + "\r\nDoctor:\r\n      First name: " + request.getSender().getFirstName() + "\r\n      Last name: " + request.getSender().getLastName() + "\r\n      email: " + request.getSender().geteMail()     + "\r\n\r\nPatient:\r\n      First name: " + patient.getFirstName()+ "\r\n      Last name: " + patient.getLastName()+ "\r\n      email: " + patient.geteMail();
	         text = text + "<br>on date: " + "<i>" + request.getAppointment().getDateTime()+"</i>" + "</h2><br><b>Doctor:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>First name: </i>" + request.getSender().getFirstName() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Last name: </i>" + request.getSender().getLastName() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>email: </i>" + request.getSender().geteMail()     + "<br><br><b>Patient:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>First name: </i>" + patient.getFirstName()+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Last name: </i>" + patient.getLastName()+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>email: </i>" + patient.geteMail();

	        // message.setText(text); 
	         message.setContent(text, "text/html");
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (MessagingException mex) {mex.printStackTrace();}  
	   }
	
	@Async
	public void sendReservation(Patient patient, Doctor doctor,Exam exam, Room room, boolean accepted) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                 new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                    }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress[] address = new InternetAddress[1];
	         
	         //address[0] = new InternetAddress(patient.geteMail());
	         address[0] = new InternetAddress(doctor.geteMail());

	         message.setRecipients(Message.RecipientType.TO, address);
	        
	         String text = null;
	         if (accepted) {
	        	 message.setSubject("New appointment");
	        	 text = "<h2>New appointment for: ";
	         }else {
	        	 message.setSubject("Appointment cancellation");
	        	 text = "<h2>Appointment cancelled by patient for: ";
	         }
	         
	        	
	         text = text + "<i>" +exam.getType().getName()+"</i>";
	        
	         text = text + "<br>on date: " + "<i>" + exam.getDateTime()+"</i>" + "</h2><br><b>Doctor:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>First name: </i>" + doctor.getFirstName() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Last name: </i>" + doctor.getLastName() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>email: </i>" + doctor.geteMail()     + "<br><br><b>Patient:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>First name: </i>" + patient.getFirstName()+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Last name: </i>" + patient.getLastName()+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>email: </i>" + patient.geteMail() + "<br><br><b>Room:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Name: </i>" + room.getName()+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Number: </i>" +room.getNumber();
 
	         message.setContent(text, "text/html");	         
	         message.setSentDate(new Date());	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (MessagingException mex) {mex.printStackTrace();}  
	   }
	
	
	@Async
	public void sendLeaveConfirmation(User u, Date startDate, Date endDate) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                 new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                    }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress address = new InternetAddress(u.geteMail());
	
	         message.setRecipient(Message.RecipientType.TO, address);
	         
	         message.setSubject("Leave of absence request confirmation ");
	         
	         
	        
	         String text =  "<i> Dear "+ u.getFirstName() + " " + u.getLastName()+ "</i><br>";
	         text = text + "Your request for leave of absence from <b>"+ startDate +"</b> until <b>"+ endDate+"</b>";
	         text = text + " has been <b>approved.</b>";
	 
	         message.setContent(text, "text/html");
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (Exception mex) {mex.printStackTrace();}  
		
	}
	
	@Async
	public void sendLeaveRejection(User u, Date startDate, Date endDate, String reason) {
	     try{
	    	Session session = Session.getDefaultInstance(props, 
                 new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                    }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress address = new InternetAddress(u.geteMail());
	
	         message.setRecipient(Message.RecipientType.TO, address);
	         
	         message.setSubject("Leave of absence request denial ");
	         
	         
			System.err.println("REASON u mailu: " + reason);

	         String text =  "<i> Dear "+ u.getFirstName() + " " + u.getLastName()+ "</i><br>";
	         text = text + "Your request for leave of absence from <b>"+ startDate +"</b> until <b>"+ endDate+"</b>";
	         text = text + " has been <b>denied</b>.<br>";
	         text = text + "<i>Reason:<br>";
	         text = text + reason + "</i>";
	 
	         message.setContent(text, "text/html");
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (Exception mex) {mex.printStackTrace();}  
		
	}
	
	@Async
	public void sendQSConfirmation(Patient p, Exam e) {
		try{
	    	Session session = Session.getDefaultInstance(props, 
                 new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                    }});
	    	
	         MimeMessage message = new MimeMessage(session); 
	         message.setFrom(new InternetAddress(sender));  
	         
	         InternetAddress address = new InternetAddress(p.geteMail());
	
	         message.setRecipient(Message.RecipientType.TO, address);
	         
	         message.setSubject("Appointment successfully scheduled");
	         
	         String text = "<h2>Appointment details:<h2>";
	        
	        
	         text = text + "<br> Date and time: "+e.getDateTime();
	         text = text + "<br> Type: "+e.getType().getName();
	         text = text + "<br> Price: "+e.getType().getPrice()+"$";
	         text = text + "<br> Doctor: "+e.getDoctor().getName();
	         text = text + "<br> Address: "+e.getClinic().getAddress().getStreetName();
	         text = text + "<br> Room: "+e.getReservation().getRoom().getNumber();
	         
	         message.setContent(text, "text/html");
	         
	         message.setSentDate(new Date());
	         
	         // Send message  
	         Transport.send(message);  
	         System.out.println("message sent successfully....");  
	  
	      }catch (Exception mex) {mex.printStackTrace();}  
		
	}

	public void sendRequestNotification(ArrayList<Admin> admins) {
		 try{
		    	Session session = Session.getDefaultInstance(props, 
	                  new Authenticator(){
	                     protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                     }});
		    	
		         MimeMessage message = new MimeMessage(session); 
		         
		         message.setFrom(new InternetAddress(sender));  
		         
		         InternetAddress[] address = new InternetAddress[admins.size()];
		         
		         for(int i =0;i< admins.size();i++) {
		        	 address[i] = new InternetAddress(admins.get(i).geteMail());
		         }
		         message.setRecipients(Message.RecipientType.TO, address);
		         
		         message.setSubject("Appointment request");
		         
		         String text = "<h2>You have a new appointment request to proccess.<h2>" ;
		   
		         message.setContent(text, "text/html");
		         
		         message.setSentDate(new Date());
		         
		         // Send message  
		         Transport.send(message);  
		         System.out.println("message sent successfully....");  
		  
		      }catch (MessagingException mex) {mex.printStackTrace();}  
		
	}

	
	}
	

