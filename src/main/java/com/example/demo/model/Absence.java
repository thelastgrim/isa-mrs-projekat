package com.example.demo.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.print.Doc;

@Entity
@Table(name = "absence")
public class Absence {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = false)
	private Date start;
	@Column(nullable = false, unique = false)
	private Date end;

	
	public Absence() {
		
	}

	public Absence(Integer id, Date start, Date leave) {
		super();
		this.id = id;
		this.start = start;
		this.end = leave;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getLeave() {
		return end;
	}

	public void setLeave(Date leave) {
		this.end = leave;
	}

	

	

}
