package com.example.demo.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "appointments")
@Inheritance(strategy = InheritanceType.JOINED)
public class Appointment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = false)
	private Date dateTime;
	@Column(nullable = false, unique = false)
	private boolean held;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Reservation reservation;
	
	//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)-CHANGE FOR CHRON
	@ManyToOne(fetch = FetchType.LAZY)
	private MedicalRecord medRecord;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Clinic clinic;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isHeld() {
		return held;
	}

	public void setHeld(boolean held) {
		this.held = held;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public MedicalRecord getMedRecord() {
		return medRecord;
	}

	public void setMedRecord(MedicalRecord medRecord) {
		this.medRecord = medRecord;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", dateTime=" + dateTime + ", held=" + held + ", reservation=" + reservation.getId()
				+ ", medRecord=" + medRecord + ", clinic=" + clinic + "]";
	}

	

	
	
}
