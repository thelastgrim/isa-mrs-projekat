package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ccAdmins")
public class CCAdmin extends User {


	public CCAdmin() {
		super();
	}

	
	public CCAdmin(User u){
		setAddress(u.getAddress());
        seteMail(u.geteMail());
        setFirstName(u.getFirstName());
        setLastName(u.getLastName());
        setPassword(u.getPassword());
        setPhoneNUmber(u.getPhoneNUmber());
        setStatus(STATUS.VALID); 
	}

}
