package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "diagnoses")
public class Diagnosis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = true, updatable = false)
	private String code;
	@Column(nullable = false, unique = false)
	private String description;
	@Column(nullable = false, unique = true)
	private String latin;

	public Diagnosis() {
		super();
	}

	public Diagnosis(Integer id, String description, String latin) {
		super();
		this.id = id;
		this.description = description;
		this.latin = latin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLatin() {
		return latin;
	}

	public void setLatin(String latin) {
		this.latin = latin;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	//@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//private ClinicCenter clinicCenter;
	
	//@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//private Appointment appointment;
}
