package com.example.demo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;

@Entity
@Table(name = "doctors")
public class Doctor extends User{


	@Column(nullable =  false, unique = false)
	private String startTime;
	@Column(nullable =  false, unique = false)
	private String endTime;
	@Column(nullable =  false, unique = false)
	private double rating;
	
	public Doctor() {
		super();
	}
	public Doctor(User u){
        setAddress(u.getAddress());
        seteMail(u.geteMail());
        setFirstName(u.getFirstName());
        setLastName(u.getLastName());
        setPassword(u.getPassword());
        setPhoneNUmber(u.getPhoneNUmber());
        setStatus(STATUS.VALID); 
    }
	
	@ManyToMany()
	@JoinTable(
		name = "surgery_doctor",
		joinColumns = @JoinColumn(name = "doctor_id"),
		inverseJoinColumns = @JoinColumn(name = "surgery_id"))
	private Set<Surgery> surgeries = new HashSet<Surgery>();
	
	
	@ManyToMany()
	@JoinTable(
		name = "specializations",
		joinColumns = @JoinColumn(name = "doctor_id"),
		inverseJoinColumns = @JoinColumn(name = "exam_type_id"))
	private Set<ExamType> specializations = new HashSet<ExamType>();
	
	@OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Exam> exam = new HashSet<Exam>();
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Clinic clinic;
	
	@ManyToMany()
	@JoinTable(
		name = "absence_doctors",
		joinColumns = @JoinColumn(name = "doctor_id"),
		inverseJoinColumns = @JoinColumn(name = "absence_id"))
	private Set<Absence> absence = new HashSet<Absence>();

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	public Set<Absence> getAbsences() {
		return absence;
	}

	public void setAbsences(Set<Absence> absences) {
		this.absence = absences;
	}
	public Set<Exam> getExam() {
		return exam;
	}
	public void setExam(Set<Exam> exam) {
		this.exam = exam;
	}
	public Set<Surgery> getSurgeries() {
		return surgeries;
	}
	public void setSurgeries(Set<Surgery> surgeries) {
		this.surgeries = surgeries;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public Set<ExamType> getSpecializations() {
		return specializations;
	}
	public void setSpecializations(Set<ExamType> specializations) {
		this.specializations = specializations;
	}

	public String getName() {
		
		return getFirstName() +" " + getLastName();
	}
	
}
