package com.example.demo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "examTypes")
public class ExamType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = false)
	private String name;
	@Column(nullable = false, unique = false)
	private int duration;
	@Column(nullable = false, unique = false)
	private int price;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Clinic clinic;
	
	@ManyToMany(mappedBy = "specializations")
	private Set<Doctor> doctors = new HashSet<Doctor>();
	
	public ExamType() {
		
	}

	public ExamType(Integer id, String name, int duration, int price, Clinic clinic) {
		super();
		this.id = id;
		this.name = name;
		this.duration = duration;
		this.price = price;
		this.clinic = clinic;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}
	

	@Override
	public String toString() {
		return "ExamType [id=" + id + ", name=" + name + ", duration=" + duration + ", price=" + price + ", clinic="
				+ clinic + "]";
	}

	public Set<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}
}
