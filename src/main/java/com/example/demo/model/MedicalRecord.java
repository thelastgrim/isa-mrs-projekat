package com.example.demo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "medRecords")
public class MedicalRecord {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = true, unique = false)
	private String diopter;
	@Column(nullable = true, unique = false)
	private int weight;
	@Column(nullable = true, unique = false)
	private int height;
	@Column(nullable = true, unique = false)
	private String bloodType;
	
	//@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL) //kljuc je ujedno i strani kljuc zato nema "@Generated Value" kod @id
	//@MapsId
	@OneToOne(mappedBy = "medrec", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private Patient patient;
	
	@OneToMany(mappedBy = "medRecord", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Appointment> appointments = new HashSet<Appointment>();
	
	@OneToMany(mappedBy = "medRecord", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Drug> allergicTo = new HashSet<Drug>();
	
	public MedicalRecord(Integer id, String diopter, int weight, int height, String bloodType) {
		super();
		this.id = id;
		this.diopter = diopter;
		this.weight = weight;
		this.height = height;
		this.bloodType = bloodType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDiopter() {
		return diopter;
	}

	public void setDiopter(String diopter) {
		this.diopter = diopter;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	
	public Set<Drug> getAllergicTo() {
		return allergicTo;
	}

	public void setAllergicTo(Set<Drug> allergicTo) {
		this.allergicTo = allergicTo;
	}

	public MedicalRecord() {
		super();
	}

	@Override
	public String toString() {
		return "MedicalRecord [id=" + id + ",...";
	}

	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}
	
}
