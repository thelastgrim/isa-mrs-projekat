package com.example.demo.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity	
@Table(name = "nurses")
public class Nurse extends User{
	
	@Column(nullable =  false, unique = false)
	private String startTime;
	@Column(nullable =  false, unique = false)
	private String endTime;
	
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Clinic clinic;
	
	
	@OneToMany(mappedBy = "nurse", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Prescription> validated = new HashSet<Prescription>();
	
	
	@ManyToMany()
	@JoinTable(
			name = "absence_nurses",
			joinColumns = @JoinColumn(name = "nurse_id"),
			inverseJoinColumns = @JoinColumn(name = "absence_id"))
	private Set<Absence> absence = new HashSet<Absence>();


	public Nurse() {
		super();
	}

	public Nurse(String startTime, String endTime, Clinic clinic, Set<Prescription> validated, Set<Absence> absences) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.clinic = clinic;
		this.validated = validated;
		this.absence = absences;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	public Set<Prescription> getValidated() {
		return validated;
	}

	public void setValidated(Set<Prescription> validated) {
		this.validated = validated;
	}

	public Set<Absence> getAbsences() {
		return absence;
	}

	public void setAbsences(Set<Absence> absences) {
		this.absence = absences;
	}
}
