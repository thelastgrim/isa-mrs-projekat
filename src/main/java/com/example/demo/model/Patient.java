package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "patients")
public class Patient extends User{

	@Column(nullable = false, unique = true, updatable = false)
	private String ssn; //Patient Identification Number SOcial Security Number
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "medrec_id")
	private MedicalRecord medrec;

	public Patient(String ssn) {
		super();
		this.ssn = ssn;
	}
	
	public Patient(User u){
        setAddress(u.getAddress());
        seteMail(u.geteMail());
        setFirstName(u.getFirstName());
        setLastName(u.getLastName());
        setPassword(u.getPassword());
        setPhoneNUmber(u.getPhoneNUmber());
        setStatus(STATUS.VALID); 
    }
	
	public Patient() {
		super();
	}

	

	public MedicalRecord getMedrec() {
		return medrec;
	}

	public void setMedrec(MedicalRecord medrec) {
		this.medrec = medrec;
	}

	@Override
	public String toString() {
		return "Patient [pin=" + ssn + geteMail() + ", medrec=" + medrec + "]";
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	
	public List<Appointment> getUpcoming(List<Appointment> appointments){
		
		appointments.removeIf(a -> a.isHeld());
		
		return appointments;
	}

	
}
