package com.example.demo.model;

import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;


@Entity
public class ReqAppointment extends Request {
	
	@OneToOne(fetch= FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Appointment appointment;
	
	public ReqAppointment() {
		
	}

	public ReqAppointment(Integer id, RequestType type, Time sent, String arrived, boolean seen, User sender,
			Appointment appointment) {
		super(id, type, sent, arrived, seen, sender);
		this.appointment = appointment;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	/*@Override
	public String toString() {
		String ss = super.toString();
		return ss+  "  ReqAppointment [appointment=" + appointment + "]";
	}*/
	
	
	
	

	
}
