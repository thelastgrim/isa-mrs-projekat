package com.example.demo.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;

@Entity
public class ReqLeave extends Request{

	private Date startDate;
	private Date endDate;
	private LeaveType leaveType;
	
	public ReqLeave() {
	
	}
	public ReqLeave(Integer id, RequestType type, Time sent, String arrived, boolean seen, User sender, Date startDate,
			Date endDate, LeaveType leaveType) {
		super(id, type, sent, arrived, seen, sender);
		this.startDate = startDate;
		this.endDate = endDate;
		this.leaveType = leaveType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public LeaveType getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}
	
	
	
	
}
