package com.example.demo.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class ReqOperation extends Request{
	@OneToOne(fetch= FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Surgery operation;
	@Column(nullable = false, unique = false)
	private Date dateTime;
	public ReqOperation() {
	}

	public ReqOperation(Integer id, RequestType type, Time sent, String arrived, boolean seen, User sender,
			Surgery operation) {
		super(id, type, sent, arrived, seen, sender);
		this.operation = operation;
	}

	public Surgery getOperation() {
		return operation;
	}

	public void setOperation(Surgery operation) {
		this.operation = operation;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	
	
	

}
