package com.example.demo.model;

import java.sql.Time;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
//import javax.persistence.FetchType;
//import javax.persistence.OneToOne;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class ReqRegistration extends Request{
	
//	@OneToOne(fetch= FetchType.EAGER, cascade = CascadeType.PERSIST)
//	private User user;
	
	//@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@OneToOne(fetch= FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Request request;
	
//	@Column(name = "conformation_token")
//	private String conformationToken;
//	
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date date;
//	
//	public ReqRegistration(Integer id,RequestType type,Time sent,String arrived,boolean seen,User sender) {
//		super(id, type, sent, arrived, seen, sender);
//	}
//	
//	@OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
//    @JoinColumn(nullable = false, name = "users")
//    private User user;
//	
//	public ReqRegistration(User user) {
//		this.user = user;
//		date = new Date();
//		conformationToken = UUID.randomUUID().toString();
//	}
	
	
	
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public ReqRegistration() {
		super();
	}
	
}
