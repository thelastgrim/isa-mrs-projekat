package com.example.demo.model;

public enum RequestType {
	EXAM,
	OPERATION,
	REGISTER,
	LEAVE

}
