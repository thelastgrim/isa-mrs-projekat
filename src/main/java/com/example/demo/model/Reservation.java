package com.example.demo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "reservations")
public class Reservation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private Room room;
	//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST) - -CHANGE FOR CHRON
	@ManyToOne(fetch = FetchType.LAZY)
	private Patient patient;
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Appointment appointment;
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Surgery operation;
	
	@Version
	private long version;

	public Reservation() {
		super();
	}

	
	public Reservation(Room room, Patient patient, Appointment appointment) {
		super();
		this.room = room;
		this.patient = patient;
		this.appointment = appointment;
	}
	public Reservation(Integer id, Room room, Patient patient, Appointment appointment) {
		super();
		this.id = id;
		this.room = room;
		this.patient = patient;
		this.appointment = appointment;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}


	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

		
	public Surgery getOperation() {
		return operation;
	}


	public void setOperation(Surgery operation) {
		this.operation = operation;
	}


	@Override
	public String toString() {
		return "Reservation [id=" + id + ", room=" + room + ", appointment=" + appointment
				+ "]";
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}


	public long getVersion() {
		return version;
	}


	public void setVersion(long version) {
		this.version = version;
	}
	
	

}
