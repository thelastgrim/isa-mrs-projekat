package com.example.demo.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "rooms")
public class Room {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false, unique = false)
	private int number;
	@Column(nullable = false, unique = false)
	private String name;
	@Version
	private long version;

	public Room(Integer id, int number, String name, Set<Reservation> ress, Clinic clinic) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.ress = ress;
		this.clinic = clinic;
	}

	//@OneToMany(mappedBy = "room",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
			@OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
			private Set<Reservation> ress = new HashSet<Reservation>();
			
			//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
			@ManyToOne(fetch = FetchType.LAZY)
			private Clinic clinic;



	public Room(Integer id, int number, Set<Reservation> ress) {
		super();
		this.id = id;
		this.number = number;
		this.ress = ress;
	}

	public Room() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Set<Reservation> getRess() {
		return ress;
	}

	public void setRess(Set<Reservation> ress) {
		this.ress = ress;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Reservation> getRessByDate(Date date){
		Set<Reservation> allRess = this.ress;
		Set<Reservation>  ress = new HashSet<Reservation>();
		/*System.err.println("    Date in getRessByDate: " + date);
		System.err.println("    all room reservation: " );
		*/
		for(Reservation r: allRess) {
	//		System.err.println(r);
			if(getDateOnly(r.getAppointment().getDateTime()).equals(getDateOnly(date)) ) {//| date.before(r.getAppointment().getDateTime())
				ress.add(r);
			}
		}
		
		
		return ress;
	}
	
	private String getDateOnly(Date date) {
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		return formatter.format(date);
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	
}
