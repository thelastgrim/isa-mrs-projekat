package com.example.demo.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "surgeries")
public class Surgery extends Appointment{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToMany(mappedBy = "surgeries")
	private Set<Doctor> doctors = new HashSet<Doctor>();
	
	@Column(nullable = false, unique = false)
	private String name;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Reservation reservation;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Clinic clinic;
	
	
	public Surgery() {
		super();
	}
	

	public Clinic getClinic() {
		return clinic;
	}


	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}


	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


//	public Date getDateTime() {
//		return dateTime;
//	}
//
//
//	public void setDateTime(Date dateTime) {
//		this.dateTime = dateTime;
//	}


	public Set<Doctor> getDoctors() {
		return doctors;
	}


	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}



}
