package com.example.demo.scheduler;


import java.util.HashMap;
import java.util.Map;

import javax.persistence.OptimisticLockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.demo.database.PatientRepo;
import com.example.demo.model.Patient;
import com.example.demo.services.PatientService;
import com.example.demo.services.SystemService;



@Service
public class RatingScheduler {
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private PatientRepo prepo;
	@Autowired
	private SystemService systemService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/*
	@Scheduled(cron = "0,5 * * * * *")
	public void rate() {
		System.out.println("radi li ovo?");
		Patient p = prepo.getOne(9999);
		
		try {
			patientService.rateClinic(9999, 1, p);
		}
		catch (OptimisticLockException e) {
			logger.info("1 - Ocena u isto vreme");
		}
		catch (ObjectOptimisticLockingFailureException e) {
			logger.info("1 - Ocena u isto vreme2");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Scheduled(cron = "0,5 * * * * *")
	public void rate2() {
		Patient p = prepo.getOne(8004);
		
		try {
			patientService.rateClinic(9999, 5, p);
			logger.info("Drugi rejtovao");
		}
		catch (OptimisticLockException e) {
			logger.info("2 - Ocena u isto vreme");
		}
		catch (ObjectOptimisticLockingFailureException e) {
			logger.info("2 - Ocena u isto vreme2");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	
	/*
	 * QUICK APPO ZAKAZIVANJE
	 * promene u Appointment , REservations
	@Scheduled(cron = "0,10 * * * * *")
	public void quickAppo() {
		Patient p = prepo.getOne(9999);

		try {
			
			if(systemService.quickSchedule(p, 111113)) {
				System.out.println("PRVI USPEO");
			}else {
				System.out.println("prvi nije");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Scheduled(cron = "0,5 * * * * *")
	public void quickAppo2() {
		Patient p = prepo.getOne(8004);
		
		try {
			if(systemService.quickSchedule(p, 111113)) {
				System.out.println("drugi USPEO");
			}else {
				System.out.println("drugi nije");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	/*
	@Scheduled(cron = "0,10 * * * * *")
	public void quickAppo() {
		Patient p = prepo.getOne(9999);

		try {
			Map<String, String> payload = new HashMap<>();
			payload.put("clinic", "9999");
			payload.put("doctorId", "8000");
			payload.put("type", "Complete biochemical assessment of the cardiovascular system");
			payload.put("date", "2020-06-19");
			payload.put("time", "14:00");
			System.out.println(systemService.scheduleAppointment(payload, p));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Scheduled(cron = "0,5 * * * * *")
	public void quickAppo2() {
		Patient p = prepo.getOne(8004);
		
		try {
			Map<String, String> payload = new HashMap<>();
			payload.put("clinic", "9999");
			payload.put("doctorId", "8000");
			payload.put("type", "Complete biochemical assessment of the cardiovascular system");
			payload.put("date", "2020-06-19");
			payload.put("time", "14:00");
			System.out.println(systemService.scheduleAppointment(payload, p));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
}
