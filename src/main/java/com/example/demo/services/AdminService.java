package com.example.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.database.AbsenceRepo;
import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.NurseRepo;
import com.example.demo.database.RequestLeaveRepo;
import com.example.demo.database.ReservationRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.database.SurgeryRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.Absence;
import com.example.demo.model.Admin;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.ExamType;
import com.example.demo.model.Nurse;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqLeave;
import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.google.gson.Gson;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service
@Transactional(readOnly = true)
public class AdminService {
	@Autowired
	RequestLeaveRepo leaveRepo;
	@Autowired
	DoctorRepo drRepo;
	@Autowired
	AbsenceRepo absRepo;
	@Autowired
	NurseRepo nurRepo;
	@Autowired
	UserRepo uRepo;
	@Autowired
	ExamTypeRepo examRepo;
	@Autowired
	RoomRepo roomRepo;
	@Autowired
	SystemService systemService;
	@Autowired
	AppointmentRepo appoRepo;
	@Autowired
	ReservationRepo resRepo;
	@Autowired
	SurgeryRepo surgeryRepo;
	
	
	private static Gson g = new Gson();
	public void saveLReqResponse(Map<String, String> payload) {
		Integer reqId = Integer.parseInt(payload.get("reqId"));
		String response = payload.get("response");//accept or decline
		ReqLeave rl = leaveRepo.findById(reqId).get();
		SendEmail se = new SendEmail();
		Integer id = rl.getSender().getId();
		Doctor dr = drRepo.getOne(id);
		
		System.err.println("KLASA: " + rl.getSender().getClass().getSimpleName());
		if(response.equals("accept")) {
			Absence a = new Absence();
			a.setStart(rl.getStartDate());
			a.setLeave(rl.getEndDate());
			if(dr!= null) {//ako je doktor poslao mail njemu setujemo Odsustvo
		//		Doctor dr = drRepo.findById(rl.getSender().getId()).get();
				Set<Absence> abs = dr.getAbsences();
				System.err.println(" - -- - - -ABS kod dr: "+abs);
				abs.add(a);
				dr.setAbsences(abs);
				absRepo.saveAndFlush(a);
				drRepo.saveAndFlush(dr);
				System.err.println(" - -- - - -ABS after: "+dr.getAbsences());
			}else {//ako je m.sestra poslala mail njoj setujemo Odsustvo
				Nurse nur = nurRepo.findById(rl.getSender().getId()).get();
				Set<Absence> abs = nur.getAbsences();
				System.err.println(" - -- - - -ABS: "+abs);
				abs.add(a);
				nur.setAbsences(abs);
				absRepo.saveAndFlush(a);
				nurRepo.saveAndFlush(nur);
				System.err.println(" - -- - - -ABS after: "+nur.getAbsences());
			}
			
			//se.sendLeaveConfirmation(rl.getSender(), rl.getStartDate(), rl.getEndDate());
			
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendLeaveConfirmation(rl.getSender(), rl.getStartDate(), rl.getEndDate());
			    }
			}).start();

			
		}else {
			String reason = payload.get("reason").toString();
			System.err.println("REASON u servisu: " + reason);
			//se.sendLeaveRejection(rl.getSender(), rl.getStartDate(), rl.getEndDate(), reason);
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendLeaveRejection(rl.getSender(), rl.getStartDate(), rl.getEndDate(), reason);
			    }
			}).start();

		}
		leaveRepo.delete(rl);
	}
	
	public void saveFreeAppo(Map<String, String> payload) {
		String dateAndTime = payload.get("date");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = new Date();
		try {
			date = formatter.parse(dateAndTime); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ExamType et = examRepo.findByName(payload.get("type"));
		Room r = roomRepo.findById(Integer.parseInt(payload.get("room"))).get();
		Doctor dr = drRepo.findById(Integer.parseInt(payload.get("doctor"))).get();
		Exam a = new Exam();
		Reservation res = new Reservation();
		a.setDateTime(date);
		Admin admin = (Admin) Hibernate.unproxy(systemService.getLoggedUser()); 
		a.setClinic(admin.getClinic());
		a.setHeld(false);
		a.setReservation(res);
		a.setDoctor(dr);
		System.err.println("DR: "+ dr);
		a.setType(et);
		System.err.println("TYPE: "+ et);
		res.setAppointment(a);
		System.err.println("RES: "+res);
		res.setRoom(r);
		appoRepo.saveAndFlush(a);
		resRepo.saveAndFlush(res);
		
	}
	
	public String searchDoc(Clinic c, Map<String, String> payload) {
		String ename = payload.get("drName");
		System.err.println("search dr"+ename);
		List<Doctor> drs = drRepo.findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(ename,ename);
		System.err.println("search dr "+ drs);
		JSONArray jar = new JSONArray();
		for (Doctor d: drs) {
			if(d.getClinic().equals(c)) {
				JSONObject job = new JSONObject();
				job.put("name", d.getFirstName());
				job.put("lastName", d.getLastName());
				job.put("rating", d.getRating());
				jar.add(job);
			}
			
		}
		return g.toJson(jar);

	}


public String searchEType(Integer idc, Map<String, String> payload) {
	String ename = payload.get("ename");
	System.err.println("search e type "+idc + " "+ename);
	List<ExamType> types = examRepo.findAllByClinic_idcAndNameContainingIgnoreCase(idc,ename);
	List<ExamType> ts = examRepo.findAllByNameContainingIgnoreCase(ename);
	System.err.println("search e type "+ ts);
	JSONArray jar = new JSONArray();
	for (ExamType e: types) {
		JSONObject job = new JSONObject();
		job.put("name", e.getName());
		job.put("duration", e.getDuration());
		job.put("price", e.getPrice());
		jar.add(job);
	}
	return g.toJson(jar);

}

}
