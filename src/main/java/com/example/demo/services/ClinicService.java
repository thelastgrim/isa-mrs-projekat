package com.example.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.OnlyOnceLoggingDenyMeterFilter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.database.ClinicRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.database.SurgeryDoctorRepo;
import com.example.demo.database.SurgeryRepo;
import com.example.demo.functionalites.ClinicSorter;
import com.example.demo.functionalites.DoctorAvailability;
import com.example.demo.functionalites.DoctorSorter;
import com.example.demo.functionalites.ExamSorter;
import com.example.demo.model.Absence;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.ExamType;
import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.example.demo.model.Surgery;
import com.google.gson.Gson;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Component
//@Service
//@Transactional(readOnly = true)
public class ClinicService {
	@Autowired
	ClinicRepo clinicRepo;
	@Autowired
	ExamRepo examRepo;
	@Autowired
	ExamTypeRepo examTypeRepo;
	
	@Autowired
	RoomRepo roomRepo;
	@Autowired
	SystemService systemService;
	
	@Autowired
	SurgeryRepo surgRepo;
	
	final int PAUSE = 5; //da se pregledi ne bi "lepili" jedan za drugi, ovo je pauza izmedju nih, "5 minuta"
	final int APPOINTMENT_STEP = 20; //razmak izmedju vremena dozvoljenih za biranje, za pacijenta
	private static Gson g = new Gson();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	public Model getClinicPageData(Model model, Integer id, String type, String date) {
		Clinic c = null;
		List<DoctorAvailability> da = new ArrayList<DoctorAvailability>();
		ExamType e_type = examTypeRepo.findByName(type);
		try {
			c = clinicRepo.findById(id).get();
		}
		catch (NoSuchElementException e) {
			throw new ResponseStatusException(
					  HttpStatus.NOT_FOUND, "Clinic not found."
					);
		}
		
		List<Exam> exams = examRepo.findByHeldFalseAndMedRecordIsNullAndReservationIsNotNullAndClinic(c);
		exams.removeIf(e -> e.getReservation().getPatient()!=null);
		
		model.addAttribute("types", c.getTypes());
		model.addAttribute("date", date.equals("Any") ? "" : date);
		model.addAttribute("exams", exams);
		model.addAttribute("clinic", c);
		if (type.equals("Any") || date.equals("Any")) {
			if (!type.equals("Any")) {
				da = getAllDoctorsAvailableForType(e_type, c);
			}else {
				da = getAllDoctorsAvailable(c);
			}	
		}
		else if(!type.equals("Any") && !date.equals("Any")) {
			
			da = getDoctorsByType(type, date, null);
			da.forEach(a -> System.out.println(a.getDoctor().getName()));
			da.removeIf(a -> !a.getDoctor().getClinic().equals(clinicRepo.getOne(id)));
			
		}
		
		model.addAttribute("selected", type);
		model.addAttribute("da", da);
			
		return model;
	}
	
	public String getSingleClinicData(Clinic c, String dateFilter, String typeFilter, String sorterFilter) {
		//String sorter = payload.get("sorter");
		Set<Doctor> doctors = null;
		List<DoctorAvailability> da = null;	
		JSONArray jar = new JSONArray();
		if(typeFilter.equals("Any") || dateFilter.equals("")) {
			doctors = c.getDoctors();
			List<Doctor> doctorList = new ArrayList<>(doctors);
			
			if(!sorterFilter.equals("none")) {
				
				doctorList = DoctorSorter.sort(doctorList, sorterFilter);
			}
			
			for (Doctor d : doctorList) {
				JSONObject job = new JSONObject();
				job.put("id", d.getId());
				job.put("name", d.getName());
				job.put("rating", Double.toString(d.getRating()));
				job.put("times", new ArrayList<String>());
				jar.add(job);
			}
			return g.toJson(jar);	
		}
		else {
			ExamType ex_type = examTypeRepo.findByName(typeFilter);
			doctors = getAllDoctorsForType(ex_type, c);
			
			for (Iterator<Doctor> i = doctors.iterator(); i.hasNext();) {
				Doctor doctor = i.next();
				if (!isDoctorAvailable(dateFilter, doctor, ex_type)) {
				i.remove();
				}
			}		
			System.err.println("-----System " + ex_type + "  \n\t" + dateFilter + " \n\t" + doctors);
			da = getDoctorsByType(ex_type.getName(), dateFilter, doctors);	
			
			List<DoctorAvailability> doctorAvaList = new ArrayList<>(da);
			
			if(!sorterFilter.equals("none")) {
				
				doctorAvaList = DoctorSorter.sortAva(doctorAvaList, sorterFilter);
			}
			
			for (DoctorAvailability d : da) {	
				JSONObject job = new JSONObject();
				job.put("id", d.getDoctor().getId());
				job.put("name", d.getDoctor().getName());
				job.put("rating", Double.toString(d.getDoctor().getRating()));
				job.put("times",d.getFroms());
				jar.add(job);
			}
		return g.toJson(jar);
			
		}
					
	}
	
	public String getFilteredClinics(Map<String, String> payload) {
		
		List<Clinic> clinics = clinicRepo.findAll();
		String typeFilter = payload.get("type"); 
		String cityFilter = payload.get("city");
		String dateFilter = payload.get("date");
		String ratingFilter = payload.get("rating");
		String sorter = payload.get("sorter");
		String comp = payload.get("comp");
		
		ExamType ex_type = null;
		for (Iterator<Clinic> i = clinics.iterator(); i.hasNext();) {
		    Clinic clinic = i.next();
		    
		    if(!typeFilter.equals("Any")) {
				boolean has = false;
				for (ExamType type: clinic.getTypes()) {
					if(type.getName().equals(typeFilter)) {
						ex_type = examTypeRepo.findByName(typeFilter);
						has = true;
						break;
					}
				}
				if(!has) {
					i.remove();
					continue;
				}
			}
		    if(!cityFilter.equals("Any")) {
				if(!clinic.getAddress().getCity().equals(cityFilter)) {
					i.remove();
					continue;
				}
			}
		    if(!dateFilter.equals("")) {
				if(!typeFilter.equals("Any")) {
					ex_type = examTypeRepo.findByName(typeFilter);
					if(!isDateTypeAvailable(dateFilter, ex_type)) {
						i.remove();
						continue;
					}	
				}
			}
		    if(!ratingFilter.equals("Any")) {
		    	switch (comp) {
				case "=":
					if(clinic.getRating() != Double.parseDouble(ratingFilter))
						i.remove();
					break;
				case ">":
					if(clinic.getRating() <= Double.parseDouble(ratingFilter))
						i.remove();
					break;
				case "<":
					if(clinic.getRating() >= Double.parseDouble(ratingFilter))
						i.remove();
					break;
				case "<=":
					if(clinic.getRating() > Double.parseDouble(ratingFilter))
						i.remove();
					break;
				case ">=":
					if(clinic.getRating() < Double.parseDouble(ratingFilter))
						i.remove();
					break;
				default:
					break;
				}
				System.out.println(ratingFilter);
				
					continue;
				
			}       
		}
			
		
		if(!sorter.equals("none")) {
			clinics = ClinicSorter.sort(clinics, sorter);
		}
		JSONArray jar = new JSONArray();
		for (Clinic clinic : clinics) {
			JSONObject job = new JSONObject();
			job.put("idc", clinic.getIdc());
			job.put("name", clinic.getName());
			job.put("city", clinic.getAddress().getCity());
			job.put("rating",Double.toString(clinic.getRating()));
			if (ex_type!=null) {
				job.put("price", ex_type.getPrice());
			}
			else {
				job.put("price", "---");
			}
			

			jar.add(job);
		}
		return g.toJson(jar);
	}
	
	private List<DoctorAvailability> getAllDoctorsAvailableForType(ExamType type, Clinic c){
		List<DoctorAvailability> da = new ArrayList<DoctorAvailability>();
		for (Doctor doctor : c.getDoctors()) {
			if(doctor.getSpecializations().contains(type)) {
				da.add(new DoctorAvailability(doctor, null));
			}
		}
		return da;
				
	}
	
	private List<DoctorAvailability> getAllDoctorsAvailable(Clinic c){
		List<DoctorAvailability> da = new ArrayList<DoctorAvailability>();
		for (Doctor doctor : c.getDoctors()) {
			da.add(new DoctorAvailability(doctor, null));
		}
		return da;
	}

	private List<DoctorAvailability> getDoctorsByType(String type, String date, Set<Doctor> _doctors){
		
		List<DoctorAvailability> da = new ArrayList<DoctorAvailability>();
		
		ExamType ex_type = examTypeRepo.findByName(type);
		int duration = ex_type.getDuration();
		Set<Doctor> doctors = null;
		
		//doktori koji su specijalizovani za taj pregled
		if(_doctors==null) {
			doctors = ex_type.getDoctors();
		}else {
			doctors = _doctors;
		}

		for (Doctor doctor : doctors) {
			
			//List<Exam> his_appointments = examRepo.findByHeldFalseAndReservationIsNotNullAndDoctor(doctor);
			List<Exam> his_appointments = examRepo.findByHeldFalseAndDoctor(doctor);
			List<Exam> exams_today = new ArrayList<Exam>();
			ArrayList<String> doctors_times;
			boolean complete_available = true;
			for (Exam exam: his_appointments) {
				String exams_day = exam.getDateTime().toString().split(" ")[0];
				if(exams_day.equals(date)) {
					complete_available = false;
					exams_today.add(exam);
				}	
			}
			if (complete_available) {
				doctors_times = generateFroms(doctor, null, duration);
				System.out.println("Doctor: " + doctor.getFirstName());
				for (String time : doctors_times) {
					System.out.println("\t\t"+time);
				}	
			}else {
				
				ArrayList<String> busy_time = ExamSorter.sortBusyTime(getBusyTime(exams_today));
				
				ArrayList<String> available_times = getAvailableTimes(ex_type.getDuration(), busy_time, doctor.getStartTime(), doctor.getEndTime());
				doctors_times = generateFroms(doctor, available_times, duration);
				System.out.println("Doctor: " + doctor.getFirstName());
				
				for (String time : doctors_times) {
					System.out.println("\t\t"+time);
				}
				
			}
					
			da.add(new DoctorAvailability(doctor, doctors_times));
		}
		
		
		return da;
	}
	
private List<DoctorAvailability> getDoctorsBySurgery(String date, Set<Doctor> doctors){
		
		List<DoctorAvailability> da = new ArrayList<DoctorAvailability>();
		
		
		int duration = 60; // po defaultu sam stavio 60

		for (Doctor doctor : doctors) {
			
			//List<Exam> his_appointments = examRepo.findByHeldFalseAndReservationIsNotNullAndDoctor(doctor);
			List<Exam> his_appointments = examRepo.findByHeldFalseAndDoctor(doctor);
			List<Exam> exams_today = new ArrayList<Exam>();
			ArrayList<String> doctors_times;
			boolean complete_available = true;
			for (Exam exam: his_appointments) {
				String exams_day = exam.getDateTime().toString().split(" ")[0];
				if(exams_day.equals(date)) {
					complete_available = false;
					exams_today.add(exam);
				}	
			}
			if (complete_available) {
				doctors_times = generateFroms(doctor, null, duration);
				
					
			}else {
				
				ArrayList<String> busy_time = ExamSorter.sortBusyTime(getBusyTime(exams_today));
				
				ArrayList<String> available_times = getAvailableTimes(60, busy_time, doctor.getStartTime(), doctor.getEndTime());
				doctors_times = generateFroms(doctor, available_times, duration);
			
				
			}
					
			da.add(new DoctorAvailability(doctor, doctors_times));
		}
		
		
		return da;
	}
	
	
	
	private ArrayList<String> generateFroms(Doctor doctor, ArrayList<String> available_times, int duration) {
		ArrayList<String> froms = new ArrayList<String>();
		LocalTime start_time = null;
		LocalTime old_start = null; //kad treca smena, start predje preko 00:00 i infinite loop
		LocalTime end_time = null;
		//doktor absolutno slobodan
		if(available_times==null) {
			boolean midnight = false;
			start_time = StringToTime(doctor.getStartTime());
			old_start = StringToTime(doctor.getStartTime());
			end_time = StringToTime(doctor.getEndTime());
			//kada doktor radi posle ponoci, endtime treba da je do kraja dana
			if(start_time.isAfter(end_time.minusMinutes(duration))) {
				midnight = true;
				end_time = LocalTime.of(23, 55);
				end_time = end_time.minusMinutes(duration);
			}
			while (start_time.isBefore(midnight ? end_time : end_time.minusMinutes(duration))){
				//vodi se racuna da ne moze da se pregled zakaze ako ce probiti trajanje doktorovog radnog vremena
				froms.add(start_time.toString());
				start_time = start_time.plusMinutes(APPOINTMENT_STEP);
				if (start_time.isBefore(old_start)) {
					break;
				}
			}
			return froms;
		}else {
			for (String time : available_times) {
				String[] data = time.split("-");
				start_time = StringToTime(data[0]);
				old_start = StringToTime(data[0]);
				end_time = StringToTime(data[1]);
				if(start_time.isAfter(end_time)) {
					end_time = LocalTime.of(23, 55);
				}
				while (start_time.isBefore(end_time)){ 
					froms.add(start_time.toString());
					start_time = start_time.plusMinutes(APPOINTMENT_STEP);
					if (start_time.isBefore(old_start)) {
						break;
					}
				}
				froms.add(end_time.toString());
				
			}
			return froms;	
		}
	
	}
	
	private LocalTime StringToTime(String time) {
		/*
		 * time format : hh:mm 
		 */
			String[] data = time.split(":");
			return LocalTime.of(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
		}
	
	private ArrayList<String> getBusyTime(List<Exam> exams_today){

		LocalTime time1 = null;
		LocalTime time2 = null;
		ArrayList<String> busy_time = new ArrayList<String>();
		for (Exam exam : exams_today) {
			String exams_time = exam.getDateTime().toString().split(" ")[1];
			time1 =  StringToTime(exams_time);
			int exams_duration = exam.getType().getDuration();
			time2 = time1.plusMinutes(exams_duration);		
			busy_time.add(time1.toString()+"-"+time2.toString());					
		}
		return busy_time;
	}
	
	private ArrayList<String> getAvailableTimes(int duration, List<String> busy_time, String start_time, String end_time){
		ArrayList<String> available_hours = new ArrayList<String>();
		LocalTime start = null;
		LocalTime end = null;
		int difference = 0;;
		start = StringToTime(start_time);
		end = StringToTime(end_time);
		for (String string : busy_time) {
			String[] times = string.split("-");
			LocalTime from = StringToTime(times[0]);
			LocalTime to = StringToTime(times[1]);
			difference = (int)ChronoUnit.MINUTES.between(start, from);	
			if(duration >= difference) {
				//i da ima tacno slobodnog vremena koliko i traje pregled, opet ne moze da se zakaze
				//jer doktor mora malo i da odmori
				start = to;
				continue;
			}
			else {
				from = from.minusMinutes(duration);
				from = from.minusMinutes(PAUSE);
				String to_add = start.toString() +"-"+ from.toString();
				available_hours.add(to_add);
				start = to;	
			}				
		}
		difference = (int) ChronoUnit.MINUTES.between(start, end.minusMinutes(1));
		if(duration < difference) {//difference manje od nule kad je treca smena odnosno end je pre starta
			end = end.minusMinutes(duration);
			end = end.minusMinutes(PAUSE);
			String to_add = start.toString() +"-"+end.toString();
			available_hours.add(to_add);	
		}
		return available_hours;	
	}
	
	private Set<Doctor> getAllDoctorsForType(ExamType type, Clinic c){
		Set<Doctor> doctors_at_clinic = c.getDoctors();
		for (Iterator<Doctor> i = doctors_at_clinic.iterator(); i.hasNext();) {
		    Doctor doctor = i.next();
		    if (!doctor.getSpecializations().contains(type)) {
		    	i.remove();
		    }
		}
		return doctors_at_clinic;		
	}
	
	private Set<Doctor> getSurgeryDoctors(int surgeryId){
		
		List<Surgery> s = surgRepo.findAll();
		
		Surgery surgery = null;
		
		for(Surgery s1 : s) {
			
			if(s1.getId() == surgeryId) {
				surgery = s1;
				break;
			}
			
		}
		
		return surgery.getDoctors();
		
		
		
	}
	
	
	private boolean isDoctorAvailable(String _date, Doctor doctor, ExamType _type) {
		
		//List<Exam> his_appointments = examRepo.findByHeldFalseAndReservationIsNotNullAndDoctor(doctor);
		List<Exam> his_appointments = examRepo.findByHeldFalseAndDoctor(doctor);
		boolean complete_available = true;
		List<Exam> exams_today = new ArrayList<Exam>();
		int duration = _type.getDuration();
		Date date = null;
		try {
			date = formatter.parse(_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Absence absence : doctor.getAbsences()) {
			if((absence.getStart().before(date) && absence.getLeave().after(date)) || 
					(absence.getStart().equals(date) || absence.getLeave().equals(date))) {
				System.out.println("\t Doktor je na odmoru.");
				return false;
			}
		}
		
		for (Exam exam: his_appointments) {
			String exams_day = exam.getDateTime().toString().split(" ")[0];
			//ako se barem jedan datum iz svih doktorovih poklopi sa biranim, doktor nije APSOLUTNO SLOBODAN
			if(exams_day.equals(_date)) {
				complete_available = false;
				exams_today.add(exam);
			}
		}
		if (complete_available) {
			System.out.println("\tPotpuno slobodan.");
			ArrayList<String>aa = generateFroms(doctor, null, duration);
			for (String string : aa) {
				System.out.println("\t\t\t" + string);
			}
			return true;
		}else {
			exams_today = ExamSorter.sortByDateTime(exams_today);
			List<String> busy_time = getBusyTime(exams_today);
			System.out.println("\t\tSlobodan od-do");
			ArrayList<String> at = getAvailableTimes(duration, busy_time, doctor.getStartTime(), doctor.getEndTime());
			if(at.size()==0) {
				System.out.println("\t Doktor nema vremena za ovaj pregled");
				return false;
			}
			else {
				return true;
			}
						
		}
		
	}
	
	private boolean isDateTypeAvailable(String _date, ExamType _type) {
		
		Set<Doctor> doctors = _type.getDoctors();
		if(doctors.size()==0) {
			System.out.println("Trenutno nema doktora specijalizovanih za - "+_type.getName());
			return false;
		}
		System.out.println("Pregled traje: "+_type.getDuration()+"min");
		boolean atleast_one = false;
		for (Doctor doctor : doctors) {
			System.out.println(doctor.getFirstName()+"("+doctor.getStartTime()+" - "+doctor.getEndTime()+")");
			if (isDoctorAvailable(_date, doctor, _type)) {
				atleast_one = true;
				//break - U IMPLEMENTACIJI TREBA
			};
		}
		if(atleast_one) {
			return true;
		}else {
			System.out.println("Trenutno nema slobodnih doktora za ovaj datum.");
			return false;
		}	
	}
	
	//vraca doktore i termine kad su slobodni i oni i izabrana sala za odabrani datum
	//npr.doktor John je slobodan 07/02/2020 u 15:20h ali room1 nije, pa se taj termin nece prikazati (8812, '20200702 15:20',false, 9998,  9876),
	public String getDoctorsAndResTime(Clinic c, String dateFilter, String typeFilter, int roomNum, String roomName) {
		
		Set<Doctor> doctors = null;
		List<DoctorAvailability> da = null;	
		JSONArray jar = new JSONArray();
		Room r = roomRepo.findByNumberAndName(roomNum, roomName);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = formatter.parse(dateFilter);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<String> resTime = new ArrayList<String>();
		Set<Reservation> ress = r.getRessByDate(date);
		for(Reservation res: ress) {
			resTime.add(systemService.getTimeOnly(res.getAppointment().getDateTime()));
		}
		
		ExamType ex_type = examTypeRepo.findByName(typeFilter);
		doctors = getAllDoctorsForType(ex_type, c);
		for (Iterator<Doctor> i = doctors.iterator(); i.hasNext();) {
			Doctor doctor = i.next();
			if (!isDoctorAvailable(dateFilter, doctor, ex_type)) {
			i.remove();
			}
		}		
		System.err.println("-----System " + ex_type + "  \n\t" + dateFilter + " \n\t" + doctors);
		da = getDoctorsByType(ex_type.getName(), dateFilter, doctors);	
		for (DoctorAvailability d : da) {
			System.err.println("~~~~~~~~ forms: "+ d.getFroms());
			d.getFroms().removeAll(resTime); //izbaci sve termine za koje je dr slobodan ali ne i sala
			ArrayList<String> times = d.getFroms();
			System.err.println("~~~~~~~~ times: "+ times);
			JSONObject job = new JSONObject();
			job.put("id", d.getDoctor().getId());
			job.put("name", d.getDoctor().getName());
			job.put("rating", Double.toString(d.getDoctor().getRating()));
			job.put("times",times);
			jar.add(job);
		}
	return g.toJson(jar);
		
	}
	
public String getDoctorsAndResTimeSurgeries(Clinic c, String dateFilter, String typeFilter, int roomNum, String roomName,int surgeryId) {
		
		Set<Doctor> doctors = null;
		List<DoctorAvailability> da = null;	
		JSONArray jar = new JSONArray();
		Room r = roomRepo.findByNumberAndName(roomNum, roomName);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = formatter.parse(dateFilter);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<String> resTime = new ArrayList<String>();
		Set<Reservation> ress = r.getRessByDate(date);
		for(Reservation res: ress) {
			resTime.add(systemService.getTimeOnly(res.getAppointment().getDateTime()));
		}
		
		
		doctors = getSurgeryDoctors(surgeryId);
		List<Doctor> doctorsForClinic = new ArrayList<Doctor>();
		
		for(Doctor d: doctors) {
			if(d.getClinic().getIdc() == c.getIdc() ) {
				doctorsForClinic.add(d);
			}
		}
			
		//System.err.println("-----System " + ex_type + "  \n\t" + dateFilter + " \n\t" + doctors);
		da = getDoctorsBySurgery(dateFilter, doctors);	
		
		for (DoctorAvailability d : da) {
			System.err.println("~~~~~~~~ forms: "+ d.getFroms());
			d.getFroms().removeAll(resTime); //izbaci sve termine za koje je dr slobodan ali ne i sala
			ArrayList<String> times = d.getFroms();
			System.err.println("~~~~~~~~ times: "+ times);
			JSONObject job = new JSONObject();
			job.put("id", d.getDoctor().getId());
			job.put("name", d.getDoctor().getName());
			job.put("rating", Double.toString(d.getDoctor().getRating()));
			job.put("times",times);
			jar.add(job);
		}
	return g.toJson(jar);
		
	}
					

	
	
	
	
}
