package com.example.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.database.AdminRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.RequestLeaveRepo;
import com.example.demo.functionalites.PatientSorter;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.Admin;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.ExamType;
import com.example.demo.model.LeaveType;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.ReqLeave;
import com.example.demo.model.ReqRegistration;
import com.example.demo.model.RequestType;
import com.example.demo.model.Surgery;
import com.google.gson.Gson;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;




@Service
public class DoctorService {
	@Autowired
	SystemService systemService;
	
	@Autowired
	PatientRepo patRepo;
	
	@Autowired
	RequestLeaveRepo leaveRepo;
	
	@Autowired
	AdminRepo adrepo;
	
	@Autowired
	ExamTypeRepo exTypeRepo;
	
	@Autowired
	RequestAppoRepo reAppoRepo;

	private static Gson g = new Gson();
	public String getSortedPatients(Map<String, String> payload) {
		Doctor d = (Doctor) Hibernate.unproxy(systemService.getLoggedUser());
		Clinic c = d.getClinic();
		
		String sorter = payload.get("sorter");
		List<Patient> sortedPatients = new ArrayList<Patient>();
		List<Patient> clinicPatients = getClinicPatients(c);
		if(!sorter.equals("none")) {
			sortedPatients = PatientSorter.sort(clinicPatients, sorter);
		}
		
		JSONArray jar = new JSONArray();
		for (Patient patient : sortedPatients) {
			JSONObject job = new JSONObject();
			job.put("id", patient.getId());
			job.put("firstName", patient.getFirstName());
			job.put("lastName", patient.getLastName());
			job.put("ssn",patient.getSsn());
			jar.add(job);
		}
		return g.toJson(jar);
	
	}
	
	public List<Patient> getClinicPatients(Clinic c){
		List<Patient> patients = patRepo.findAll();
		List<Patient> clinicPatients = new ArrayList<Patient>();
		for(Patient p: patients) {
			for(Appointment a: p.getMedrec().getAppointments()) {
				if(a.getClinic().equals(c)) {
					clinicPatients.add(p);
					break;//da ne bi vise puta dodali istog pacijenta
				}
			}
		}
		
		return clinicPatients;
	}

	
	
	public void saveReqLeave(Map<String, String> payload) {

		Doctor dr = (Doctor) Hibernate.unproxy(systemService.getLoggedUser()); 
		ReqLeave rl = new ReqLeave();
		rl.setType(RequestType.LEAVE);
				
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
		
		rl.setArrived(formatter.format(date).toString());
		rl.setSeen(false);
		rl.setSender(dr);
		
		try {
			rl.setStartDate(formatter2.parse(payload.get("startDate")));
			rl.setEndDate(formatter2.parse(payload.get("endDate")));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int leaveType = Integer.parseInt(payload.get("type"));
		if(leaveType == 1) {
			rl.setLeaveType(LeaveType.HOLIDAY);
		}else {
			rl.setLeaveType(LeaveType.ExcusedAbsence);
		}
		
		leaveRepo.saveAndFlush(rl);
	}
	
	
public String bookAppoByDr(@RequestBody Map<String, String> payload) {
		
		Doctor dr = (Doctor) Hibernate.unproxy(systemService.getLoggedUser()); 
		List<Admin> admins = adrepo.findAllByClinic_Idc(dr.getClinic().getIdc());
		Patient patient = patRepo.findById(Integer.parseInt(payload.get("patient").toString())).get();
		String dateTime = payload.get("date") + " " + payload.get("time");
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		
		ReqAppointment ra = new ReqAppointment();
		
		ra.setSender(dr);
		ra.setArrived(formatter.format(date).toString());
		ra.setSeen(false);
	//	ra.setTest("radi");
		
		if(payload.get("type").equals("1")) {
			ra.setType(RequestType.OPERATION);
			Surgery apo = new Surgery();
			apo.setHeld(false);
			apo.setMedRecord(patient.getMedrec());
			try {
				apo.setDateTime(formatter.parse(dateTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			apo.setClinic(dr.getClinic());
			apo.setName(payload.get("name"));
		//	apo.setId(1112009);
			
			//sRepo.saveAndFlush(apo);
			
			ra.setAppointment(apo);
			
		}else {
			ra.setType(RequestType.EXAM);
			Exam apo = new Exam();
			apo.setHeld(false);
			apo.setMedRecord(patient.getMedrec());
			try {
				apo.setDateTime(formatter.parse(dateTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			apo.setClinic(dr.getClinic());
			ExamType exType = exTypeRepo.findById(Integer.parseInt(payload.get("type"))).get();
			apo.setType(exType);
			apo.setDoctor(dr);
		//	apo.setId(1112009);
		//	examRepo.saveAndFlush(apo);
			ra.setAppointment(apo);
		}
		
		
		//repo.saveAndFlush(rr);
		ReqRegistration r = new ReqRegistration();
		
		reAppoRepo.saveAndFlush(ra);
	//	reqRepo.saveAndFlush(ra);
	//	System.out.println(ra);
		if(admins.size()>0) {//ukoliko postoje admini klinike posalji im mail
			SendEmail se = new SendEmail();
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendReqAppointment(ra,patient, admins);
			    }
			}).start();

		}
		return "1";
	}
}
