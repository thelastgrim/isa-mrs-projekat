package com.example.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.database.AdminRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.RequestLeaveRepo;
import com.example.demo.functionalites.PatientSorter;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.LeaveType;
import com.example.demo.model.Nurse;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqLeave;
import com.example.demo.model.RequestType;
import com.google.gson.Gson;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service
public class NurseService {
	
	@Autowired
	SystemService systemService;
		
	@Autowired
	PatientRepo patRepo;
	
	@Autowired
	RequestLeaveRepo reqLeaveRepo;
	
	@Autowired
	AdminRepo adrepo;
	
	@Autowired
	ExamTypeRepo exTypeRepo;
	
	@Autowired
	RequestAppoRepo reqAppoRepo;
	
	private static Gson g = new Gson();
	public String getSortedPatients(Map<String,String> payload) {
		Nurse nurse = (Nurse) Hibernate.unproxy(systemService);
		Clinic clinic = nurse.getClinic();
		
		String sorter = payload.get("sorter");
		List<Patient> sortedPatients = new ArrayList<Patient>();
		List<Patient> clinicPatients = getClinicPatients(clinic);
		
		if(!sorter.equals("none")) {
			sortedPatients = PatientSorter.sort(clinicPatients, sorter);
		}
		JSONArray jar = new JSONArray();
		for(Patient p : sortedPatients) {
			JSONObject jo = new JSONObject();
			jo.put("id", p.getId());
			jo.put("firstName", p.getFirstName());
			jo.put("lastName", p.getLastName());
			jo.put("ssn", p.getSsn());
			
			jar.add(jo);
			
		}
		return g.toJson(jar);
	}
	public List<Patient> getClinicPatients(Clinic c){
		List<Patient> patients = patRepo.findAll();
		List<Patient> clinicPatients = new ArrayList<Patient>();
		for(Patient p: patients) {
			for(Appointment a: p.getMedrec().getAppointments()) {
				if(a.getClinic().equals(c)) {
					clinicPatients.add(p);
					break;
				}
			}
		}
		
		return clinicPatients;
	}
	
	public void saveReqLeave(Map<String,String> payload) {
		
		Nurse nurse = (Nurse) Hibernate.unproxy(systemService.getLoggedUser());
		ReqLeave req = new ReqLeave();
		req.setType(RequestType.LEAVE);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
		
		req.setArrived(formatter.format(date).toString());
		req.setSeen(false);
		req.setSender(nurse);
		
		try {
			req.setStartDate(formatter2.parse(payload.get("startDate")));
			req.setEndDate(formatter2.parse(payload.get("endDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int leaveType = Integer.parseInt(payload.get("type"));
		if(leaveType == 1) {
			req.setLeaveType(LeaveType.HOLIDAY);
		}else {
			req.setLeaveType(LeaveType.ExcusedAbsence);
		}
		reqLeaveRepo.saveAndFlush(req);
	}

}
