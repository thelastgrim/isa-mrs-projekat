package com.example.demo.services;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RatingsClinicsRepo;
import com.example.demo.database.RatingsDoctorsRepo;
import com.example.demo.functionalites.AppointmentSorter;
import com.example.demo.functionalites.RatingClinicSorter;
import com.example.demo.functionalites.RatingDoctorSorter;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.ExamType;
import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;
import com.example.demo.model.RatingsClinic;
import com.example.demo.model.RatingsDoctor;
import com.example.demo.model.Surgery;
import com.google.gson.Gson;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service
@Transactional(readOnly = true)
public class PatientService {
	
	@Autowired
	ClinicRepo clinicCrepo;
	@Autowired
	AppointmentRepo appointmentRepo;
	@Autowired
	RatingsDoctorsRepo ratingsDoctorRepo;
	@Autowired
	RatingsClinicsRepo ratingsClinicRepo;
	@Autowired
	DoctorRepo doctorRepo;
	@Autowired
	PatientRepo patientRepo;
	
	private static Gson g = new Gson();
	DecimalFormat df = new DecimalFormat("#.##");
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public Model getPatientPageData(Model model, Patient patient) {
		
		Set<String> cities = new HashSet<String>();
		Set<String> types = new HashSet<String>();
		for (Clinic clinic : clinicCrepo.findAll()) {
			cities.add(clinic.getAddress().getCity());
			for (ExamType type : clinic.getTypes()) {
				types.add(type.getName());
			}	
		}
		MedicalRecord medrec = patient.getMedrec();
		List<Appointment> appointments = appointmentRepo.findByHeldFalseAndReservationIsNotNullAndMedRecord(patient.getMedrec());
		appointments.removeIf(ap -> ap.getReservation().getPatient()==null);
		List<Appointment> pastAppointments = appointmentRepo.findByHeldTrueAndMedRecord(patient.getMedrec());
		//pastAppointments.removeIf(a -> a instanceof Surgery);
		List<RatingsClinic> ratingsClinics = ratingsClinicRepo.findByPatient(patient);
		List<RatingsDoctor> ratingsDoctors = ratingsDoctorRepo.findByPatient(patient);
		model.addAttribute("clinics", clinicCrepo.findAll());
		model.addAttribute("cities", cities);
		model.addAttribute("types", types);
		model.addAttribute("medrec", medrec);
		model.addAttribute("appointments", appointments);
		model.addAttribute("pastAppo", pastAppointments);
		model.addAttribute("ratingsClinics", ratingsClinics);
		model.addAttribute("ratingsDoctors", ratingsDoctors);
		
		return model;
	}
	
	public String getAppointmentDetails(Map<String, String> payload) {
		
		Integer apo_id = Integer.parseInt(payload.get("id"));
		Appointment apo = appointmentRepo.findById(apo_id).get();
		JSONObject job = new JSONObject();
		Clinic c = apo.getClinic();

		job.put("country", c.getAddress().getCountry());
		job.put("city", c.getAddress().getCity());
		job.put("address", c.getAddress().getFullName());
		job.put("clinic", c.getName());
		
		if(apo instanceof Exam) {
			job.put("doctor", ((Exam) apo).getDoctor().getName());
			job.put("type", 0);
		}else {
			String doctors = "";
			for (Doctor doctor : ((Surgery) apo).getDoctors()) {
				doctors = doctors+ doctor.getName()+", ";
			}
			job.put("doctor", doctors);
			job.put("type", 1);
		}
		
		job.put("room", apo.getReservation().getRoom().getNumber());
		
		return g.toJson(job);	
	}
	
	public String getSortedDiagnoses(Map<String, String> payload, Patient patient) {
		
		String sorter = payload.get("sorter");
		if(!sorter.equals("none")) {
			MedicalRecord medrec = patient.getMedrec();
			List<Appointment> appointments = null;
			appointments = appointmentRepo.findByHeldTrueAndMedRecord(patient.getMedrec());
			appointments.removeIf(a -> a instanceof Surgery);
			appointments = AppointmentSorter.sort(appointments, sorter);
			JSONArray jar = new JSONArray();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			for (Appointment appo : appointments) {
				JSONObject job = new JSONObject();
				job.put("dateTime", formatter.format(appo.getDateTime()));
				job.put("name", ((Exam) appo).getDiagnosis().getLatin());
				jar.add(job);
			}
			return g.toJson(jar);
		
		}
		return null;
	}
	
	@Transactional(readOnly = false)
	public void rateDoctor(Integer id, Integer value, Patient patient) {
		
		Doctor doctor = doctorRepo.findById(id).get();
		RatingsDoctor rd = ratingsDoctorRepo.findByPatientAndDoctor(patient, doctor);
		if(rd == null) {
			System.out.println("prvi REJT D");
			rd = new RatingsDoctor();
			rd.setDoctor(doctor);
			rd.setPatient(patient);
		}
		rd.setRating(value);
		ratingsDoctorRepo.saveAndFlush(rd);
		List<RatingsDoctor> rds = ratingsDoctorRepo.findByDoctor(doctor);
		int sum = 0;
		int count = 0;
		double newRating = 0;
		int rate =0;
		for (RatingsDoctor ratingsDoctor: rds) {
			rate = ratingsDoctor.getRating();
			if(rate>0) {
				sum += ratingsDoctor.getRating();
				count++;
			}
		}
		newRating = (double)sum/count;
		doctor.setRating(Double.valueOf(df.format(newRating)));
		doctorRepo.saveAndFlush(doctor);
		System.out.println("[PatientService] Doctor id="+doctor.getId()+" updated.");

	}

	@Transactional(readOnly = false)
	public void rateClinic(Integer id, Integer value, Patient patient) {
		Clinic c = clinicCrepo.findById(id).get();
		RatingsClinic rc = ratingsClinicRepo.findByPatientAndClinic(patient, c);
		
		if(rc == null) {
			System.out.println("prvi REJT");
			rc = new RatingsClinic();
			rc.setClinic(c);
			rc.setPatient(patient);
		}
		
		rc.setRating(value);
		ratingsClinicRepo.saveAndFlush(rc);
		List<RatingsClinic> rcs = ratingsClinicRepo.findByClinic(c);
		int sum = 0;
		int count = 0;
		double newRating = 0;
		int rate =0;
		for (RatingsClinic ratingsClinic : rcs) {
			rate = ratingsClinic.getRating();
			if(rate>0) {
				count++;
				sum += ratingsClinic.getRating();
			}	
		}	
		newRating = (double)sum/count;
		c.setRating(Double.valueOf(df.format(newRating)));
		clinicCrepo.saveAndFlush(c);
		System.out.println("[PatientService]Clinic id="+c.getIdc()+" updated.");
	}
	
	public String getSortedClinicsRatings(String sorter, Patient patient) {
		
		if(!sorter.equals("none")) {
			/* ratingUp/ratingDow
			 * clinicNameUp/clinicNameDown
			 */
			List<RatingsClinic> ratingsClinics = null;
			if(sorter.equals("ratingUp")) {
				ratingsClinics = ratingsClinicRepo.findByPatientOrderByRatingAsc(patient);
			}else if(sorter.equals("ratingDown")) {
				ratingsClinics = ratingsClinicRepo.findByPatientOrderByRatingDesc(patient);
			}
			else {
				ratingsClinics = ratingsClinicRepo.findByPatient(patient);
				ratingsClinics = RatingClinicSorter.sort(ratingsClinics, sorter);
			}
			JSONArray jar = new JSONArray();
			for (RatingsClinic rating: ratingsClinics) {
				JSONObject job = new JSONObject();
				job.put("id", rating.getClinic().getIdc());
				job.put("name", rating.getClinic().getName());
				job.put("rating", rating.getRating());
				jar.add(job);
			}
			
			return g.toJson(jar);
		}
		return null;
	}
	
	public String getSortedDoctorsRatings(String sorter, Patient patient) {
		
		if(!sorter.equals("none")) {
			List<RatingsDoctor> ratingsDoctor = null;
			if(sorter.equals("ratingUp")) {
				ratingsDoctor = ratingsDoctorRepo.findByPatientOrderByRatingAsc(patient);	
			}else if(sorter.equals("ratingDown")) {
				ratingsDoctor = ratingsDoctorRepo.findByPatientOrderByRatingDesc(patient);
			}
			else {
				ratingsDoctor = ratingsDoctorRepo.findByPatient(patient);
				ratingsDoctor = RatingDoctorSorter.sort(ratingsDoctor, sorter);
			}
			JSONArray jar = new JSONArray();
			for (RatingsDoctor rating: ratingsDoctor) {
				JSONObject job = new JSONObject();
				job.put("name", rating.getDoctor().getFirstName()+ " "+rating.getDoctor().getLastName());
				job.put("rating", rating.getRating());
				jar.add(job);
			}
			return g.toJson(jar);
		}
		return null;
	}
	
	public String getSortedAppointments(String sorter, String type, Patient p) {
			/*
			 * type : upcoming/past
			 */
			if(!sorter.equals("none")) {
				MedicalRecord medrec = p.getMedrec();
				List<Appointment> appointments = null;
				if(type.equals("upcoming")) {
					appointments = appointmentRepo.findByHeldFalseAndReservationIsNotNullAndMedRecord(p.getMedrec());
					appointments.removeIf(ap -> ap.getReservation().getPatient()==null);
				}
				else if (type.equals("past")) {
					appointments = appointmentRepo.findByHeldTrueAndMedRecord(p.getMedrec());
					//appointments.removeIf(a -> a instanceof Surgery);
				}
				appointments = AppointmentSorter.sort(appointments, sorter);
				JSONArray jar = new JSONArray();
				for (Appointment appo : appointments) {
					JSONObject job = new JSONObject();
					
					job.put("dateTime", formatter.format(appo.getDateTime()));
					job.put("clinic", appo.getClinic().getName());
					job.put("type", appo instanceof Exam ? "Exam" : "Surgery");
					if(type.equals("upcoming")) {
						if(appo instanceof Exam) {
							job.put("price", ((Exam) appo).getType().getPrice()+"$");
						}
						else {
							job.put("price", "No charge");
						}
					
				
					}else if (type.equals("past")) {
						//job.put("doctor", ((Exam) appo).getDoctor().getFirstName() +" "+((Exam) appo).getDoctor().getFirstName());
						
						try {
							job.put("doctor", ((Exam) appo).getDoctor().getFirstName() +" "+((Exam) appo).getDoctor().getFirstName());
						} catch (ClassCastException e) {
							job.put("doctor", ((Surgery) appo).getDoctors().toString());
						}

					}
					
					
					if(appo instanceof Exam) {
						job.put("name", ((Exam) appo).getType().getName());
					}
					else {
						job.put("name", ((Surgery) appo).getName());
					}
				
					job.put("id", appo.getId());
					jar.add(job);
				}
				return g.toJson(jar);
			}
		return null;
			
	}
	
	public String getPatientProfile(Patient p) {
		JSONObject job = new JSONObject();
		job.put("name", p.getFirstName());
		job.put("last", p.getLastName());
		job.put("city", p.getAddress().getCity());
		job.put("country", p.getAddress().getCountry());
		job.put("streetName", p.getAddress().getStreetName());
		job.put("number", p.getPhoneNUmber());
		return g.toJson(job);
	}
	
	@Transactional(readOnly = false)
	public void updatePatientProfile(Map<String, String> payload, Patient p) {

		p.getAddress().setCity(payload.get("city"));
		p.getAddress().setCountry(payload.get("country"));
		p.getAddress().setStreetName(payload.get("street"));
		
		p.setFirstName(payload.get("firstName"));
		p.setLastName(payload.get("lastName"));
		p.setPhoneNUmber(payload.get("phoneNumber"));
		
		if(!payload.get("pass").equals("")) {
			p.setPassword(payload.get("pass"));
		}
		
		
		patientRepo.saveAndFlush(p);
		
	}
	
}


