package com.example.demo.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.database.AppointmentRepo;
import com.example.demo.database.ClinicRepo;
import com.example.demo.database.DoctorRepo;
import com.example.demo.database.ExamRepo;
import com.example.demo.database.ExamTypeRepo;
import com.example.demo.database.PatientRepo;
import com.example.demo.database.RequestAppoRepo;
import com.example.demo.database.ReservationRepo;
import com.example.demo.database.RoomRepo;
import com.example.demo.database.UserRepo;
import com.example.demo.functionalites.SendEmail;
import com.example.demo.model.Admin;
import com.example.demo.model.Appointment;
import com.example.demo.model.Clinic;
import com.example.demo.model.Doctor;
import com.example.demo.model.Exam;
import com.example.demo.model.MedicalRecord;
import com.example.demo.model.Patient;
import com.example.demo.model.ReqAppointment;
import com.example.demo.model.RequestType;
import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.example.demo.model.STATUS;
import com.example.demo.model.User;

@Service
@Transactional(readOnly = true)
public class SystemService {

	@Autowired
	UserRepo userRepo;
	@Autowired
	ExamRepo examRepo;
	@Autowired
	RequestAppoRepo requestAppoRepo;
	@Autowired
	PatientRepo patientRepo;
	@Autowired
	AppointmentRepo appointmentRepo;
	@Autowired
	ReservationRepo reservationRepo;
	@Autowired
	ClinicRepo clinicRepo;
	@Autowired
	DoctorRepo doctorRepo;
	@Autowired
	ExamTypeRepo examTypeRepo;
	@Autowired
	RoomRepo roomRepo;
	
	private Base64.Decoder decoder = Base64.getDecoder();  
	
	public static final long HOUR = 3600*1000;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public String LogIn(String username, String password) {
		Set<GrantedAuthority> app_users = new HashSet<>();
		
		try {
			User u = userRepo.findByeMailAndPassword(username, password);
			System.out.println(u.getClass().getSimpleName());
			
			
			if (u.getStatus().equals(STATUS.PENDING)) {
					return "pending";
			}
			
			String type = u.getClass().getSimpleName();
			System.out.println("[AUTHO]:"+type);
			app_users.add(new SimpleGrantedAuthority(type));
			PreAuthenticatedAuthenticationToken paat = new PreAuthenticatedAuthenticationToken(u.getId(), null, app_users);
	        SecurityContextHolder.getContext().setAuthentication(paat);
			
	        switch (type) {
			case "Patient":
				return "patient";
			case "Admin":
				return "admin";
			case "CCAdmin":
				return "ccAdmin";
			case "Doctor":
				return "doctor";
			case "Nurse":
				return "nurse";
			default:
				return "none";
			}

		} catch (Exception e) {
			System.out.println("NISAM");
			return "none";
		}
		
	}
	
	public User getLoggedUser() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Integer id = Integer.parseInt(auth.getName());
        System.out.println(auth.getAuthorities().toString());
        return userRepo.getOne(id);
	}
	
	@Transactional(readOnly = false)
	public boolean quickSchedule(Patient p, Integer id ) {
	
		Exam e = examRepo.findByIdForWrite(id);
		MedicalRecord m = e.getMedRecord();
		if (m!=null){
			return false;
		}
		e.getReservation().setPatient(p);
		e.setMedRecord(p.getMedrec());
		examRepo.saveAndFlush(e);
		
		SendEmail se = new SendEmail();
		new Thread(new Runnable() {
		    public void run() {
		        se.sendQSConfirmation(p, e);
		    }
		}).start();

		
		return true;
	}
	
	@Transactional(readOnly = false)
	
	public String proccessPatientAnswer(String payload) {

		
		String _answer = new String(decoder.decode(payload)); 	
		
		System.out.println("decoded:" +_answer);
	
		String[] data = _answer.split("-");
		String answer = data[0];
		String[] ids = data[1].split("for");
		Integer appoId = Integer.parseInt(ids[0]);
		Integer patientId = Integer.parseInt(ids[1]);
		
		Appointment a = appointmentRepo.getOne(appoId);
		
		if(a.getReservation().getPatient()!=null) {
			return "Appointment already proccessed";
		}
		SendEmail se = new SendEmail();
		
		Patient p = patientRepo.findById(patientId).get();
		Exam e = examRepo.getOne(a.getId());
		if(answer.equals("accept")) {
			
			//Appointment a =  ra.getAppointment();
			
			//a.setMedRecord(p.getMedrec());
			a.getReservation().setPatient(p);
		
			
			//requestAppoRepo.delete(ra);
			
			
			appointmentRepo.saveAndFlush(a);
			
			
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendReservation(p, e.getDoctor(), e, a.getReservation().getRoom(), true);
			    }
			}).start();

			return "Your appointment has been successfully scheduled.";
						
		}else if (answer.equals("decline")) {
			//Appointment a =  ra.getAppointment();
			appointmentRepo.delete(a);
		
				/*
				 * AKO REZERVACIJA IMA SAMO JEDAN TERMINA (BAS OVAj)
				 * ONDA REZERVACIJA MOZE DA SE OBRISE
				 * U SUPROTNOM, REZERVACIJA OSTAJE JER SE NALAZI U DRUGIM TERMINIMA
				 */
				
			// PROVERITI DA LI RADI BRISANJe
			//requestAppoRepo.delete(ra);
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendReservation(p, e.getDoctor(), e, a.getReservation().getRoom(), false);
			    }
			}).start();

			return "Your appointment has been successfully cancelled.";
		}
		
		return "Invalid request";
	}
	
	@Transactional(readOnly = false)
	public String cancelAppointment(Map<String, String> payload, Patient p) {
		Date now = new Date();
		Integer exam_id = Integer.parseInt(payload.get("id"));
		Exam e= null;
		try {
			e = examRepo.findById(exam_id).get();
		} catch (NoSuchElementException ex) {
			return "Appointment no longer exists.";
		}
		
		if(e.getReservation().getPatient().getId() != p.getId()) {
			return "Invalid operation";
		}
		
		Date exams_date = e.getDateTime();
		if(exams_date.getTime()-now.getTime()>24*HOUR) {	
			Reservation r = e.getReservation();
			System.out.println(e.getId().toString());
			examRepo.deleteById(e.getId());	
			// PROVERITI DA LI SE BRISI REZERVACIJA
			examRepo.flush();
			return "Appointment successfully cancelled.";
		}else {
			return "You can't cancel an appointment which is in less than 24h.";
		}	
	}
	
	private boolean appointmentUnique(Integer doctorId, Date ex_date, String type) {
		
		Doctor d = doctorRepo.findById(doctorId).get();
		
		Set<Exam> exams = d.getExam();
		
		boolean unique = true;
		for (Exam exam : exams) {
			if(getDateOnly(exam.getDateTime()).equals(getDateOnly(ex_date))
				&& getTimeOnly(exam.getDateTime()).equals(getTimeOnly(ex_date))) {
				unique = false;
				break;
			}
	
		}
		
		return unique;
	}
	
	public boolean reservationUnique(Date ex_date, Room r) {
		System.err.println("				Datum: "  +ex_date);
			
			//Clinic c = clinicRepo.findById(clinicId).get();
		//	List<Reservation> allRes = reservationRepo.findByAppointmentClinic(c);			
			
			Set<Reservation> allRes= r.getRessByDate(ex_date);
			System.err.println(allRes);
			//System.err.println("       "+ allRes);
			boolean unique = true;
			for (Reservation res: allRes) {
				System.err.println("       "+res);
				System.err.println("     res date  "+getDateOnly(res.getAppointment().getDateTime()));
				System.err.println("     ex date  "+getDateOnly(ex_date));
				System.err.println("     res time  "+getTimeOnly(res.getAppointment().getDateTime()));
				System.err.println("     ex time  "+getTimeOnly(ex_date));
				if(getDateOnly(res.getAppointment().getDateTime()).equals(getDateOnly(ex_date))
					&& getTimeOnly(res.getAppointment().getDateTime()).equals(getTimeOnly(ex_date))) {
					unique = false;
					break;
				}
		
			}
			
			return unique;
		}
	
	private String getDateOnly(Date date) {
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		return formatter.format(date);
	}
	
	public String getTimeOnly(Date date) {
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		
		return formatter.format(date);
	}
	
	@Transactional(readOnly = false)
	public String scheduleAppointment(Map<String, String> payload, Patient p) {
		synchronized(this) {
			Date date = new Date(); 
			
			
			Integer clinic_id = Integer.parseInt(payload.get("clinic"));
			Integer doctor_id = Integer.parseInt(payload.get("doctorId"));
			String type = payload.get("type");
			Date ex_date = null;
			try {
				ex_date = formatter.parse(payload.get("date")+" "+payload.get("time"));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if (!appointmentUnique(doctor_id, ex_date, type)) {
				System.out.println("VREME VISE NIJE SLOBODNO");
				return "Selected time is not available anymore.";
			}
			
			ReqAppointment ra = new ReqAppointment();

			Exam e = new Exam();
			Clinic c = clinicRepo.findById(clinic_id).get();
			e.setClinic(c);

			e.setDoctor(doctorRepo.findById(doctor_id).get());
			e.setDateTime(ex_date);
			e.setHeld(false);
			e.setType(examTypeRepo.findByName(type));
			
			//DODATO, za testiranje
			e.setMedRecord(p.getMedrec());
			/*
			
			 // AUTOMATSKI SE PRAVI REZERVACIJA ZA POTREBE TESTIRANJA, TREBA OBRISATI KAD SE NAPRAVI DA ADMIN RESAVA REZERVACIJE
			 
					Room room = new Room();
				
					room.setNumber(9999999);
					room.setName("NON EXISTING ROOM");
					Reservation r = new Reservation();
					r.setPatient(p);
					r.setRoom(room);
				
					
					reservationRepo.saveAndFlush(r);
					
					e.setReservation(r);
					
			*/
			
			ra.setSender(p);
			ra.setType(RequestType.EXAM);
			ra.setArrived(formatter.format(date).toString());
			ra.setSeen(false);
			ra.setAppointment(e);
			ra.setId(444);
			
			examRepo.saveAndFlush(e);
			requestAppoRepo.saveAndFlush(ra);
			
			
			//SendEmail se = new SendEmail();
			//se.sendAppointmentConfirmation(ra, p);
			
			SendEmail se = new SendEmail();
			ArrayList<Admin> admins = new ArrayList<>(c.getAdmins());
			new Thread(new Runnable() {
			    public void run() {
			    	se.sendRequestNotification(admins);
			    }
			}).start();

			
			return "We'll send you an email notification when your appointment has been approved.";
		}
		
	}
}
