package com.example.demo.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.database.UserRepo;
import com.example.demo.model.Patient;
import com.example.demo.model.User;
import com.google.gson.Gson;

import net.minidev.json.JSONObject;

@Service
@Transactional(readOnly = true)
public class UserService {

	@Autowired
	UserRepo uRepo;
	private static Gson g = new Gson();
	public String getUserProfile(User u) {
		JSONObject job = new JSONObject();
		job.put("name", u.getFirstName());
		job.put("last", u.getLastName());
		job.put("city", u.getAddress().getCity());
		job.put("country", u.getAddress().getCountry());
		job.put("streetName", u.getAddress().getStreetName());
		job.put("number", u.getPhoneNUmber());
		return g.toJson(job);
	}
	
	@Transactional(readOnly = false)
	public void updateUserProfile(Map<String, String> payload, User u) {

		u.getAddress().setCity(payload.get("city"));
		u.getAddress().setCountry(payload.get("country"));
		u.getAddress().setStreetName(payload.get("street"));
		
		u.setFirstName(payload.get("firstName"));
		u.setLastName(payload.get("lastName"));
		u.setPhoneNUmber(payload.get("phoneNumber"));
		
		if(!payload.get("pass").equals("")) {
			u.setPassword(payload.get("pass"));
		}
		
		
		uRepo.saveAndFlush(u);
		
	}

}
