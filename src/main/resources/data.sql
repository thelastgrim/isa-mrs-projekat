INSERT INTO address (ID, CITY, COUNTRY, STREET_NAME) VALUES
	(1, 'Belgrade', 'Serbia', 'Kralja Petra 121B'),
	(2, 'Sofia', 'Bulgaria', 'Kalotina 12-14'),
	(3, 'Monte Carlo','Monaco','Avenue ''Ostende D12'),
	(4, 'Gelsenkirchen','Germany','Florastraße 6214' ),
	(5, 'Paris','France','Avenue des Champs-Élysées VQ21'),
	(9999, 'Niš', 'Serbia', 'test'),
	(9998, 'Belgrade', 'Serbia', 'Savska'),
	(9997, 'Novi Sad','Serbia','Bul Oslobođenja'),
	(9996, 'Novi Sad','Serbia','Bul Oslobođenja');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (6,'Taiwan','Cameroon','Ap #683-677 In Road'),(7,'French Southern Territories','Belgium','5307 Curabitur Road'),(8,'Taiwan','Mongolia','P.O. Box 172, 6692 Sed Rd.'),(9,'Korea, South','Saint Pierre and Miquelon','P.O. Box 150, 9024 Pretium Road'),(10,'Haiti','Denmark','251-2106 Phasellus Av.');	
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (11,'Rankweil','Kyrgyzstan','7105 Accumsan Road'),(12,'San Juan de la Costa','French Polynesia','1516 Convallis Rd.'),(13,'Henley-on-Thames','Tajikistan','495-7547 Sem. Ave'),(14,'Amaro','Greece','P.O. Box 382, 2273 Gravida. St.'),(15,'Paderborn','Poland','P.O. Box 621, 2454 Malesuada St.'),(16,'San José de Maipo','Palau','844 Quis Road'),(17,'Levallois-Perret','Eritrea','Ap #466-3991 Ultricies Rd.'),(18,'Minyar','Syria','5798 Semper Road'),(19,'Firozabad','Senegal','1728 In, Av.'),(20,'Lincoln','Micronesia','2864 Justo. Avenue');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (21,'Montefiore dell''Aso','Ukraine','2505 Sed Avenue'),(22,'Fairbanks','Greenland','P.O. Box 345, 912 Euismod Rd.'),(23,'Verdun','Bhutan','1378 Odio. Road'),(24,'Conca Casale','Djibouti','8304 Elit Av.'),(25,'Vosselaar','Fiji','928-8412 Integer Rd.'),(26,'Ayas','Kyrgyzstan','Ap #721-3408 Odio, Street'),(27,'Tarma','Dominican Republic','2003 Erat. Avenue'),(28,'Fusagasugá','Cook Islands','P.O. Box 109, 4165 Pede, St.'),(29,'Hillsboro','Philippines','5937 Cras Av.'),(30,'Harderwijk','Papua New Guinea','502-802 A Road');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (31,'Kotli','Korea, South','Ap #334-6188 Aliquam Avenue'),(32,'Harderwijk','Qatar','6712 Facilisis Rd.'),(33,'Maglie','Gabon','P.O. Box 568, 3681 Lacus Street'),(34,'Stevenage','Burkina Faso','922-7906 Est, Road'),(35,'Dégelis','United Kingdom (Great Britain)','Ap #602-3129 At St.'),(36,'Villenave-d''Ornon','American Samoa','259-4519 Euismod Av.'),(37,'Coltauco','Burundi','640 Pede. Rd.'),(38,'Springdale','Ireland','Ap #887-4800 Pharetra, Rd.'),(39,'Nakusp','Malta','P.O. Box 846, 9117 Ante. Rd.'),(40,'Kawartha Lakes','Cook Islands','117 Commodo Road');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (41,'Bhusawal','Jamaica','3749 Venenatis Rd.'),(42,'Katihar','Ethiopia','257-2685 Aliquet. Ave'),(43,'Heredia','Iraq','P.O. Box 164, 1689 Lorem Road'),(44,'Fort Wayne','Lebanon','979-5757 Tincidunt, Avenue'),(45,'Lier','Korea, South','4541 Aliquam Rd.'),(46,'Zhob','Anguilla','P.O. Box 433, 2023 Donec Avenue'),(47,'Kahramanmaraş','Barbados','Ap #226-3225 Ultrices. Av.'),(48,'Jinju','French Southern Territories','Ap #237-1928 Vitae Rd.'),(49,'Tranent','Viet Nam','7579 Nascetur Av.'),(50,'Calle Larga','Hungary','9764 Semper Rd.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (51,'Dawson Creek','Grenada','748-9257 In, Rd.'),(52,'Rochester','Montserrat','Ap #331-8324 Cum Rd.'),(53,'Ch‰tillon','Slovenia','7285 Nibh. Road'),(54,'Tomsk','Oman','459-6858 Malesuada. Rd.'),(55,'Lanester','Guatemala','9786 Sociis Street'),(56,'Pathankot','Niue','537-7748 Interdum Street'),(57,'Gdańsk','Cuba','998-4496 Amet St.'),(58,'Renlies','Kyrgyzstan','192-8553 Mattis St.'),(59,'Geraldton-Greenough','United States Minor Outlying Islands','235-5429 Feugiat Av.'),(60,'Ottawa-Carleton','French Polynesia','Ap #938-6800 Sit St.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (61,'Leernes','Maldives','P.O. Box 178, 5599 Fusce Ave'),(62,'North Jakarta','Niger','Ap #831-5612 Nam Street'),(63,'Valle del Guamuez','French Guiana','P.O. Box 348, 9026 Massa. Avenue'),(64,'Hampstead','Palestine, State of','4546 Elementum St.'),(65,'Orai','Botswana','365-504 Nunc St.'),(66,'San Lorenzo','Niger','8335 A Street'),(67,'Roux','Holy See (Vatican City State)','507-5598 Mollis. Rd.'),(68,'Castres','Tunisia','160-4214 Ullamcorper. Street'),(69,'Dubuisson','Norway','Ap #685-8655 Ullamcorper, Avenue'),(70,'Yangju','Estonia','9353 Praesent St.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (71,'Comox','Cayman Islands','P.O. Box 881, 171 Ante. St.'),(72,'Tiruvannamalai','Guam','Ap #336-2241 Magna. Ave'),(73,'Sainte-Marie-sur-Semois','Dominican Republic','516-4893 Nec Rd.'),(74,'Saint-GŽry','Iran','947-3864 Mauris Street'),(75,'Llanelli','Morocco','812-5677 Dictum St.'),(76,'Khanewal','Hungary','Ap #298-5738 Lorem, Av.'),(77,'Springfield','Antarctica','115 At Rd.'),(78,'Angleur','Northern Mariana Islands','409-6377 Sit Rd.'),(79,'Nerem','Guinea','682-4222 Orci, Avenue'),(80,'Cardiff','Monaco','431-2086 Nulla. Rd.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (81,'Moerbeke','United States Minor Outlying Islands','P.O. Box 481, 4396 Dolor. St.'),(82,'Burns Lake','Norfolk Island','Ap #796-7333 Quisque Rd.'),(83,'Richmond','Zimbabwe','Ap #314-2750 Dolor St.'),(84,'Ipswich','Cook Islands','Ap #678-3775 Luctus. Rd.'),(85,'Codó','United States','4820 Consectetuer, Avenue'),(86,'Orilla','Kenya','890-3933 Tellus Street'),(87,'Rae Lakes','Philippines','P.O. Box 579, 4553 Sit St.'),(88,'Steyr','Western Sahara','3451 Lectus St.'),(89,'HomprŽ','Cayman Islands','341-1993 Non, Avenue'),(90,'Forbach','Netherlands','Ap #871-2027 Egestas. Rd.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (91,'Camarones','Cook Islands','3375 Blandit Av.'),(92,'Rosolini','Kyrgyzstan','P.O. Box 911, 6595 Aenean Road'),(93,'Pisa','Saudi Arabia','Ap #274-6459 Enim. Street'),(94,'Salt Lake City','Taiwan','Ap #165-120 Ligula. St.'),(95,'Talgarth','Belarus','363-2560 Id, Rd.'),(96,'Miami','Switzerland','874-4289 Nunc Road'),(97,'Würzburg','Lithuania','P.O. Box 639, 6587 Nec Ave'),(98,'Notre-Dame-du-Nord','Paraguay','717-8470 Arcu Avenue'),(99,'Bellefontaine','French Polynesia','6564 Et, Av.'),(100,'Kashmore','Malaysia','P.O. Box 256, 8611 Tincidunt Rd.');
INSERT INTO address (ID,CITY,COUNTRY,STREET_NAME) VALUES (101,'San Cesario di Lecce','Bahrain','Ap #896-7021 Eget, Rd.'),(102,'Bois-de-Villers','Togo','6225 Adipiscing Rd.'),(103,'Villers-Perwin','United Kingdom (Great Britain)','3612 Vehicula St.'),(104,'Landeck','Azerbaijan','107 Non, Av.'),(105,'Cartago','Palau','Ap #732-9870 A, St.'),(106,'Kitscoty','Burkina Faso','5815 Elit, Street'),(107,'Cuenca','Eritrea','Ap #780-6536 Lorem Rd.'),(108,'Martigues','Nepal','375-1253 Arcu St.'),(109,'Codegua','Taiwan','425-3269 Lorem, Ave'),(110,'Bacabal','Mozambique','P.O. Box 781, 6623 Cubilia Street');

INSERT INTO clinics (IDC, NAME, DESCRIPTION, ADDRESS_ID, RATING, VERSION) VALUES
	(9999, 'Cardiology clinic', 'Lepa klinika', 11, 3.5, 0),
	(9998, 'Endocrinology clinic', 'Lepa klinika', 12, 3.0, 0),
	(9997, 'Sports Medicine Clinics', 'Lepa klinika',13, 5.0, 0),
	(9996, 'Clinic for Special Children', 'Fighting Genetic Illnesses with Cutting-Edge Research and Compassion', 14, 4.5, 0);
	
INSERT INTO diagnoses (ID, CODE, LATIN, DESCRIPTION) VALUES
	(9000, 'ReInSy', 'Nefritis', 'Againts cancer.'),
	(8999, 'GlNe', 'Glomerulonephritis', 'Againts HIV'),
	(7999, 'HypeTe', 'Hypertension', 'Againts FTN'),
	(7998, 'HypoTe', 'Hypotension', 'Againts cancer');

INSERT INTO users (VERSION, ID, E_MAIL, PASSWORD, FIRST_NAME, LAST_NAME, PHONENUMBER, ADDRESS_ID, STATUS) VALUES
	(0, 9999, 'test@gmail.com',    'test',    'test',    'test',    12, 5, 1),
	(0, 9998, 'ccadmin@gmail.com', 'ccadmin', 'ccAdmin', 'ccAdmin', 9,  4, 1),
	--(0, 9997, 'ana.jordanovic@hotmail.com',   'admin',   'admin',   'admin',   9,  3, 1),
	(0, 9996, 'filip.radojicic.ue@gmail.com',   'admin2',   'admin2',   'admin2',   9,  2, 1),
	(0, 8000, 'doctor@gmail.com',  'doctor',  'John',    'Doe',     12, 1, 1),
	(0, 8001, 'sawjigsaw009@gmail.com', 'doctor2', 'Tina',    'Turner',  12, 6, 1),
	(0, 8002, 'doctor3@gmail.com', 'doctor3', 'Mickey',  'Rourke',  12, 7, 1),
	(0, 8003, 'nurse@gmail.com', 'nurse', 'Nurse1',    'Nurse1',  123, 8, 1),
	(0, 8004, 'horrorue@gmail.com', 'test', 'Filip', 'Radojicic', '12', 9, 1);

INSERT INTO doctors (ID, RATING, START_TIME, END_TIME, CLINIC_IDC) VALUES
	(8000, 5.0, '12:00', '00:00', 9999),
	(8001, 4.25, '08:00', '14:00', 9999),
	(8002, 3.5, '18:00', '02:00', 9998);
	
INSERT INTO ratings_clinics (ID, CLINIC_IDC, RATING) VALUES
	(9999, 9999, 5.0),
	(9998, 9999, 2.0),
	(9997, 9998, 5.0),
	(9996, 9998, 1.0),
	(9995, 9997, 5.0),
	(9994, 9996, 4.5);

INSERT INTO ratings_doctors (ID, DOCTOR_ID, RATING) VALUES
	(9999, 8000, 5.0),
	(9998, 8001, 5.0),
	(9997, 8001, 4.0),
	(9996, 8001, 5.0),
	(9995, 8001, 3.0),
	(9994, 8002, 2.0),
	(9993, 8002, 5.0);
	
	
INSERT INTO med_records (ID, BLOOD_TYPE, DIOPTER, HEIGHT, WEIGHT) values
	(5000, 'AB+', '-1/-0.5', 180, 85),
	(5001, 'A-', '-2/-1.5', 156, 75);
	
INSERT INTO patients (ID, MEDREC_ID, SSN) VALUES
	(9999, 5000, 12312512312),
	(8004, 5001, 29057298347);
	

INSERT INTO exam_types(ID, NAME, PRICE, CLINIC_IDC, DURATION) VALUES
	(9999, 'Stress test on a treadmill (exercise test)', 150, 9999, 120),
	(9998, '24-hour Holter monitoring ECG and blood pressure', 850, 9999, 90),
	(9997, 'Complete biochemical assessment of the cardiovascular system', 850, 9999, 60),
	(9996, 'Fine-needle biopsy of the thyroid (FNB)', 250, 9998, 30),
	(9995, 'Toxicological analysis', 300, 9998, 45);
	

INSERT INTO ratings_clinics (ID, RATING, PATIENT_ID, CLINIC_IDC) VALUES
	(8994, 0, 9999, 9998),
	(8993, 0, 9999, 9999),
	(8992, 0, 8004, 9999);

	

	
INSERT INTO ratings_doctors (ID, RATING, PATIENT_ID, DOCTOR_ID) VALUES
	(8999, 0, 9999, 8000),
	(8997, 0, 9999, 8001),
	(8996, 0, 9999, 8002),
	(8995, 0, 8004, 8002);

	
	
INSERT INTO rooms (ID, NUMBER, CLINIC_IDC, NAME,version) VALUES
	(9999, 27,9999, 'ROOM1',0),
	(9996, 277,9999, 'ROOM2',0),
	(9995, 227,9999, 'ROOM3',0),	
	(9994, 722,9999, 'ROOM4',0),
	(9993, 7277,9999, 'ROO',0),
	
	(666, 66, 9999, 'ae',0),
	(601, 23, 9999, 'x123',0),
	(602, 54, 9999, '31dv',0),
	(603, 44, 9999, 'f2x',0),
	(604, 55, 9999, 'x314',0),
	(505, 40, 9999, 'OperSala2', 0);

	


INSERT INTO RESERVATIONS (id,room_id,version)values
	(987,9999,0),
	(9877,9999,0),
	(9876,9999,0),
	(98765,9999,0),
	(1,9999,0),
	(2,9999,0),
	(3,9999,0),
	(4,9999,0),
	(5,9999,0),
	(6,9999,0),
	(7,9999,0),
	(119,9999,0),
	(229,9999,0),
	(339,9999,0),
	(449,9999,0),
	(559,9999,0),
	(669,9999,0),
	(779,9999,0),
	(889,9999,0),
	(8899,9999,0),
	(8877,9999,0),
	(654,9996,0);

INSERT INTO reservations (ID, ROOM_ID, PATIENT_ID,version) VALUES
	(1000,9999, 9999,0),
	(10001,9999, 9999,0),
	(10002,9999, 9999,0),
	(10003,9999, 9999,0),
	(10004,9999, 9999,0),
	(10005,9999, 9999,0),
	(10006,9999, 9999,0),
	(10007,505, 9999,0);	

	
INSERT INTO APPOINTMENTS  (ID,DATE_TIME, HELD, CLINIC_IDC, RESERVATION_ID) VALUES
	(11, '20190502 10:15',true, 9999,  null),
	(22,  '20200502 10:30',true, 9999,  null),
	(33, '20200102 10:15',true, 9999,  null),
	(44, '20200202 10:15',true, 9999,  null),
	(55, '20200302 10:15',true, 9999,  null),
	(551, '20200302 10:30',true, 9998,  null),
	(66, '20200402 10:45',true, 9998,  null),
	(661, '20200402 10:30',true, 9998,  null),
	(662, '20200402 10:55',true, 9998,  null),
	(77, '20200502 10:15',true, 9998,  null),
	
	(771, '20200427 10:15',true, 9999,  null),
	(7712, '20200427 10:30',true, 9999,  null),
	(7713, '20200427 11:15',true, 9998,  null),
	(111122, '20200504 01:15',true, 9998,  null),
	(11113, '20200504 01:50',true, 9999,  null),
	(11111, '20200504 05:00',true, 9999,  null),
	(1111, '20200504 06:00',true, 9998,  null),
	(111, '20200504 07:00',true, 9998,  null),
	(222, '20200504 07:15',true, 9999,  null),
	(2222, '20200504 07:30',true, 9999,  null),
	(22222, '20200504 07:45',true, 9998,  null),
	(3333, '20200504 09:00',true, 9999,  null),
	(333, '20200504 09:15',true, 9998,  null),
	(444, '20200504 10:15',true, 9999,  null),
	(4444, '20200504 10:30',true, 9998,  null),
	(555, '20200504 11:15',true, 9999,  null),
	(666, '20200504 13:15',true, 9999,  null),
	(7777, '20200504 14:00',true, 9998,  null),
	(777, '20200504 14:15',true, 9998,  null),
	(888, '20200504 22:15',false, 9999,  null),
	(999, '20200504 22:30',false, 9998,  null),
	
	(1, '20200630 07:25',false, 9998,  1),
	(2, '20200630 08:15',false, 9998,  2),
	(88, '20200630 10:00',false, 9998, 987),
	(3, '20200630 10:55',false, 9998,  3),
	(4, '20200630 11:50',false, 9998,  4),
	(5, '20200630 12:45',false, 9998,  5),
	(6, '20200630 13:40',false, 9998,  6),
	(7, '20200630 14:35',false, 9998,  7),
	
	(119, '20200629 07:25',false, 9998,  119),
	(229, '20200629 08:15',false, 9998,  229),
	(339, '20200629 10:55',false, 9998,  339),
	(449, '20200629 11:50',false, 9998,  449),
	(559, '20200629 12:45',false, 9998,  559),
	(669, '20200629 13:40',false, 9998,  669),
	(779, '20200629 14:35',false, 9998,  779),
	(889, '20200629 15:15',false, 9998,  889),
	(8899, '20200629 09:10',false, 9998,  8899),
	(8877, '20200629 10:00',false, 9998,  8877),
	(88123, '20200630 15:15',false, 9998,  9877),
	(881234, '20200630 09:10',false, 9998,  98765),
	(8812, '20200702 15:20',false, 9998,  9876),
	(99, '20200702 10:15',false, 9998,  654);

UPDATE RESERVATIONS SET APPOINTMENT_ID = 1 WHERE ID = 1;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 2 WHERE ID = 2;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 88 WHERE ID = 987;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 3 WHERE ID = 3;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 4 WHERE ID = 4;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 5 WHERE ID = 5;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 6 WHERE ID = 6;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 7 WHERE ID = 7;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 119 WHERE ID = 119;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 229 WHERE ID = 229;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 339 WHERE ID = 339;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 449 WHERE ID = 449;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 559 WHERE ID = 559;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 669 WHERE ID = 669;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 779 WHERE ID = 779;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 889 WHERE ID = 889;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 8899 WHERE ID = 8899;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 8877 WHERE ID = 8877;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 88123 WHERE ID = 9877;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 881234 WHERE ID = 98765;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 8812 WHERE ID = 9876;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 99 WHERE ID = 654;

	
INSERT INTO APPOINTMENTS  (ID, DATE_TIME, HELD, CLINIC_IDC, MED_RECORD_ID, RESERVATION_ID) VALUES
	(9999, '20201212 10:00', false, 9998, 5000, 1000),
	(9998, '20191212 10:00', true, 9998, 5000,  10001),
	(9997, '20201213 10:00', false, 9998, 5000,  10002),
	(9996, '20191213 10:00', true, 9998, 5000,  10003),
	(9990, '20191213 20:00', true, 9998, 5000,  10004),
	(9980, '20191210 15:00', true, 9999, 5000,  10005),
	(9970, '20191012 08:45', true, 9999, 5001,  10006),
	(9960, '20200730 08:45', false, 9999, 5000,  null),
	(9950, '20200729 08:45', false, 9999, 5000,  null),
	(9940, '20200728 08:45', false, 9999, 5000,  null),
	(9930, '20200727 08:45', false, 9999, 5000,  null),
	(9800, '20190727 08:00', true, 9999, 5000,  10007); --prosla operacija

UPDATE RESERVATIONS SET APPOINTMENT_ID = 9800 WHERE ID = 10007;
	
	
INSERT INTO REQUEST(ID, ARRIVED, SEEN, TYPE, SENDER_ID) values
	(1,'2020-06-07 18:00', false,0, 8000 ),
	(2,'2020-06-07 19:00', false,0, 8000 ),
	(3,'2020-06-07 18:00', false,0, 8000 ),
	(4,'2020-06-07 18:00', false,0, 8000 ),
	(55,'2020-06-10 18:00', false,3, 8000 ),
	(66,'2020-06-10 18:00', false,3, 8000 ),
	(77,'2020-06-10 18:00', false,3, 8000 ),
	(88,'2020-06-10 18:00', false,3, 8000 );
	
INSERT INTO REQ_APPOINTMENT(ID, APPOINTMENT_ID) values
	(1,9960),
	(2,9950),
	(3,9940),
	(4,9930);



INSERT INTO REQ_LEAVE(ID,START_DATE,END_DATE,LEAVE_TYPE ) values
	(55,'2020-07-01 00:00','2020-07-07 00:00',0),
	(66,'2020-07-07 00:00','2020-07-14 00:00',0),
	(77,'2020-07-14 00:00','2020-07-21 00:00',1),
	(88,'2020-07-21 00:00','2020-07-30 00:00',1);

INSERT INTO RESERVATIONS (ID, ROOM_ID,version) values
	(8888, 666,0),
	(8887, 666,0),
	(8886, 666,0),
	(8885, 601,0),
	(8884, 602,0),
	(8883, 603,0),
	(8882, 604,0);
	
	--(8601, 601,0),
	--(8602, 602,0),
	--(8603, 603,0),
	--(8604, 604,0);

	
	

	
/* brzi pregledi i zakazani pregledi */	
INSERT INTO APPOINTMENTS (ID, DATE_TIME, HELD, CLINIC_IDC, RESERVATION_ID) values
	(111111, '20210515 17:25', false, 9999, 8888),
	(111112, '20210518 12:00', false, 9999, 8887),
	(111113, '20210510 20:45', false, 9998, 8886),
	
	(111114, '20210514 15:25', false, 9999, 8885),
	(111115, '20210514 17:30', false, 9999, 8884),
	(111117, '20210514 21:35', false, 9999, 8883),
	(111118, '20210514 09:40', false, 9999, 8882);

	
/* brzi pregledi i zakazani pregledi */	
INSERT INTO EXAMS (ID, DIAGNOSIS_ID, DOCTOR_ID, TYPE_ID) VALUES
	(111111, null, 8000, 9999),
	(111112, null, 8001, 9998),
	(111113, null, 8002, 9997),
	
	(111114, null, 8000, 9999),
	(111115, null, 8000, 9999),
	(111117, null, 8000, 9999),
	(111118, null, 8001, 9999),
	(9960, null, 8000, 9999),
	(9950, null, 8000, 9998),
	(9940, null, 8000, 9997),
	(9930, null, 8000, 9999);
	
	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111111 WHERE ID = 8888;
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111112 WHERE ID = 8887;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111113 WHERE ID = 8886;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111114 WHERE ID = 8885;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111115 WHERE ID = 8884;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111117 WHERE ID = 8883;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 111118 WHERE ID = 8882;

UPDATE RESERVATIONS SET APPOINTMENT_ID = 9999 WHERE ID = 1000;

UPDATE RESERVATIONS SET APPOINTMENT_ID = 9970 WHERE ID = 10006;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 9980 WHERE ID = 10005;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 9990 WHERE ID = 10004;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 9996 WHERE ID = 10003;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 9997 WHERE ID = 10002;	
UPDATE RESERVATIONS SET APPOINTMENT_ID = 9998 WHERE ID = 10001;		
	
	
INSERT INTO EXAMS (ID, DIAGNOSIS_ID, DOCTOR_ID, TYPE_ID) VALUES
	(9999, 9000, 8000, 9995),
	(9998, 9000, 8000, 9995),
	(9990, 8999, 8001, 9996),
	(9980, 7999, 8002, 9999),
	(9970, 7998, 8002, 9999);
	
INSERT INTO SURGERIES (ID, NAME, CLINIC_IDC) VALUES
	(9997, 'Adrenalectomy', 9998),
	(9996, 'Hysteroscopy', 9998),
	(9800, 'Coronary bypass surgery', 9999);

INSERT INTO SURGERY_DOCTOR (DOCTOR_ID, SURGERY_ID) VALUES
	(8000, 9800),
	(8001, 9800),
	(8002, 9996);



	
INSERT INTO CC_Admins (ID) VALUES 
	(9998);


	
	
INSERT INTO admins (ID, CLINIC_IDC) VALUES
	(9996, 9999);
	--(9997, 9999);

	
INSERT INTO SPECIALIZATIONS (DOCTOR_ID, EXAM_TYPE_ID) VALUES
	(8000, 9999),
	(8000, 9998),
	(8000, 9997),
	(8000, 9996),
	(8000, 9995),
	(8001, 9999),
	(8001, 9998),
	(8001, 9997),
	(8001, 9996),
	(8001, 9995),
	(8002, 9999),
	(8002, 9998),
	(8002, 9997),
	(8002, 9996),
	(8002, 9995);

INSERT INTO nurses (ID, CLINIC_IDC, START_TIME, END_TIME) VALUES 
	(8003,9997, '08:00','16:00');
	
INSERT INTO prescriptions (ID,THERAPY,VALIDATED,EXAM_ID,NURSE_ID) VALUES
	(1,'Cold Shower', FALSE,9970,8003),
	(2,'Chill', FALSE,9998,8003),
	(3,'Smoke weed', FALSE,9999,8003),
	(4,'Resting', FALSE,9930,8003),
	(5,'Gym', FALSE,9940,8003),
	(6,'Swimming', FALSE,9950,8003),
	(7,'Running', FALSE,9970,8003),
	(8,'Fruits', FALSE,9999,8003);

INSERT INTO ABSENCE(ID, START, END) VALUES
	(1, '20210614', '20210628');
	
INSERT INTO absence_doctors(ABSENCE_ID, DOCTOR_ID) VALUES
	(1,8002);
	

